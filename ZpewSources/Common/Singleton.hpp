#ifndef __ZPEW_SINGLETON_HPP__
#define __ZPEW_SINGLETON_HPP__

#include "ZpewSources/Common/Basement/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

template < typename T >
class Singleton
	:	private NonCopyable
{

public: 

	static T& getSingletonRef();
	static T* getSingleton();
	static void destroy();

protected: 

	explicit Singleton()
	{ 
		Assert( Singleton::ms_Instance == 0 ); 
		Singleton::ms_Instance = static_cast< T* >(this); 
	}
	virtual ~Singleton()
	{ 
		Singleton::ms_Instance = 0; 
	}

private: 

	static T * createInstance();
	static void scheduleForDestruction( void (*)( ) );
	static void destroyInstance( T* );

private: 

	static T * ms_Instance;

};  //class Singleton


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T& Singleton< T >::getSingletonRef()
{
	if ( Singleton::ms_Instance == 0 )
	{
		Singleton::ms_Instance = Singleton::createInstance();
		scheduleForDestruction( Singleton::destroy );
	}
	return *( Singleton::ms_Instance );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T* Singleton< T >::getSingleton()
{
	if ( Singleton::ms_Instance == 0 )
	{
		Singleton::ms_Instance = createInstance();
		scheduleForDestruction( Singleton::destroy );
	}
	return Singleton::ms_Instance;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
void
Singleton< T >::destroy()
{
	if ( Singleton::ms_Instance != 0 ) 
	{
		destroyInstance(Singleton::ms_Instance);
		Singleton::ms_Instance = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
typename T* Singleton< T >::createInstance()
{
	return new T();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
void
Singleton< T >::scheduleForDestruction( void ( * pFun )( ) )
{
	atexit( pFun );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
void
Singleton< T >::destroyInstance( T* p )
{
	delete p;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
typename T* Singleton< T >::ms_Instance = 0;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_SINGLETON_HPP__
