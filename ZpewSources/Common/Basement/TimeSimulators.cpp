#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Common/Basement/TimeSimulators.hpp"
#include "ZpewSources/Common/Basement/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

VariableTimeSimulator::VariableTimeSimulator()
	:	mDt( 0 )
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
VariableTimeSimulator::update( float time )
{
	mDt = time;
	return true;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
VariableTimeSimulator::getCurrentTimeStep() const
{
	return mDt;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


FixedTimeSimulator::FixedTimeSimulator( float idealSimulationTime )
	:	mIdealSimulationTime ( idealSimulationTime )
{
	mDt = rand( 0.f, idealSimulationTime );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
FixedTimeSimulator::update( float time )
{
	if( mDt >= mIdealSimulationTime )
		mDt = 0;

	mDt += time;
	
	if( mDt >= mIdealSimulationTime )
		return true;
	else
		return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

