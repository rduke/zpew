#ifndef __ZPEW_SWITCH_HPP__
#define __ZPEW_SWITCH_HPP__

#include "ZpewSources/Common/Basement/Utility.hpp"
#include <limits.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	// store some objects _Size - is number of them, represents sequential
	// access between two neightbours.

template< typename Type, int Size >
class Switch
{

public:

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	template< typename Type, int Size >
	struct Iterator
	{
	public:

		typedef Switch< Type, Size > Base;

		inline Iterator( Base& base, int currentIndex )
			:	mBase( base )
			,	mBaseIndex( currentIndex )
		{}
	
		// rollback time on "positions" frames back in past for this Iterator
		inline void rollBackPositions( int positions )
		{
			Assert( positions > 0 );
			Assert( positions < Size );
			//correctIndex( positions );
			mBaseIndex -= positions;
			correctIndex( mBaseIndex );
		}

		// forward in future!
		inline Iterator< Type, Size > & operator ++ ()
		{
			++ mBaseIndex;
			correctIndex( mBaseIndex );
				
			return *this;
		}

		// backward in past!
		inline Iterator< Type, Size > & operator --()
		{
			-- mBaseIndex;
			correctIndex( mBaseIndex );
		
			return *this;
		}

		inline Type& get()
		{
			return **this;
		}

		inline Type& operator *()
		{
			if ( mBaseIndex == mBase.mCurrentIndex )
				throw std::exception( "Switch rolling overflow" );

			return mBase.mData[ mBaseIndex ];
		}

		inline bool operator == ( const Iterator< Type, Size >& that )
		{
			return ( that.mBase == mBase && that.mBaseIndex == that.mBaseIndex );
		}

	private:
		
		int mBaseIndex;
		Base& mBase;

	}; // struct Iterator
	typedef Iterator< Type, Size > TIterator;

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	inline Switch( const Type& zeroValue )
		:	mCurrentIndex( 0 )
		,	mRollingCounter ( 0 )
	{
		for (int i=0; i<Size; i++ )
			mData [ i ] = zeroValue;
	}

	inline TIterator begin()
	{
		int previousIndex = mCurrentIndex - 1;
		correctIndex( previousIndex );
		return TIterator( *this, previousIndex );
	}
	
	inline TIterator end()
	{
		return TIterator( *this, mCurrentIndex );
	}

	
	inline void setObject( const int index, const Type& value )
	{
		mData[ index ] = value;
	}


	inline const int getSwitchSize() const
	{
		return Size;
	}
	
	inline const long long getInsertedRolledDataCount() const
	{
		return mRollingCounter;
	}

	inline void insertCurrent( const Type& New )
	{
		mData[ mCurrentIndex ] = New;
		++mCurrentIndex;
		correctIndex( mCurrentIndex );
		increaseRollingCounter(); // this means - data inserted
	}



	inline const Type & getPreviousByPos( int index ) const
	{
		Assert( index > 0 );
		int newIndex = mCurrentIndex - index;
		correctIndex( newIndex );
		return mData[ newIndex ];
	}
	
	inline Type & getPreviousByPos( int index )
	{
		Assert( index > 0 );
		int newIndex = mCurrentIndex - index;
		correctIndex( newIndex );
		return mData[ newIndex ];
	}


	inline Type & getPreviousPrevious()
	{
		return getPreviousByPos( 2 );
	}

	inline Type & getPrevious()
	{
		return getPreviousByPos( 1 );
	}


	inline const Type & getPreviousPrevious() const
	{
		int index = mCurrentIndex - 2;
		correctIndex( index );
		return mData[ index ];
	}

	inline const Type & getPrevious() const
	{
		int index = mCurrentIndex - 1;
		correctIndex( index );
		return mData[ index ];
	}


	inline Type getVelocity() const
	{
		int previous = mCurrentIndex - 1;
		
		correctIndex( previous );
		
		return Type(
				getPrevious()
			-	getPreviousPrevious()
		);
	}


	inline Type getVec3Velocity() const
	{
		int previous = mCurrentIndex - 1;
		
		correctIndex( previous );
		Type result;
		vecMin( result, getPrevious(), getPreviousPrevious() );
		return result;
	}

	inline void increaseRollingCounter()
	{
		if ( ++mRollingCounter > LLONG_MAX -1 )
			mRollingCounter = LLONG_MAX - 1;
	}

private:

	inline static void correctIndex( int& index )
	{
		index %= Size;
		if ( index < 0 )
			index = Size + index;
	}

	Type mData[ Size ];

	bool mIsCurrentValid;
	long long mRollingCounter;
	int mCurrentIndex;
	
}; // class Switch


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_SWITCH_HPP__
