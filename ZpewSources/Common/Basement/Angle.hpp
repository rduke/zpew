#ifndef __ZPEW_ANGLE_HPP__
#define __ZPEW_ANGLE_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#pragma warning( disable : 4305 4244 )

	const float Pi		=	3.14159265359;
	const float TwoPi	=	Pi * 2.0;
	const float G		=	9.80665;

#pragma warning( default : 4305 4244 )


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Angle
{

private:

	Angle( float radians )
		:	mRadians( radians )
	{}

public:

	static Angle buildRadians( float radians )
	{
		return Angle( radians );
	}
	
	static Angle buildDegrees( float degrees )
	{
		Angle a;
		a.setDegrees( degrees );
		return a;
	}

	inline Angle()
		:	mRadians(0)
	{}

	inline void div( float d )
	{
		mRadians /= d;
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	inline bool operator < ( const Angle a )
	{
		return mRadians < a.mRadians;
	}

	inline bool operator > ( const Angle a )
	{
		return mRadians > a.mRadians;
	}

	inline bool operator == ( const Angle a )
	{
		return mRadians == a.mRadians;
	}

	inline bool operator != ( const Angle a )
	{
		return mRadians != a.mRadians;
	}

	inline Angle operator + ( const Angle a )
	{
		return Angle( mRadians + a.mRadians );
	}

	inline Angle operator - ( const Angle a )
	{
		return Angle( mRadians - a.mRadians );
	}

	inline Angle operator - ()
	{
		return Angle( - mRadians );
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	inline float getRadians() const
	{
		return mRadians;
	}

	inline float getDegrees() const
	{
		return mRadians / Pi * 180;
	}

	inline void setRadians( float radians )
	{
		mRadians = radians;
	}

	inline void setDegrees( float degrees )
	{
		mRadians = degrees * Pi / 180;
	}

	inline float cos() const
	{
		return ::cos( mRadians );
	}
	
	inline float sin() const
	{
		return ::sin( mRadians );
	}
	
	inline float tan() const
	{
		return ::tan( mRadians );
	}
	
	static Angle atan( float trygValue )
	{
		return Angle ( ::atan( trygValue ) );
	}

	static Angle acos( float trygValue )
	{
		if ( trygValue >= 1 )
			return Angle ( ::acos( 1.f ) );

		if ( trygValue <= -1 )
			return Angle ( ::acos( -1.f ) );

		return Angle ( ::acos( trygValue ) );
	}

	static Angle asin( float trygValue )
	{
		if ( trygValue >= 1 )
			return Angle ( ::asin( 1.f ) );

		if ( trygValue <= -1 )
			return Angle ( ::asin( -1.f ) );

		return Angle ( ::asin( trygValue ) );
	}


	template< typename TVec3 >
 	static Angle angleBetweenVectors3( const TVec3& vec1, const TVec3& vec2 )
 	{		
 		float lenProduct = magnitudeXZ( vec1 ) * magnitudeXZ( vec2 );
 		lenProduct = returnCorrectDivider( lenProduct );
 
 		float f = ( vec1.x * vec2.x  +  vec1.z * vec2.z ) / lenProduct;
  		clamp( -1.0f, 1.0f, f );
 		return Angle::acos( f );
 	}

private:

	float mRadians;

}; // class Angle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_ANGLE_HPP__
