#ifndef __ZPEW_COSINE_INTERPOLATION_CURVE_HPP__
#define __ZPEW_COSINE_INTERPOLATION_CURVE_HPP__

#include "ZpewSources/Common/Basement/Utility.hpp"
#include <map>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define BASEMENT_MapType			std::map< Type, Type >
#define BASEMENT_MapIterator		BASEMENT_MapType::iterator
#define BASEMENT_ConstMapIterator	BASEMENT_MapType::const_iterator


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename Type >
class CosineInterpolationCurve
{
public:

	CosineInterpolationCurve()
		:	mMinValue( 0 )
		,	mMaxValue( 0 )
	{}

	bool isEmpty() const
	{
		return mMap.empty();
	}

	void clear() { mMap.clear(); }

	void insert( Type index, Type value)
	{
		if ( mMap.empty() )
		{
			mMinValue = index;
			mMaxValue = index;
		}
		else
		{
			mMinValue = std::min< Type >( mMinValue, index );
			mMaxValue = std::max< Type >( mMaxValue, index );
		}
		
		mMap [ index ] = value;
	}

	Type getValue( Type index ) const
	{
		if ( mMap.empty() )
			throw std::exception( "CosineInterpolationCurve::getValue" );
	
		BASEMENT_ConstMapIterator lower = mMap.begin();
		if ( index <= mMinValue )
			return lower->second;

		BASEMENT_ConstMapIterator upper = mMap.end();
		--upper;

		if ( index >= mMaxValue )
			return upper->second;

		upper = mMap.lower_bound( index );
		if ( upper == lower )
			return ( upper->second );

		lower = upper;
		-- lower;

		Type w1 = index - lower->first;

		return cosineInterpolation(
				lower->second // value
			,	upper->second // value
			,	w1 / ( upper->first - lower->first ) // interval value 0..1
		);
	} //  getValue

	Type mMinValue;
	Type mMaxValue;
	BASEMENT_MapType mMap;

}; // class CosineInterpolationCurve


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#undef BASEMENT_MapType
#undef BASEMENT_MapIterator
#undef BASEMENT_ConstMapIterator


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_COSINE_INTERPOLATION_CURVE_HPP__
