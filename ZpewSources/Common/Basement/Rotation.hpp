#ifndef __ZPEW_ROTATION_HPP__
#define __ZPEW_ROTATION_HPP__

#include "ZpewSources/Common/Basement/Angle.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Rotation
{
public:

	inline Rotation()
		:	mRotationsPerSec(0)
	{}

	friend inline Rotation operator * ( float ratio, const Rotation& rotation );
	friend inline Rotation operator * ( const Rotation& rotation, float ratio );

	inline float getRPM() const;
	inline float getRPS() const;
	inline float getRadiansPS() const;
	inline float getRadiansPM() const;
	inline float getDegreesPS() const;
	inline float getDegreesPM() const;

	inline void setRPM( float value );
	inline void setRPS( float value );
	inline void setRadiansPS( float value );
	inline void setRadiansPM( float value );
	inline void setDegreesPS( float value );
	inline void setDegreesPM( float value );

private:

	float mRotationsPerSec;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



Rotation operator * ( float ratio, const Rotation& rotation )
{
	Rotation r;
	r.setRPS( rotation.getRPS() * ratio );
	return r;
}

Rotation operator * ( const Rotation& rotation, float ratio )
{
	Rotation r;
	r.setRPS( rotation.getRPS() * ratio );
	return r;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float Rotation::getRPM() const
{
	return 60 * mRotationsPerSec;
}


float Rotation::getRPS() const
{
	return mRotationsPerSec;
}


float Rotation::getRadiansPS() const
{
	return getRPS() * TwoPi;
}


float Rotation::getRadiansPM() const
{
	return getRadiansPS() * 60;
}


float Rotation::getDegreesPS() const
{
	return getRPS() * 360;
}


float Rotation::getDegreesPM() const
{
	return getDegreesPS() * 60;
}	


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Rotation::setRPM( float value )
{
	mRotationsPerSec = value / 60;
}


void
Rotation::setRPS( float value )
{
	mRotationsPerSec = value;
}


void
Rotation::setRadiansPS( float value )
{
	setRPS( value / TwoPi );
}


void
Rotation::setRadiansPM( float value )
{
	setRadiansPS( value / 60 );
}


void
Rotation::setDegreesPS( float value )
{
	setRPS( value / 360 );
}


void
Rotation::setDegreesPM( float value )
{
	setDegreesPS( value / 60 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_ROTATION_HPP__