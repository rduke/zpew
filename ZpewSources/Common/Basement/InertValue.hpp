#ifndef __ZPEW_INERT_VALUE_HPP__
#define __ZPEW_INERT_VALUE_HPP__

#include "ZpewSources/Common/Basement/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename Type >
class InertValue
	:	NonCopyable
{

public:

	InertValue(
			const Type fixedTimeStep
		,	const Type goalValue
		,	const Type stepPerSecond
		,	const Type currentValue = 0
	)
		:	mFrameSimulationTime( fixedTimeStep )
		,	mCurrentRestfloatTime( 0 )
	{
		set(
				goalValue
			,	stepPerSecond
		);

		mCurrentValue = currentValue;
	}

	InertValue( const Type fixedTimeStep )
		:	mFrameSimulationTime( fixedTimeStep )
		,	mCurrentRestfloatTime( 0 )
	{
		reset( true );
	}

	// this methods should be used only
	// if you are sure this is hardly needed
	// no controls or asserts could be done in this method
	void setCurrentValue( const Type& currentValue )
	{
		mCurrentValue = currentValue;
	}

	void reset( bool shouldResetCurrentValue = false )
	{
		if( shouldResetCurrentValue )
			mCurrentValue = 0;

		mCurrentRestfloatTime = 0;
		mIsWorking = false;
	}


	void set(
			const Type goalValue
		,	const Type stepPerSecond
	)
	{
		Assert( stepPerSecond > 0 );
	
		mGoalValue = goalValue;
		mStepPerSecond = stepPerSecond;

		mCurrentStepping = fabs( mGoalValue - mCurrentValue );
		mIsWorking = true;
	}

	Type operator * () const
	{
		return mCurrentValue;
	}


	void update( float timeStep )
	{
		if ( ! mIsWorking )
			return;

		calculateConvergence();
		mCurrentRestfloatTime += timeStep;
		while ( mFrameSimulationTime <= mCurrentRestfloatTime )
		{			
			updateCurrentValue();
			mCurrentRestfloatTime -= mFrameSimulationTime; 
		}
	
	} // update

private:

	void updateCurrentValue() // uses fixed TimeStep!
	{
 		if ( mCurrentValue > mGoalValue )
 		{	
 		 	mCurrentValue -= fabs( mCurrentValue - mGoalValue ) * mStepPerSecond * mFrameSimulationTime * mConvergenceSpeedMultiplier;
 
 			if ( mCurrentValue < mGoalValue )
 				mCurrentValue = mGoalValue;
 		}
 		else if ( mCurrentValue < mGoalValue )
 		{
 	 		mCurrentValue += fabs( mCurrentValue - mGoalValue ) * mStepPerSecond * mFrameSimulationTime * mConvergenceSpeedMultiplier;
 
 			if ( mCurrentValue > mGoalValue )
 				mCurrentValue = mGoalValue;
 		}	
	}

	Type getDistanceDiameterRatio() const
	{
		Type diameter;
		if ( mCurrentValue * mGoalValue >= 0.0 )
		{
			Type current = fabs( mCurrentValue );
			Type goal = fabs( mGoalValue );

			if ( goal > current )
				diameter = goal;
			else
				diameter = current;
		}
		else
		{
			if ( mCurrentValue < 0.0 )
				diameter = - mCurrentValue + mGoalValue;
			else
				diameter = mCurrentValue - mGoalValue;
		}

		Type distance =fabs( mCurrentValue - mGoalValue );
		if ( distance < 0.001 )
			return 1;
		else
			return diameter / fabs( mCurrentValue - mGoalValue );
	}

	void calculateConvergence()
	{
		mConvergenceSpeedMultiplier = 1;//getDistanceDiameterRatio();
	}

private:

	bool mIsWorking;

	const Type mFrameSimulationTime;
	Type mCurrentRestfloatTime;

	Type mCurrentStepping;
	Type mConvergenceSpeedMultiplier;
	Type mStepPerSecond;
	Type mCurrentValue;
	Type mGoalValue;
	
}; // class InertValue


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_INERT_VALUE_HPP__