#ifndef __ZPEW_TimeSimulators_HPP__
#define __ZPEW_TimeSimulators_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct TimeSimulator
{
	virtual bool update( float time ) = 0;
	virtual float getCurrentTimeStep() const = 0;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class VariableTimeSimulator
	:	public TimeSimulator
{
public:

	VariableTimeSimulator();
	
	virtual bool update( float time );
	virtual float getCurrentTimeStep() const;

protected:

	float mDt;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class FixedTimeSimulator
	:	public VariableTimeSimulator
{
public:

	FixedTimeSimulator( float idealSimulationTime );

	virtual bool update( float time );

private:

	float mIdealSimulationTime;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_TimeSimulators_HPP__
