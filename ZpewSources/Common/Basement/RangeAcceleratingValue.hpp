#ifndef __ZPEW_RANGE_ACCELERATING_VALUE_HPP__
#define __ZPEW_RANGE_ACCELERATING_VALUE_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
class RangeAcceleratingValue
{

public:

	RangeAcceleratingValue( T velocity )
		:	mVelocity( velocity )
		,	mCurrent( 0 )
	{}

	void update( float timeStep, T goal )
	{
		if ( mCurrent > goal )
		{
			mCurrent -= mVelocity * timeStep;
			if ( mCurrent < goal )
				mCurrent = goal;
		}
		else
		{
			mCurrent += mVelocity * timeStep;
			if ( mCurrent > goal )
				mCurrent = goal;
		}
	}

	T getCurrent()
	{
		return mCurrent;
	}

	void setVelocity( T velocity )
	{
		mVelocity = velocity;
	}

private:

	T mCurrent;
	T mVelocity;
	
}; // class RangeAcceleratingValue


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


typedef RangeAcceleratingValue< float > RealRangeAcceleratingValue;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_RANGE_ACCELERATING_VALUE_HPP__