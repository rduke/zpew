#ifndef __ZPEW_OPTIONAL_HPP__
#define __ZPEW_OPTIONAL_HPP__


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace OptionalImpl{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class BaseSimpleOptional
{
public:

	BaseSimpleOptional()										{ mInit = false; }
	BaseSimpleOptional( const BaseSimpleOptional< T >& that )	{ reset(that); }
	void operator = ( const BaseSimpleOptional< T >& that )		{ reset(that); }

	operator bool() const		{ return mInit; }
	bool isInit() const			{ return mInit; }
		
	void reset( const BaseSimpleOptional< T >& that )
	{
		if( that.isInit() )
		{
			mInit = true;
			memcpy( &mRawData, &(that.mRawData), sizeof(T) );
		}
		else
			mInit = false;
	}
	
protected:

	template< typename T >
	struct Holder
	{
		char data[ sizeof(T) ];
	};

	bool mInit;
	Holder<T> mRawData;

}; // class BaseSimpleOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace OptionalImpl

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class RefOptional
	:	public OptionalImpl::BaseSimpleOptional< T >
{
public:

	void reset() { mInit = false; }

	void reset( const T data )
	{
		mInit = true;
		memcpy( &mRawData, &data, sizeof(T) );
	}
	
	RefOptional(){}

	RefOptional( const T data )			{ reset( data );	}
	void operator = ( const T & data )	{ reset( data );	}

	T& operator * ()					{ return get();		}
	T* operator -> ()					{ return &get();	}

	const T& operator * () const		{ return get();		}
	const T* operator -> ()	const		{ return &get();	}
	
	T& get()
	{
		Assert( mInit );
		return reinterpret_cast< T& >( mRawData );
	}
	
	const T& get() const
	{
		Assert( mInit );
		return reinterpret_cast< const T& >( mRawData );
	}

}; // class RefOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class ValueOptional
	:	public OptionalImpl::BaseSimpleOptional< T >
{
public:

	void reset() { mInit = false; }
	
	void reset( const T& data )
	{
		mInit = true;
		memcpy( &mRawData, &data, sizeof(T) );
	}

	ValueOptional(){}

	ValueOptional( const T data )		{ reset( data );	}
	void operator = ( const T data )	{ reset( data );	}

	T operator * ()	const				{ return get();		}
	T operator -> () const				{ return get();		}
	T get()	const
	{
		Assert( mInit );
		return *reinterpret_cast< const T* >( &mRawData );
	}

}; // class ValueOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_OPTIONAL_HPP__