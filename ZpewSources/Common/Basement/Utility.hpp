#ifndef __ZPEW_UTILITY_HPP__
#define __ZPEW_UTILITY_HPP__

// DO not use here any Physx or Bullet depended code!
#include "ZpewSources/Common/Basement/Angle.hpp"

#include <cassert>
#include <cmath>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class NonCopyable
{
protected:
	
	NonCopyable(){}
	~NonCopyable(){}

private:
	
	NonCopyable( const NonCopyable& );
	const NonCopyable& operator=( const NonCopyable& );

}; // class NonCopyable


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline Angle degrees( float value )
{
	return Angle::buildDegrees( value );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline Angle redians( float value )
{
	return Angle::buildRadians( value );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline float sqrt( float value )
{
	return ::sqrt( value );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
inline void Assert( T value )
{
	if ( ! value )	
  		assert( false );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
inline float returnCorrectDivider( T value )
{
	Assert(
			typeid( T ) != typeid( float )
		||	typeid( T ) != typeid( double )
		||	typeid( T ) != typeid( float )
	);

	if ( abs( value ) == 0 )
		return float( 0.00000000000001f );
	else
		return float( value );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
inline void assertNonNullDivider( T value )
{
	Assert ( value != 0 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec1, typename TVec2 >
inline bool equalVec3( const TVec1& vec1, const TVec2& vec2, float epsylon = 0.0002f )
{
	Assert( epsylon > 0 && epsylon < 0.1f );
	return
			fabs( vec1.x - vec2.x ) < epsylon
		&&	fabs( vec1.y - vec2.y ) < epsylon
		&&	fabs( vec1.z - vec2.z ) < epsylon
	;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T1, typename T2, typename T3 >
inline void clamp( T1 min, T2 max, T3& value )
{
	Assert( min <= max );
	
	if ( value > max )
	{
		value = T3( max );
		return;
	}
	else if ( value < min )
		value = T3( min );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
inline void replaceWithBigger( T & base, T New )
{
	if ( base < New )
		base = New;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
inline float magnitudeXZSquared( const TVec3& vec )
{
	return vec.x * vec.x  +  vec.z * vec.z;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
inline void vecMin( TVec3& vecResult, const TVec3& a, const TVec3& b  )
{
	vecResult.x = a.x - b.x;
	vecResult.y = a.y - b.y;
	vecResult.z = a.z - b.z;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
inline float magnitudeXZ( const TVec3& vec )
{
	return sqrt( magnitudeXZSquared( vec ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
inline void normalizeXZ( TVec3 & vec )
{
	float n = 1.f / returnCorrectDivider( sqrt( magnitudeXZSquared( vec ) ) );
	vec.x *= n;
	vec.z *= n;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
inline float getXZandYVectorAngle( const TVec3 & v )
{
	float xzVector = returnCorrectDivider( magnitudeXZ( v ) );
	if ( fabs( v.y ) < 0.0001f )
		return 0;
	else
		return Angle::atan( (float)v.y / xzVector ).getDegrees();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
inline float getXZFlatAngle_n180_180( const TVec3& vec )
{
	float angle = Angle::angleBetweenVectors3( vec, TVec3( 0.f, 0.f, 100.f ) ).getDegrees();

	if ( vec.x > 0 )
		return angle;
	else
		return - angle;

} // getXZFlatAngle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
inline float getXZFlatAngle_0_360( const TVec3& vec )
{
	float angle = getXZFlatAngle_n180_180 ( vec );
	if( angle < 0 )
		return angle + 360;
	else
		return angle;
}


/*----------------------------------------------------------*/


template< typename T1Vec3, typename T2Vec3 >
inline float // -180....+180
getXZAngleBetweenVectors( const T1Vec3 & vec1, const T2Vec3& vec2 )
{
	// rotate vec1 to the _2!
	float result = getXZFlatAngle_n180_180( vec2 ) - getXZFlatAngle_n180_180( vec1 );
	if( result <= -180 )
		result = 360 + result;
	
	else if ( result > 180 )
		result = result - 360;

	Assert( 180 >= result && result >= -180 );
	return result;
}


/*----------------------------------------------------------*/


template< typename TVec >
inline void rotateXZ( TVec& vec, float angle )
{
	float magXZ = magnitudeXZ( vec );
	float vecAngle = getXZFlatAngle_0_360( vec );

	vecAngle += angle;
	correctAngle_0_360( vecAngle );

	vec.x = magXZ * Angle::buildDegrees( vecAngle ).sin();
	vec.z = magXZ * Angle::buildDegrees( vecAngle ).cos();
}


/*----------------------------------------------------------*/


template< typename TVec >
inline void makeVecOrientationToAngle( TVec& vec, float angle )
{
	correctAngle_0_360( angle );
	float a = getXZFlatAngle_0_360( vec );
	a = angle - a;
	correctAngle_0_360( a );
	rotateXZ( vec, a );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline void correctAngle_0_360( T& angle )
{
	while ( angle < 0 )
		angle += 360;

	while ( angle >= 360 )
		angle -= 360;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T getRotationMinimalAngleFirstToSecond( T angle1, T angle2 )
{
	T result = angle2 - angle1;
	
	while ( result > 180 )
		result -= 360;

	while ( result <= -180 )
		result += 360;

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TRetVec, typename TInVec >
inline TRetVec toVector3( const TInVec& vec )
{
#pragma warning( disable : 4244 )

	TRetVec result;
	result.x = vec.x;
	result.y = vec.y;
	result.z = vec.z;

#pragma warning( default: 4244 )

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T rand( T begin, T end )
{
	Assert( begin <= end );
	T delta = end - begin;
	return ( ::rand() % int( delta * 4000 ) ) / returnCorrectDivider( 4000  + begin );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T3Vec >
inline bool isZero( const T3Vec& vec, float epsilon = 0.00001f )
{
	return fabs( vec.x ) + fabs ( vec.y ) + fabs( vec.z ) < epsilon;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline bool isZeroValue( const T& value, float epsilon = 0.00001f )
{
	return fabs( value ) < epsilon;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T3Vec >
inline float dotXZ ( const T3Vec & vec1, const T3Vec & vec2 )
{
	float dot = ( vec1.x * vec2.x + vec1.z * vec2.z );
	dot /= returnCorrectDivider( magnitudeXZ( vec1 ) * magnitudeXZ( vec2 ) );
	return dot;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T1Vec3, typename T2Vec3 >
inline float cosAngleXZ( const T1Vec3& vec1, const T2Vec3& vec2 )
{
	return dotXZ( vec1, vec2 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T3Vec >
inline T3Vec getReflectionXZ( const T3Vec & normal, const T3Vec & motion )
{
	return T3Vec(  -2 * ( dotXZ( normal, motion ) * normal ) + motion );
}



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T cosineInterpolation( T v1, T v2, T mu )
{
	// http://paulbourke.net/miscellaneous/interpolation/

	T one = 1;
	T two = 2;
	T mu2 = ( one - cos( mu * 3.1415926f ) ) / two;
	return v1 * ( one - mu2 ) + v2 * mu2;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3, typename T >
inline TVec3 cosineVector3Interpolation( const TVec3& vec1, const TVec3& vec2, T mu )
{
	// http://paulbourke.net/miscellaneous/interpolation/

	T one = 1;
	T two = 2;
	T mu2 = ( one - cos( mu * Pi ) ) / two;
	T r1 = one - mu2;

	return TVec3(
			vec1.x * r1	+	vec2.x * mu2
		,	vec1.y * r1	+	vec2.y * mu2
		,	vec1.z * r1	+	vec2.z * mu2
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
inline T linearInterpolation ( T a, T b, float mix )
{
	return a + ( b - a ) * mix; 
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline void checkNumericalOverflow( T v )
{
	const double max = 1000.f * 1000.f * 1000.f;

	Assert(
			v < max
		&&	v > -max
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
inline void checkNumericalOverflowV3( const TVec3& vec )
{
#if defined ZPEW_DEBUG

	checkNumericalOverflow( vec.x );
	checkNumericalOverflow( vec.y );
	checkNumericalOverflow( vec.z );

#endif // ZPEW_DEBUG
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T sign( T T )
{
	if ( T > 0 )
		return 1;

	if ( T < 0 )
		return -1;

	return 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// a == b
inline bool cmpEqual ( float a, float b, float eps )
{
	Assert( eps > 0 );
	return fabs( a-b ) < eps;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

// a < b
inline bool cmpSmaller( float a, float b, float eps )
{
	Assert( eps > 0 );
	return b - a > -eps;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// a > b
inline bool cmpBigger( float a, float b, float eps )
{
	Assert( eps > 0 );
	return a - b > -eps;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec >
inline TVec vectorXFlip( const TVec& t )
{
	return TVec( - t.x, t.y, t.z );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline float random( float min, float max )
{
	Assert( min <= max );
	float r = ::rand() / ( float )RAND_MAX;
	r *= max - min;
	r += min;
	return r;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TQuaternion, typename TVec3 >
inline TQuaternion buildQuat( Angle a, const TVec3& v )
{
	TQuaternion q;
	a.div(2);
	float asin = a.sin();
	
	q.x = v.x * asin;
	q.y = v.y * asin;
	q.z = v.z * asin;
	q.w = a.cos();

	float n = 1.f / sqrt( q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w );
	q.x = q.x * n;
	q.y = q.y * n;
	q.z = q.z * n;
	q.w = q.w * n;

	return q;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
bool isZeroXZ( const TVec3& vec, float epsylon = 0.00000000001 )
{
	return fabs( vec.x ) + fabs( vec.z ) < epsylon;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// e - errata ( indirect vector,  <(e,i) = 90 )
// i - initiated vector ( direct )
// v - origin base vector
// v = i*L + e*k

template < typename Vec3 >
inline void decomposeVec3XZ( const Vec3& v, const Vec3& i, const Vec3& e, float& L, float& K, float errataInitiatedXZExpectedAngle = 90.0f )
{

#ifdef ZPEW_DEBUG
	//-180..180
	float a = getXZAngleBetweenVectors( i, e );
	Assert( errataInitiatedXZExpectedAngle > 0 );
	Assert(
			( a > 0.2f && a < 179.8f )
		||	( a > -179.8f && a < -0.2f )
	);
	Assert( i.x / i.z != e.x / e.z );
	Assert( cmpEqual( fabs( a ), errataInitiatedXZExpectedAngle, 0.05f ) );
#endif // ZPEW_DEBUG

	float r = e.x * i.z - e.z * i.x;
	r = returnCorrectDivider( r );
	L = e.x * v.z - e.z * v.x;
	L /= r;

	K = v.x * i.z - v.z * i.x;
	K /= r;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T minimal( T a, T b )
{
	if ( a < b )
		return a;
	return b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
inline T maximal( T a, T b )
{
	if ( a > b )
		return a;
	return b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_UTILITY_HPP__
