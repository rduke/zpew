#ifndef __ZPEW_AUTO_CONFIG_HPP__
#define __ZPEW_AUTO_CONFIG_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <string>
#include "Configuration.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define ZpewRealAccurancyReal 0
#define ZpewRealAccurancyDouble 1
#define ZpewCompilerUnknown 0
#define ZpewCompilerMSVC    1
#define ZpewCompilerGNUC    2

#define ZpewPlatformUnknown 0
#define ZpewPlatformWindows 1
#define ZpewPlatformLinux   2

#define ZpewArchitecture32Bit 32
#define ZpewArchitecture64Bit 64

//#define ZPEW_LIBRARY_OPTIONS
#define ZPEW_DYNAMIC_LIBRARY 0
#define ZPEW_STATIC_LIBRARY  1

//#define ZPEW_DYMANIC_LIBRARY_OPTIONS
#define ZPEW_DYNAMIC_LIBRARY_OPTIONS_EXPORT 0
#define ZPEW_DYNAMIC_LIBRARY_OPTIONS_IMPORT 1

//#define ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL
#define ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_RETURN_NULL 0
#define ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_THROW 1

//#define ZPEW_DSC_DEFAULT_CONFIG 0
#define ZPEW_DSC_AUTO_DEFAULT_CONFIG 0
#define ZPEW_DSC_MANUAL_DEFAULT_CONFIG 1

#define ZPEW_OBJECT_DESCRIPTION_WITHOUT_CHECK  0
#define ZPEW_OBJECT_DESCRIPTION_QUICK_CHECK    1
#define ZPEW_OBJECT_DESCRIPTION_COMPLETE_CHECK 2

#define ZPEW_STRING2( _ZPEW_STRING_ ) #_ZPEW_STRING_
#define ZPEW_TO_STRING( _ZPEW_STRING_ ) ZPEW_STRING2( _ZPEW_STRING_ )


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ZmoCompiler == ZmoCompilerMSVC
	#define ZPEW_WARNING( _ZPEW_WARNING_ ) __pragma( warning( _ZPEW_WARNING_ ) )
	#define ZPEW_MESSAGE( _ZPEW_MESSAGE_ ) __pragma( message( _ZPEW_MESSAGE_ ) )
	#define ZPEW_WARNING_MESSAGE( _ZPEW_WARNING_MESSAGE_ ) \
		ZPEW_MESSAGE( "warning from Zmo library " __FILE__ "[" ZPEW_TO_STRING( __LINE__ ) "] : " _ZPEW_WARNING_MESSAGE_ )

	#define ZPEW_ERROR( _ZPEW_ERROR_TEXT_ ) __pragma( error( _ZPEW_ERROR_ ) )


#elif ZmoCompiler == ZmoCompilerGNUC
	#define ZPEW_WARNING( _ZPEW_WARNING_TEXT_ ) __warning( _ZPEW_WARNING_TEXT_ )
	#define ZPEW_MESSAGE( _ZPEW_NOTIFICATION_TEXT_ ) __message( _ZPEW_NOTIFICATION_TEXT_ )
	#define ZPEW_ERROR( _ZPEW_ERROR_TEXT_ ) __error( _ZPEW_ERROR_TEXT_ )
#else
	#define ZPEW_WARNING( _ZPEW_WARNING_TEXT_ ) { }
	#define ZPEW_MESSAGE( _ZPEW_NOTIFICATION_TEXT_ ) { }
	#define ZPEW_ERROR( _ZPEW_ERROR_TEXT_ ) { }
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define CRITICAL_THROW								\
	std::string str( "Critical error occured " );	\
	str += (const char*)(__FILE__);					\
	str += "  ";									\
	str += (const char*)(__LINE__);					\
	throw std::exception( str.c_str() );					
	/*-----~-----~-----~-----*/

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ZmoCompiler == ZmoCompilerMSVC
	#include <windows.h>
	#define ZPEW_CRITICAL_MESSAGE( _MSG )				\
		MessageBox(										\
				NULL									\
			,	(LPCWSTR)(_MSG)							\
			,	(LPCWSTR)"A critical state occured"		\
			,	MB_OK | MB_ICONERROR | MB_TASKMODAL		\
		);												\
		CRITICAL_THROW									
		/*-----~-----~-----~-----*/

#elif	
	#define ZPEW_CRITICAL_MESSAGE( _MSG )									\
		std::cerr << "A critical state occured: " << (_MSG) << std::endl;	\
		CRITICAL_THROW
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if defined ( _MSC_VER )
	#define ZpewCompiler ZpewCompilerMSVC
#elif defined ( __GNUC__ )
	#define ZpewCompiler ZpewCompilerGNUC
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if defined (_WIN32) || defined (__WIN32)
	#define ZpewPlatform ZpewPlatformWindows
	#define NOMINMAX
#elif defined (LINUX) || defined(_LINUX)
	#define ZpewPlatform ZpewPlatformLinux
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if defined( _M_X64 ) || defined ( _M_X64 )
	#define ZpewArchitecture ZpewArchitecture64Bit
#else
	#define ZpewArchitecture ZpewArchitecture32Bit
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if !defined( ZPEW_LIBRARY_OPTIONS )
	ZPEW_MESSAGE( "No library option was chosen Zpew will be compiled as static library" )
	#define ZPEW_LIBRARY_OPTIONS ZPEW_STATIC_LIBRARY
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if !defined( ZPEW_DSC_DEFAULT_CONFIG )
	ZPEW_MESSAGE( "defaultConfig inside dsc constructor" )
	#define ZPEW_DSC_DEFAULT_CONFIG ZPEW_AUTO_DEFAULT_CONFIG
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if !defined( ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL )
	ZPEW_MESSAGE( "vehicle factory invalid description protocol default throw" )
	#define ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL ZPEW_VEHICLE_FACTORY_INVALID_DESCRIPTION_THROW
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ZPEW_LIBRARY_OPTIONS == ZPEW_DYNAMIC_LIBRARY 
	#if ZPEW_DYNAMIC_LIBRARY_OPTIONS == ZPEW_DYNAMIC_LIBRARY_OPTIONS_EXPORT
		#define ZpewPublicClass         __declspec( dllexport )
		#define ZpewPublicFunction      __declspec( dllexport )
		#define ZpewPublicTemplateClass
	#elif ZPEW_DYNAMIC_LIBRARY_OPTIONS == ZPEW_DYNAMIC_LIBRARY_OPTIONS_IMPORT
		#define ZpewPublicClass         __declspec( dllimport )
		#define ZpewPublicFunction      __declspec( dllimport )
		#define ZpewPublicTemplateClass
	#else
		#error Dynamic Library export/import options undefined!
	#endif
#elif ZPEW_LIBRARY_OPTIONS == ZPEW_STATIC_LIBRARY
	#define ZpewPublicClass
	#define ZpewPublicFunction      
	#define ZpewPublicTemplateClass
#else
	#error Library type undefined!
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ( ZpewCompiler == ZpewCompilerMSVC )
	#define ZpewForceInline __forceinline
#endif 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ( ZpewCompiler == ZpewCompilerGNUC )
#	define ZpewForceInline __inline
#	define ZpewPublicClass 
#	define ZpewPublicFunction
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if defined( _DEBUG ) || defined( DEBUG )
	//#ifndef ZPEW_DEBUG
		#define ZPEW_DEBUG
		#define _ZPEW_DEBUG
	//#endif
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if ZpewRealAccurancy == ZpewRealAccurancyReal
	namespace Zpew { typedef float Real; }
#elif ZpewRealAccuray == ZpewRealAccurancyDouble
	namespace Zpew { typedef double Real; }
#else
	#error Realing point accuracy not specified
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//if uncommented - concurrent simulation of CC's enabled ( OMP )
//#define ZPEW_USE_CONCURRENT_CC_SIMULATION


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#ifdef BUILD_PHYSX

#include <PxPhysicsAPI.h>
#include <characterkinematic/PxController.h>
#include <extensions/PxDefaultSimulationFilterShader.h>
#include <foundation/PxMat33.h>

typedef const physx::PxVec3& InVec3;

	#if	( defined( WIN32 ) || defined( WIN32 ) ) && !( defined( WIN64 ) || defined ( _WIN64 ) )
		#if defined ZPEW_DEBUG
			#pragma comment ( lib, "PxTaskCHECKED.lib" )
			#pragma comment ( lib, "PhysX3CommonCHECKED_x86.lib" )
			#pragma comment ( lib, "PhysX3CHECKED_x86.lib" )
			#pragma comment ( lib, "PhysX3CookingCHECKED_x86.lib" )	
			#pragma comment ( lib, "PhysX3CharacterKinematicCHECKED_x86.lib" )
			#pragma comment ( lib, "PhysX3ExtensionsCHECKED.lib" )
			#pragma comment ( lib, "PhysX3VehicleCHECKED.lib" )
			#pragma comment ( lib, "RepX3CHECKED.lib" )
			#pragma comment ( lib, "RepXUpgrader3CHECKED.lib" )
			#pragma comment ( lib, "PhysXProfileSDKCHECKED.lib" )	
			#pragma comment ( lib, "PhysXVisualDebuggerSDKCHECKED.lib" )
		#else
			#pragma comment ( lib, "PxTask.lib" )
			#pragma comment ( lib, "PhysX3Common_x86.lib" )
			#pragma comment ( lib, "PhysX3_x86.lib" )
			#pragma comment ( lib, "PhysX3Cooking_x86.lib" )	
			#pragma comment ( lib, "PhysX3CharacterKinematic_x86.lib" )
			#pragma comment ( lib, "PhysX3Extensions.lib" )
			#pragma comment ( lib, "PhysX3Vehicle.lib" )
			#pragma comment ( lib, "RepX3.lib" )
			#pragma comment ( lib, "RepXUpgrader3.lib" )
			#pragma comment ( lib, "PhysXProfileSDK.lib" )	
			#pragma comment ( lib, "PhysXVisualDebuggerSDK.lib" )
		#endif

	#else if defined( WIN64 ) || defined ( _WIN64 )
		#if defined ZPEW_DEBUG
			#pragma comment ( lib, "PxTaskCHECKED.lib" )
			#pragma comment ( lib, "PhysX3CommonCHECKED_x64.lib" )
			#pragma comment ( lib, "PhysX3CHECKED_x64.lib" )
			#pragma comment ( lib, "PhysX3CookingCHECKED_x64.lib" )	
			#pragma comment ( lib, "PhysX3CharacterKinematicCHECKED_x64.lib" )
			#pragma comment ( lib, "PhysX3ExtensionsCHECKED.lib" )
			#pragma comment ( lib, "PhysX3VehicleCHECKED.lib" )
			#pragma comment ( lib, "RepX3CHECKED.lib" )	
			#pragma comment ( lib, "RepXUpgrader3CHECKED.lib" )
			#pragma comment ( lib, "PhysXProfileSDKCHECKED.lib" )	
			#pragma comment ( lib, "PhysXVisualDebuggerSDKCHECKED.lib" )
		#else
			#pragma comment ( lib, "PxTask.lib" )
			#pragma comment ( lib, "PhysX3Common_x64.lib" )
			#pragma comment ( lib, "PhysX3_x64.lib" )
			#pragma comment ( lib, "PhysX3Cooking_x64.lib" )	
			#pragma comment ( lib, "PhysX3CharacterKinematic_x64.lib" )
			#pragma comment ( lib, "PhysX3Extensions.lib" )
			#pragma comment ( lib, "PhysX3Vehicle.lib" )
			#pragma comment ( lib, "RepX3.lib" )	
			#pragma comment ( lib, "RepXUpgrader3.lib" )
			#pragma comment ( lib, "PhysXProfileSDK.lib" )	
			#pragma comment ( lib, "PhysXVisualDebuggerSDK.lib" )
		#endif
	#endif

#endif // BUILD_PHYSX


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#ifdef BUILD_BULLET

#include <btBulletDynamicsCommon.h>

	#if	( defined( WIN32 ) || defined( WIN32 ) ) && !( defined( WIN64 ) || defined ( _WIN64 ) )
		#if defined ZPEW_DEBUG
			#pragma comment ( lib, "BulletCollision_vs2010_debug.lib" )
			#pragma comment ( lib, "BulletDynamics_vs2010_debug.lib" )
			#pragma comment ( lib, "LinearMath_vs2010_debug.lib" )
		#else
			#pragma comment ( lib, "BulletCollision_vs2010.lib" )
			#pragma comment ( lib, "BulletDynamics_vs2010.lib" )
			#pragma comment ( lib, "LinearMath_vs2010.lib" )
		#endif
	#else if defined( WIN64 ) || defined ( _WIN64 )
		#if defined ZPEW_DEBUG
			#pragma comment ( lib, "BulletCollision_vs2010_debug.lib" )
			#pragma comment ( lib, "BulletDynamics_vs2010_debug.lib" )
			#pragma comment ( lib, "LinearMath_vs2010_debug.lib" )
		#else
			#pragma comment ( lib, "BulletCollision_vs2010.lib" )
			#pragma comment ( lib, "BulletDynamics_vs2010.lib" )
			#pragma comment ( lib, "LinearMath_vs2010.lib" )
		#endif
	#endif

#endif // BUILD_BULLET


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



#endif // __ZPEW_AUTO_CONFIG_HPP__