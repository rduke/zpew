#ifndef __ZPEW_PCH_HPP__
#define __ZPEW_PCH_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <hash_set>
#include <algorithm>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define BUILD_BULLET
#define BUILD_PHYSX

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Common/AutoConfiguration.hpp"

#include "ZpewSources/Common/Basement/Optional.hpp"
#include "ZpewSources/Common/Basement/RangeAcceleratingValue.hpp"
#include "ZpewSources/Common/Basement/InertValue.hpp"
#include "ZpewSources/Common/Basement/Switch.hpp"
#include "ZpewSources/Common/Basement/Utility.hpp"
#include "ZpewSources/Common/Basement/CosineInterpolationCurve.hpp"
#include "ZpewSources/Common/Basement/Angle.hpp"
#include "ZpewSources/Common/Basement/Rotation.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PCH_HPP__
