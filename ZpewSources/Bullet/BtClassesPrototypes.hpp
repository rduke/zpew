#ifndef __ZPEW_BTCLASSESPROTOTYPES_HPP__
#define __ZPEW_BTCLASSESPROTOTYPES_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btBroadphaseInterface;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
class btRigidBody;
class btRaycastVehicle;
class btVehicleTuning;
struct btVehicleRaycaster;
class btCollisionShape;
class btKinematicCharacterController;
class btPairCachingGhostObject;
class btConvexShape;
class btGhostPairCallback;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_BTCLASSESPROTOTYPES_HPP__
