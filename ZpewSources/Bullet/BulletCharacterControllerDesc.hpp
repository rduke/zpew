#ifndef __ZPEW_BULLETCHARACTERCONTROLLERDESC_HPP__
#define __ZPEW_BULLETCHARACTERCONTROLLERDESC_HPP__

#include "BulletCommon.hpp"

namespace Zpew
{

namespace bullet
{

class  CharacterControllerDesc
{
public:
	void defaultConfig();

public:
	btConvexShape	*mConvexShape;
	btScalar		 mStepHeight;
	btScalar		 mMaxJumpHeight;
	int				 mUpAxis;
	btTransform		 mWorldTransform;
	Scene			*mScene;
	btScalar		 mWalkVelocity;
	btScalar		 mRotationVelocity;
}; // class CharacterControllerDesc

} // namespace bullet

} // namespace Zpew

#endif // __ZPEW_BULLETCHARACTERCONTROLLERDESC_HPP__
