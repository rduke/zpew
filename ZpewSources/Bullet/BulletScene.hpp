#ifndef __ZPEW_BULLETSCENE_HPP__
#define __ZPEW_BULLETSCENE_HPP__

#include "ZpewSources/Common/AutoConfiguration.hpp"
#include "BtClassesPrototypes.hpp"

namespace Zpew
{

namespace bullet
{

class  Scene
{
public:
	Scene();
	~Scene();

	btDiscreteDynamicsWorld* getDiscreteDynamicsWorld() const;
	void addCollisionShape( btCollisionShape *_collisionShape );
	btRigidBody* localCreateRigidBody( Real _mass,
									   const btTransform &_startTransform,
									   btCollisionShape *_shape );

private:
	btDefaultCollisionConfiguration		*mDefaultCollisionConfiguration;
	btCollisionDispatcher				*mCollisionDispatcher;
	btBroadphaseInterface				*mOverlappingPairCache;
	btSequentialImpulseConstraintSolver	*mSequentialImpulseConstraintSolver;
	btDiscreteDynamicsWorld				*mDiscreteDynamicsWorld;
	btScalar							 mDefaultContactProcessingThreshold;
	btAlignedObjectArray< btCollisionShape* > mCollisionShapes;
	btGhostPairCallback					*mGhostPairCallBack;
}; // class Scene

} // namespace bullet

} // namespace Zpew

#endif // __ZPEW_BULLETSCENE_HPP__
