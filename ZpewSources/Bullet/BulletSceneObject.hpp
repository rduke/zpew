#ifndef __ZPEW_BULLETSCENEOBJECT_HPP__
#define __ZPEW_BULLETSCENEOBJECT_HPP__


#include "ZpewSources/Common/AutoConfiguration.hpp"


#include "BtClassesPrototypes.hpp"
#include "BulletClassesPrototypes.hpp"


namespace Zpew
{

namespace bullet
{

class  SceneObject
{
public:
	SceneObject( Scene *_scene );
	Scene* getScene() const;

private:
	Scene *mScene;
}; // class SceneObject

} // namespace bullet

} // namespace Zpew

#endif // __ZPEW_BULLETSCENEOBJECT_HPP__
