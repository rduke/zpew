#ifndef __ZPEW_BULLETVEHICLECONTROLLER_HPP__
#define __ZPEW_BULLETVEHICLECONTROLLER_HPP__

#include "BulletCommon.hpp"
#include "BulletVehicleControllerDesc.hpp"
#include "BulletSceneObject.hpp"

namespace Zpew
{

namespace bullet
{

class VehicleController
	: public SceneObject
{
public:
	VehicleController( VehicleControllerDesc *_desc );
	void initPhysics();
	void reset();
	~VehicleController();

	btRaycastVehicle* getRaycastVehicle() const;
	btCollisionShape* getWheelShape() const;
	btRigidBody*	  getChassis() const;

private:
	typedef btRaycastVehicle::btVehicleTuning btRaycastVehicleTuning;
	btRigidBody				*mChassis;
	btRaycastVehicleTuning	 mTuning;
	btVehicleRaycaster		*mVehicleRayCaster;
	btRaycastVehicle		*mRayCastVehicle;
	btCollisionShape		*mWheelShape;
}; // class VehicleController

} // namespace bullet

} // namespace Zpew

#endif // __ZPEW_BULLETVEHICLECONTROLLER_HPP__
