#ifndef __ZPEW_BULLETVEHICLECONTROLLERDESC_HPP__
#define __ZPEW_BULLETVEHICLECONTROLLERDESC_HPP__

#include "BulletCommon.hpp"

namespace Zpew
{

namespace bullet
{


class VehicleControllerDesc
{
public:
	void defaultConfig();

public:
	Scene			*mScene;
	btTransform		 mWorldTransform;

}; // class VehicleControllerDesc


} // namespace bullet


} // namespace Zpew

#endif // __ZPEW_BULLETVEHICLECONTROLLERDESC_HPP__
