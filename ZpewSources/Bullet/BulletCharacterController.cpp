#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletCharacterController.hpp"
#include "BulletCharacterControllerDesc.hpp"
#include "BulletScene.hpp"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "BulletDynamics/Character/btKinematicCharacterController.h"


namespace Zpew
{

namespace bullet
{

CharacterController::CharacterController( CharacterControllerDesc *_desc )
	: SceneObject( _desc->mScene ),
	  mKinematicCharacterController( NULL ),
	  mPairCachingGhostObject( NULL ),
	  mDt( 0 ),
	  mWalkDirection( 0, 0, 0 )
{
	mPairCachingGhostObject = new btPairCachingGhostObject();
	mPairCachingGhostObject->setWorldTransform( _desc->mWorldTransform );
	mPairCachingGhostObject->setCollisionShape( _desc->mConvexShape );
	mPairCachingGhostObject->setCollisionFlags( btCollisionObject::CF_CHARACTER_OBJECT );

	mConvexShape = _desc->mConvexShape;
	_desc->mConvexShape = NULL;

	mWalkVelocity		= _desc->mWalkVelocity;
	mUpAxis				= _desc->mUpAxis;
	mRotationVelocity	= _desc->mRotationVelocity;

	mKinematicCharacterController = new btKinematicCharacterController( mPairCachingGhostObject,
																		mConvexShape,
																		_desc->mStepHeight );

	mKinematicCharacterController->setMaxJumpHeight( _desc->mMaxJumpHeight );
	mKinematicCharacterController->setUpAxis( _desc->mUpAxis );

		///only collide with static for now (no interaction with dynamic objects)
	getScene()->
		getDiscreteDynamicsWorld()->
			addCollisionObject( mPairCachingGhostObject,
								btBroadphaseProxy::CharacterFilter,
								btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter );

	getScene()->getDiscreteDynamicsWorld()->addAction( mKinematicCharacterController );
}


void CharacterController::update( btScalar _dt )
{
	mDt = _dt;
	mKinematicCharacterController->setWalkDirection( mWalkDirection );

	// We set the direction vector to ZERO for the next step
	// to stop walking the character
	mWalkDirection.setZero();
}


void CharacterController::walk( btVector3 &_direction )
{
	btVector3 walkDirection	= btVector3( 0, 0 , 0 );
	btScalar walkSpeed		= mWalkVelocity * mDt;

	mWalkDirection =  _direction * walkSpeed;
}


void CharacterController::walkForward()
{
	btTransform xform = mPairCachingGhostObject->getWorldTransform();

	btVector3 forwardDir	= xform.getBasis()[2];
	forwardDir.normalize();
	
	walk( forwardDir );
}


void CharacterController::walkBackward()
{
	btTransform xform = mPairCachingGhostObject->getWorldTransform();

	btVector3 forwardDir	= xform.getBasis()[2];
	forwardDir.normalize();
	
	walk( -forwardDir );
}


void CharacterController::strafeLeft()
{
	btTransform xform = mPairCachingGhostObject->getWorldTransform();
	btVector3 strafeDir = xform.getBasis()[0];

	strafeDir.normalize ();
	
	walk( strafeDir );
}


void CharacterController::strafeRight()
{
	btTransform xform = mPairCachingGhostObject->getWorldTransform();
	btVector3 strafeDir = xform.getBasis()[0];

	strafeDir.normalize ();
	
	walk( -strafeDir );
}


void CharacterController::rotate( btScalar _radian )
{
	btVector3 direction = btVector3( 0, 0, 0 );
	direction[mUpAxis] = 1.0f;
	btMatrix3x3 orn = mPairCachingGhostObject->getWorldTransform().getBasis();
	orn *= btMatrix3x3( btQuaternion( direction, _radian * mDt ) );
	mPairCachingGhostObject->getWorldTransform().setBasis(orn);
}


void CharacterController::rotateLeft()
{
	rotate( -mRotationVelocity );
}


void CharacterController::rotateRight()
{
	rotate( mRotationVelocity );
}


void CharacterController::jump()
{
	mKinematicCharacterController->jump();
}


bool CharacterController::canJump()
{
	return mKinematicCharacterController->canJump();
}


btKinematicCharacterController* CharacterController::getKinematicCharacterController() const
{
	return mKinematicCharacterController;
}


btPairCachingGhostObject* CharacterController::getPairCachingGhostObject() const
{
	return mPairCachingGhostObject;
}


btScalar CharacterController::getWalkVelocity() const
{
	return mWalkVelocity;
}


int CharacterController::getUpAxis() const
{
	return mUpAxis;
}


btScalar CharacterController::getRotationVelocity() const
{
	return mRotationVelocity;
}


void CharacterController::setWalkVelocity( btScalar _velocity )
{
	mWalkVelocity = _velocity;
}


void CharacterController::setUpAxis( int _axis )
{
	if( _axis < 0 )
		_axis = 0;
	if( _axis > 2 )
		_axis = 2;
	
	mUpAxis = _axis;
	mKinematicCharacterController->setUpAxis( _axis );
}


void CharacterController::setRotationVelocity( btScalar _velocity )
{
	mRotationVelocity = _velocity;
}


} // namespace bullet


} // namespace Zpew
