#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletSceneObject.hpp"

namespace Zpew
{

namespace bullet
{

SceneObject::SceneObject( Scene *_scene )
	: mScene( _scene )
{
}

Scene* SceneObject::getScene() const
{
	return mScene;
}

} // namespace bullet

} // namespace Zpew