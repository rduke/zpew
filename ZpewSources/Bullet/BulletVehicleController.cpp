#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletVehicleController.hpp"
#include "BulletScene.hpp"

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

namespace Zpew
{

namespace bullet
{

const int maxProxies = 32766;
const int maxOverlap = 65535;

///btRaycastVehicle is the interface for the constraint that implements the raycast vehicle
///notice that for higher-quality slow-moving vehicles, another approach might be better
///implementing explicit hinged-wheel constraints with cylinder collision, rather then raycasts
Real	gEngineForce = 0.f;
Real	gBreakingForce = 0.f;

Real	maxEngineForce = 1000.f;//this should be engine/velocity dependent
Real	maxBreakingForce = 100.f;

Real	gVehicleSteering = 0.f;
Real	steeringIncrement = 0.04f;
Real	steeringClamp = 0.3f;
Real	wheelRadius = 0.5f;
Real	wheelWidth = 0.4f;
Real	wheelFriction = 1000;//BT_LARGE_FLOAT;
Real	suspensionStiffness = 20.f;
Real	suspensionDamping = 2.3f;
Real	suspensionCompression = 4.4f;
Real	rollInfluence = 0.1f;//1.0f;
Real	chassisMass	=	1500;


btScalar suspensionRestLength( 0.6f );

#define CUBE_HALF_EXTENTS 1

#ifdef FORCE_ZAXIS_UP
		int rightIndex = 0; 
		int upIndex = 2; 
		int forwardIndex = 1;
		btVector3 wheelDirectionCS0(0,0,-1);
		btVector3 wheelAxleCS(1,0,0);
#else
		int rightIndex = 0;
		int upIndex = 1;
		int forwardIndex = 2;
		btVector3 wheelDirectionCS0(0,-1,0);
		btVector3 wheelAxleCS(-1,0,0);
#endif

VehicleController::VehicleController( VehicleControllerDesc *_desc )
	:	SceneObject( _desc->mScene ),
		mChassis( NULL ),
		mRayCastVehicle( NULL ),
		mWheelShape( NULL )
{
	initPhysics();
}

VehicleController::~VehicleController()
{
	delete mVehicleRayCaster;
	delete mRayCastVehicle;
	delete mWheelShape;
}


void VehicleController::initPhysics()
{

	btCollisionShape* chassisShape = new btBoxShape(btVector3(1.f,0.5f,2.f));
	getScene()->addCollisionShape( chassisShape );

	btCompoundShape* compound = new btCompoundShape();
	getScene()->addCollisionShape( compound );
	
	btTransform localTrans;
	localTrans.setIdentity();
	//localTrans effectively shifts the center of mass with respect to the chassis
	localTrans.setOrigin(btVector3(0,1,0));

	compound->addChildShape(localTrans,chassisShape);

	btTransform tr;
	tr.setIdentity();
	tr.setOrigin( btVector3( 0, 0.f, 0 ) );

	mChassis = getScene()->localCreateRigidBody( chassisMass, tr, compound );
	mWheelShape = new btCylinderShapeX( btVector3( wheelWidth, wheelRadius, wheelRadius ) );
	
	reset();

	mVehicleRayCaster = new btDefaultVehicleRaycaster( getScene()->getDiscreteDynamicsWorld() );
	mRayCastVehicle = new btRaycastVehicle( mTuning, mChassis, mVehicleRayCaster );
		
	///never deactivate the vehicle
	mChassis->setActivationState( DISABLE_DEACTIVATION );

	getScene()->getDiscreteDynamicsWorld()->addVehicle( mRayCastVehicle );

		Real connectionHeight = 1.2f;

	
	bool isFrontWheel=true;

	//choose coordinate system
	mRayCastVehicle->setCoordinateSystem( rightIndex, upIndex, forwardIndex );

	btVector3 connectionPointCS0( CUBE_HALF_EXTENTS - ( 0.3f * wheelWidth ),
									connectionHeight,
									2 * CUBE_HALF_EXTENTS - wheelRadius );


	mRayCastVehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,mTuning,isFrontWheel);

	connectionPointCS0 = btVector3( -CUBE_HALF_EXTENTS + ( 0.3f * wheelWidth ),
										connectionHeight,
										2 * CUBE_HALF_EXTENTS - wheelRadius );


	mRayCastVehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,mTuning,isFrontWheel);

	connectionPointCS0 = btVector3( -CUBE_HALF_EXTENTS + ( 0.3f * wheelWidth ),
										connectionHeight,
									-2 * CUBE_HALF_EXTENTS + wheelRadius );

	isFrontWheel = false;
	mRayCastVehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,mTuning,isFrontWheel);

	connectionPointCS0 = btVector3( CUBE_HALF_EXTENTS - ( 0.3f * wheelWidth ),
									connectionHeight,
									-2 * CUBE_HALF_EXTENTS + wheelRadius );

	mRayCastVehicle->addWheel(connectionPointCS0,wheelDirectionCS0,wheelAxleCS,suspensionRestLength,wheelRadius,mTuning,isFrontWheel);
		
	for (int i=0;i<mRayCastVehicle->getNumWheels();i++)
	{
		btWheelInfo& wheel = mRayCastVehicle->getWheelInfo(i);
		wheel.m_suspensionStiffness = suspensionStiffness;
		wheel.m_wheelsDampingRelaxation = suspensionDamping;
		wheel.m_wheelsDampingCompression = suspensionCompression;
		wheel.m_frictionSlip = wheelFriction;
		wheel.m_rollInfluence = rollInfluence;
	}
}


void VehicleController::reset()
{
	gVehicleSteering = 0.f;
	mChassis->setCenterOfMassTransform(btTransform::getIdentity());
	mChassis->setLinearVelocity(btVector3(0,0,0));
	mChassis->setAngularVelocity(btVector3(0,0,0));

	getScene()->
		getDiscreteDynamicsWorld()->
		getBroadphase()->
		getOverlappingPairCache()->
		cleanProxyFromPairs( mChassis->getBroadphaseHandle(),
							 getScene()->getDiscreteDynamicsWorld()->getDispatcher() );

	if (mRayCastVehicle)
	{
		mRayCastVehicle->resetSuspension();
		for ( int i = 0; i < mRayCastVehicle->getNumWheels(); i++ )
		{
			//synchronize the wheels with the (interpolated) chassis worldtransform
			mRayCastVehicle->updateWheelTransform( i, true );
		}
	}
}


btRaycastVehicle* VehicleController::getRaycastVehicle() const
{
	return mRayCastVehicle;
}

btCollisionShape* VehicleController::getWheelShape() const
{
	return mWheelShape;
}

btRigidBody* VehicleController::getChassis() const
{
	return mChassis;
}


} // namespace bullet

} // namespace Zpew