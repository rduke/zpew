#ifndef __ZPEW_BULLETCOMMON_HPP__
#define __ZPEW_BULLETCOMMON_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "BtClassesPrototypes.hpp"
#include "BulletClassesPrototypes.hpp"
#include <LinearMath/btScalar.h>
#include <LinearMath/btTransform.h>
#include "ZpewSources/Common/AutoConfiguration.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_BULLETCOMMON_HPP__
