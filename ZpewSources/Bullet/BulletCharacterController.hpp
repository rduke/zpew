#ifndef __ZPEW_BULLETCHARACTERCONTROLLER_HPP__
#define __ZPEW_BULLETCHARACTERCONTROLLER_HPP__

#include "BtClassesPrototypes.hpp"
#include "BulletClassesPrototypes.hpp"
#include "BulletSceneObject.hpp"
#include "ZpewSources/Common/AutoConfiguration.hpp"

namespace Zpew
{

namespace bullet
{

class  CharacterController
	: public SceneObject
{
public:
	CharacterController( CharacterControllerDesc *_desc );
	void walk( btVector3 &_displacement );
	void walkForward();
	void walkBackward();
	void strafeLeft();
	void strafeRight();
	void rotate( btScalar _radian );
	void rotateLeft();
	void rotateRight();
	void jump();
	bool canJump();
	void update( btScalar _dt );

	btKinematicCharacterController* getKinematicCharacterController() const;
	btPairCachingGhostObject*		getPairCachingGhostObject() const;
	btScalar						getWalkVelocity() const;
	int								getUpAxis() const;
	btScalar						getRotationVelocity() const;

	void							setWalkVelocity( btScalar _velocity );
	void							setRotationVelocity( btScalar _velocity );
	void							setUpAxis( int _axis );

private:
	btKinematicCharacterController	*mKinematicCharacterController;
	btPairCachingGhostObject		*mPairCachingGhostObject;
	btConvexShape					*mConvexShape;
	btScalar						 mDt;
	btScalar						 mWalkVelocity;
	btScalar						 mRotationVelocity;
	int								 mUpAxis;

	// Strictly for internal use
	btVector3						 mWalkDirection;

}; // class CharacterController

} // namespace bullet

} // namepsace Zpew

#endif // __ZPEW_BULLETCHARACTERCONTROLLER_HPP__
