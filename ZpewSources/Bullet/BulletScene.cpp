#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletScene.hpp"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

namespace Zpew
{

namespace bullet
{

Scene::Scene()
	:	mDefaultCollisionConfiguration( new btDefaultCollisionConfiguration() ),
		mCollisionDispatcher( new btCollisionDispatcher ( mDefaultCollisionConfiguration ) ),
		mOverlappingPairCache( new btDbvtBroadphase ), 
		mSequentialImpulseConstraintSolver( new btSequentialImpulseConstraintSolver ),
		mDiscreteDynamicsWorld( new btDiscreteDynamicsWorld( mCollisionDispatcher,
															 mOverlappingPairCache,
															 mSequentialImpulseConstraintSolver,
															 mDefaultCollisionConfiguration ) ),
		mDefaultContactProcessingThreshold( BT_LARGE_FLOAT )
{
	mDiscreteDynamicsWorld->setGravity( btVector3( 0, -10, 0 ) );
	mGhostPairCallBack = new btGhostPairCallback();
	mDiscreteDynamicsWorld->getPairCache()->setInternalGhostPairCallback( mGhostPairCallBack );
}

Scene::~Scene()
{
	// remove the rigidbodies from the dynamics world and delete them
	for (int i = mDiscreteDynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i-- )
	{
		btCollisionObject *obj = mDiscreteDynamicsWorld->getCollisionObjectArray()[ i ];
		btRigidBody *body = btRigidBody::upcast( obj );
		if ( body && body->getMotionState() )
		{
			delete body->getMotionState();
		}
		mDiscreteDynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	// delete collision shapes
	for ( int j = 0; j < mDiscreteDynamicsWorld->getCollisionObjectArray().size(); j++ )
	{
		btCollisionShape * shape = mCollisionShapes[ j ];
		mCollisionShapes[ j ] = 0;
		delete shape ;
	}

	delete mGhostPairCallBack;

	// delete dynamics world
	delete mDiscreteDynamicsWorld;

	// delete solver
	delete mSequentialImpulseConstraintSolver;

	// delete broadphase
	delete mOverlappingPairCache;

	// delete dispatcher
	delete mCollisionDispatcher;

	delete mDefaultCollisionConfiguration;

	// next line is optional : it will be cleared by the destructor when the array goes out of scope
	mCollisionShapes.clear();
}


btDiscreteDynamicsWorld* Scene::getDiscreteDynamicsWorld() const
{
	return mDiscreteDynamicsWorld;
}


void Scene::addCollisionShape( btCollisionShape *_collisionShape )
{
	mCollisionShapes.push_back( _collisionShape );
}


btRigidBody* Scene::localCreateRigidBody(Real mass, const btTransform& startTransform,btCollisionShape* shape)
{
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0,0,0);
	if (isDynamic)
		shape->calculateLocalInertia(mass,localInertia);

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

#define USE_MOTIONSTATE 1
#ifdef USE_MOTIONSTATE
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass,myMotionState,shape,localInertia);

	btRigidBody* body = new btRigidBody(cInfo);
	body->setContactProcessingThreshold(mDefaultContactProcessingThreshold);

#else
	btRigidBody* body = new btRigidBody(mass,0,shape,localInertia);	
	body->setWorldTransform(startTransform);
#endif//

	mDiscreteDynamicsWorld->addRigidBody( body );

	return body;
}

} // namespace bullet

} // namespace Zpew