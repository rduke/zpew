#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletWorld.hpp"
#include "BulletScene.hpp"
#include "BulletCharacterController.hpp"
#include "BulletVehicleController.hpp"


namespace Zpew
{

namespace bullet
{

World::World()
{

}

World::~World()
{
	for( int i = 0; i < mScenes.size() - 1; ++i )
	{
		Scene* scene = mScenes[ i ];
		delete scene;
	}
	
	for( int i = 0; i < mCharacterControllers.size() - 1; ++i )
	{
		CharacterController* characterController = mCharacterControllers[ i ];
		delete characterController;
	}

	for( int i = 0; i < mVehicleControllers.size(); i++ )
	{
		VehicleController *vehicleController = mVehicleControllers[ i ];
		delete vehicleController;
	}
}

Scene* World::createScene()
{
	Scene* scene = new Scene();
	mScenes.push_back( scene );

	return scene;
}

CharacterController* World::createCharacterController( CharacterControllerDesc *_desc )
{
	CharacterController* controller = new CharacterController( _desc );
	mCharacterControllers.push_back( controller );

	return controller;
}

VehicleController* World::createVehicleController( VehicleControllerDesc *_desc )
{
	VehicleController* controller = new VehicleController( _desc );
	mVehicleControllers.push_back( controller );

	return controller;
}

} // namespace bullet

} // namespace Zpew
