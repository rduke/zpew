#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletVehicleControllerDesc.hpp"


namespace Zpew
{

namespace bullet
{

void VehicleControllerDesc::defaultConfig()
{
	mWorldTransform.setIdentity();
	mWorldTransform.setOrigin( btVector3( 0, 10, 0 ) );
	mScene = NULL;
}

} // namespace bullet

} // namespace Zpew


