#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "BulletCharacterControllerDesc.hpp"


namespace Zpew
{

namespace bullet
{

void CharacterControllerDesc::defaultConfig()
{
	mWorldTransform.setIdentity();
	mWorldTransform.setOrigin ( btVector3( 0, 10, 0 ) );

	btScalar characterHeight= 1.75f;
	btScalar characterWidth	= 1.75f;
	btConvexShape* capsule = new btCapsuleShape( characterWidth, characterHeight );

	mConvexShape		= capsule;
	mStepHeight			= 0.35f;
	mUpAxis				= 1;
	mWalkVelocity		= 35.0f;
	mRotationVelocity	= 0.5f;
}

} // namespace bullet

} // namespace Zpew
