#ifndef __ZPEW_BULLET_WORLD_HPP__
#define __ZPEW_BULLET_WORLD_HPP__


#include "ZpewSources/Common/Singleton.hpp"
#include "BtClassesPrototypes.hpp"
#include "BulletClassesPrototypes.hpp"
#include "ZpewSources/Common/AutoConfiguration.hpp"

namespace Zpew
{

namespace bullet
{


class  World
	: public Singleton< World >
{
public:
	friend class Singleton< World >;
	Scene* createScene();
	CharacterController* createCharacterController( CharacterControllerDesc *_desc );
	VehicleController*	 createVehicleController( VehicleControllerDesc *_desc );

protected:
	World();
	~World();

private:
	typedef btAlignedObjectArray< Scene* >					SceneContainer;
	typedef btAlignedObjectArray< CharacterController* >	CharacterControllerContainer;
	typedef btAlignedObjectArray< VehicleController* >		VehicleControllerContainer;

	VehicleControllerContainer		mVehicleControllers;
	SceneContainer					mScenes;
	CharacterControllerContainer	mCharacterControllers;
}; // class World

} // namespace bullet
} // namespace Zpew


#endif // __ZPEW_BULLET_WORLD_HPP__
