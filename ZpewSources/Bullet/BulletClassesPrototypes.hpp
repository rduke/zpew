#ifndef __ZPEW_BULLETCLASSESPROTOTYPES_HPP__
#define __ZPEW_BULLETCLASSESPROTOTYPES_HPP__


namespace Zpew
{

namespace bullet
{

class World;
class Scene;
class SceneObject;
class VehicleController;
class VehicleControllerDesc;
class CharacterController;
class CharacterControllerDesc;

} // namespace bullet

} // namespace Zpew


#endif // __ZPEW_BULLETCLASSESPROTOTYPES_HPP__
