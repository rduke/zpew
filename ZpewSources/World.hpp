#ifndef __ZPEW_WORLD_HPP__
#define __ZPEW_WORLD_HPP__

#include "Common/Singleton.hpp"

#include <PxPhysics.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class World
{

public: World();

}; // class World

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_WORLD_HPP__
