#ifndef __ZPEW_PHYSX_World_HPP__
#define __ZPEW_PHYSX_World_HPP__

#include "ZpewSources/Common/Singleton.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class World
	:	public physx::debugger::comm::PvdConnectionHandler
{
public:
	
	World();
	virtual ~World();

	virtual void onPvdSendClassDescriptions( physx::debugger::comm::PvdConnection& connection );
	virtual void onPvdConnected( physx::debugger::comm::PvdConnection& connection );
	virtual void onPvdDisconnected( physx::debugger::comm::PvdConnection& connection );
	
	void togglePvdConnection();

public:

	PxTolerancesScale			& getDefaultToleranceScale()	{ return mDefaultToleranceScale;		}

	PxFoundation				& getFoundation()				{ return	*	mFoundation;			}
	PxProfileZoneManager		& getProfileZoneManager()		{ return	*	mProfileZoneManager;	}
	PxPhysics					& getPhysics()					{ return	*	mPhysics;				}
	PxControllerManager			& getControllerManager()		{ return	*	mControllerManager;		}
	pxtask::CudaContextManager	* getCudaContextManager()		{ return		mCudaContextManager;	}
	PxCooking					& getCooking()					{ return	*	mCooking;				}
	PxDefaultCpuDispatcher		* getCpuDispatcher()			{ return		mCpuDispatcher;			}
	pxtask::GpuDispatcher		* getGpuDispatcher()			{ return		mGpuDispatcher;			}
	PxMaterial					& getDefaultMaterial()			{ return	*	mDefaultMaterial;		}

	
	static PxFilterFlags getSimulationFilterCallback(
			PxFilterObjectAttributes attributes0
		,	PxFilterData filterData0
		,	PxFilterObjectAttributes attributes1
		,	PxFilterData filterData1
		,	PxPairFlags& pairFlags
		,	const void* constantBlock
		,	PxU32 constantBlockSize
	);

	bool isCorrect(){ return m_isCorrect; }
	
protected:	

	void createPvdConnection();
	void release();

private:

	bool m_isCorrect;

	PxTolerancesScale			mDefaultToleranceScale;

	PxFoundation				*mFoundation;
	PxProfileZoneManager		*mProfileZoneManager;
	PxPhysics					*mPhysics;
	PxControllerManager			*mControllerManager;
	pxtask::CudaContextManager	*mCudaContextManager;
	PxCooking					*mCooking;
	PxDefaultCpuDispatcher		*mCpuDispatcher;
	pxtask::GpuDispatcher		*mGpuDispatcher;
	PxMaterial					*mDefaultMaterial;
	PxFilterFlags				*mDegaultFilterFlags;

private:

	PxDefaultErrorCallback		mDefaultErrorCallback;
	PxDefaultAllocator			mDefaultAllocatorCallback;

}; // class World


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PHYSX_World_HPP__
