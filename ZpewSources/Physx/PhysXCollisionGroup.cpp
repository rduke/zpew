#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/PhysXCollisionGroup.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CollisionGroup::CollisionGroup()
{
	//init all group pairs as true:
	for ( unsigned i = 0; i < COLLISION_GROUPS_COUNT; i ++)
		mGroupCollisionFlags[ i ] = 0xFFFFffff;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CollisionGroup::setU32CollisionFlag(
		PxU32 firstGroupNumber
	,	PxU32 secondGroupNumber
	,	bool enable
)
{
	Assert(
			firstGroupNumber < COLLISION_GROUPS_COUNT
		&&	secondGroupNumber < COLLISION_GROUPS_COUNT
	);
	if ( enable )
	{
		//be symmetric:
		mGroupCollisionFlags[ firstGroupNumber  ] |= ( 1 << secondGroupNumber );
		mGroupCollisionFlags[ secondGroupNumber ] |= ( 1 << firstGroupNumber );
	}
	else
	{
		mGroupCollisionFlags[ firstGroupNumber  ] &= ~( 1 << secondGroupNumber );
		mGroupCollisionFlags[ secondGroupNumber ] &= ~( 1 << firstGroupNumber );
	}

} // CollisionGroup::setU32CollisionFlag

	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
