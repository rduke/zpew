#ifndef __ZPEW_PhysX_COLLISION_GROUP_HPP__
#define __ZPEW_PhysX_COLLISION_GROUP_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define COLLISION_GROUPS_COUNT 32

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class CollisionGroup
{

public:

	CollisionGroup();

	void setU32CollisionFlag(
			PxU32 firstGroupNumber
		,	PxU32 secondGroupNumber
		,	bool enable
	);

	PxU32 * getCollisionGroups()		{ return mGroupCollisionFlags; }
	int getCollisionGroupsSizeBytes()	{ return COLLISION_GROUPS_COUNT * sizeof( PxU32 ); }

private:

	PxU32 mGroupCollisionFlags[ COLLISION_GROUPS_COUNT ];


}; // class CollisionGroup


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PhysX_COLLISION_GROUP_HPP__
