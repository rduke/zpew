#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CCBroadcaster.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define ALL_CALL_BODY( _CALL_NAME )											\
void																		\
CCBroadcaster::_CALL_NAME()													\
{																			\
	std::set< AbstractCC * >::iterator begin = mInputSet.begin();			\
	std::set< AbstractCC * >::iterator end = mInputSet.end();				\
	while( begin != end )													\
	{																		\
		(*begin)->_CALL_NAME();												\
		++begin;															\
	}																		\
}																			\


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


ALL_CALL_BODY(strafeLeftOn);
ALL_CALL_BODY(strafeLeftOff);

ALL_CALL_BODY(strafeRightOn);
ALL_CALL_BODY(strafeRightOff);

ALL_CALL_BODY(moveForwardOn);
ALL_CALL_BODY(moveForwardOff);

ALL_CALL_BODY(moveBackwardOn);
ALL_CALL_BODY(moveBackwardOff);

ALL_CALL_BODY(jumpOn);
ALL_CALL_BODY(jumpOff);

ALL_CALL_BODY(duckDown);
ALL_CALL_BODY(duckUp);

ALL_CALL_BODY(run);
ALL_CALL_BODY(walk);

ALL_CALL_BODY(resetInput);


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
