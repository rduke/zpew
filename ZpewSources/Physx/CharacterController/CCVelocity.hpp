#ifndef __ZPEW_CCVelocity_HPP__
#define __ZPEW_CCVelocity_HPP__

#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// in velocity system G -9.86 applied to mGlobalOptionalVelocity
// all stopping applied as classic/decompose method and UnexpressedVelocity method

// stopping X,Z is applied as classic radial method and decompose
// mLocalVelocity X,Z - decompose/classic
// mGlobalMandatoryVelocity X,Z - classic
// mGlobalOptionalVelocity X,Z - decompose

// stopping Y is applied by another ways
// mLocalVelocity Y - unexpressed
// mGlobalMandatoryVelocity - unexpressed
// mGlobalOptionalVelocity - -G*mDt ( gravity method ) / unexpressed

// ALSO unexpressed method is used always per all orths of all velocities!

class CCVelocity
{
public:

	CCVelocity();		

	inline InVec3 getCurrentSlitheringVel() const				{ return mSlitheringVel; }

	inline PxVec3& getCurrVelocity()							{ return mVelocity; }
	inline InVec3 getCurrVelocity() const						{ return mVelocity; }

	inline PxVec3& getCurrLocalVelocity()						{ return mLocalVel; }
	inline InVec3 getCurrLocalVelocity() const					{ return mLocalVel; }

	inline PxVec3& getCurrGlobalOptionalVelocity()				{ return mGlobalOptionalVel; }
	inline InVec3 getCurrGlobalOptionalVelocity() const			{ return mGlobalOptionalVel; }

	inline PxVec3& getCurrGlobalMandatoryVelocity()				{ return mGlobalUsedMandatoryVel; }
	inline InVec3 getCurrGlobalMandatoryVelocity() const		{ return mGlobalUsedMandatoryVel; }

	// now this is very important stop-Velocity CALL
	// used for free fall velocity stopping!
	void consumeUnexpressedVelocity( InVec3 theoreticalMove, InVec3 realMove );

	void addGlobalMandatoryVelocity( InVec3 velocity );
	
	// accel.y must be ZERO!, ASSERT( accel.y == 0 )
	// in 2 CALLs -
	//	1) addGlobalMandatoryVelocity ( PxVec3( 0, accel.y*dt, 0 ) );
	//	2) addGlobalOptionalAcceleration( accel );
	void addGlobalOptionalAcceleration( PxVec3 accel, float maxVel );

	void setSlitheringAcceleration( PxVec3 accel, float maxVel );
	

	void update(
			float timeStep
		,	InVec3 localAcceleration
		,	float maxVelocity
		,	float minStopVelocity
		,	float maxStopVelocity
		,	float stopVelocityMultiplier
		,	float dynamicFriction
		,	float staticFriction
		,	float G
		,	InVec3 prevGlobalVelocity
	);
	
private:

	struct BufferedAcceleration
	{
		inline BufferedAcceleration()
		{}

		inline BufferedAcceleration( PxVec3 a, float mv )
			:	accel(a),	maxVel(mv)
		{}

		inline void operator = ( const BufferedAcceleration& that )
		{
			memcpy( this, &that, sizeof( BufferedAcceleration ) );
		}

		inline bool operator < ( const BufferedAcceleration& that ) const
		{
			return maxVel < that.maxVel;
		}

	public:

		PxVec3 accel;
		float maxVel;

	}; // struct BufferedAcceleration

private:


	void applyAccelerationAndDecelerationAsComposed(
			PxVec3& usedVel
		,	InVec3 appliedAcceleration
		,	bool optionalGlobalInitiatedStopVelocityUsed
		,	bool localInitiatedStopVelocityUsed
	);

	float getRadialMethodStopVel( InVec3 sourceVel );
	void accelerateXYZRadialMethod( InVec3 accel,  float maxVel, PxVec3& result, bool checkMaxVel );
	static void chooseSupremeOrthValue( InVec3 add, PxVec3& result );
	static void chooseSupremeOrthValue( float add, float& result );

	static void applyMandatoryVelocity( float mandatory, float& result );
	static void applyMandatoryVertiaclYVelocity( float mandatory, float &result );

	void updateGlobalMandatoryVel();

	void updateGlobalOptionalVel();
	void consumeVelocityXZ( PxVec3& velocity, float stopVel );
	
	inline void correctYVelocity( PxVec3& velocity );

	// correction of usedVel by initiated vector by applying StopVelocity ( parallel to initiatedNormal )
	void applyDecomposedInitiatedStopVelocity( PxVec3& usedVel, InVec3 initiatedNormal, float initiatedMagnitude, float maxVel );

	// correction of usedVel by errata vector by applying StopVelocity ( parallel to errataNormal )
	void applyDecomposedErrataStopVelocity( PxVec3& usedVel, InVec3 errataNormal, float errataMagnitude );
	
	// apply acceleration, and correct usedVel if (initiatedMagnitude+acceleration ).magnitude() exceeded the maxVel 
	void applyDecomposedInitiatedAcceleration( PxVec3& usedVel, InVec3 initiatedNormal, float initiatedMagnitude, float maxVel, InVec3 acceleration );

private:

	std::vector< BufferedAcceleration > mBufferedGlobalOptionalAcceleration;

	float mStopVelocityMultiplier;
	float mMaxStopVel;
	float mMinStopVel;

	float mG;
	float mDt;
	float mStaticFriction;
	float mDynamicFriction;

	float mMaxLocalVel;
	PxVec3 mLocalVel;

	PxVec3 mGlobalOptionalVel;

	PxVec3 mSlitheringVel;
	float mMaxSlitheringVel;
	RefOptional< PxVec3 > mSlitheringAccel;
	
	PxVec3 mGlobalNewMandatoryVel;
	PxVec3 mGlobalUsedMandatoryVel;

	// this member is sum of mLocalVel + mGlobalUsedMandatoryVel + mGlobalOptionalVel, updated each frame after
	// all accelerations applied in main update method, used to make common simple access to result velocity;
	// so outer correction/modification is absolutely pointless, BUT
	// it can be used to temporary per frame modify calculated velocity!
	PxVec3 mVelocity;
		
}; // class CCVelocity


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCVelocity::correctYVelocity( PxVec3& velocity )
{
	if ( velocity.y > 0 )
	{
		velocity.y -= mG * mDt;
		if ( velocity.y < 0 )
			velocity.y = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_CCVelocity_HPP__
