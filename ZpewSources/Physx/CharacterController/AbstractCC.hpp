#ifndef __ZPEW_PhysX_BASE_ABSTRACT_CHARACTER_HPP__
#define __ZPEW_PhysX_BASE_ABSTRACT_CHARACTER_HPP__

#include "ZpewSources/Physx/CharacterController/CCProperties.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	class CCManager;
	class LockablePxController;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct AbstractCCInput
	:	private NonCopyable
{
	//*On() - means 
	virtual void strafeLeftOn() = 0;
	virtual void strafeLeftOff() = 0;

	virtual void strafeRightOn() = 0;
	virtual void strafeRightOff() = 0;

	virtual void moveForwardOn() = 0;
	virtual void moveForwardOff() = 0;

	virtual void moveBackwardOn() = 0;
	virtual void moveBackwardOff() = 0;

	virtual void jumpOn() = 0;
	virtual void jumpOff() = 0;

	virtual void duckDown() = 0;
	virtual void duckUp() = 0;
	virtual void run() = 0;
	virtual void walk() = 0;

	virtual void resetInput() = 0;

	virtual ~AbstractCCInput() {}

}; // struct AbstractCCInput


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct AbstractCC
	:	public AbstractCCInput
{
	virtual PxVec3 getRealPosition() const = 0;		
	virtual PxVec3 getInterpolatedPosition() const = 0;
	
	virtual void setDirection( InVec3 dir ) = 0;
	virtual PxVec3 getDirection() const = 0;
	virtual Angle getDirectionAngle() const = 0;

	virtual PxRigidDynamic& getCharacterRigidDynamic() = 0;

	// return practical frame velocity meter per second
	//{
		virtual PxVec3 getCurrentVectorVelocity() const = 0;
		inline float getCurrentVelocity() const			{ return getCurrentVectorVelocity().magnitude(); }
	//}


	virtual void update( float timeStep ) = 0;
	virtual float getCurrentHeight() const = 0;

	virtual const CCManager& getManager() const = 0;
	virtual CCManager& getManager() = 0;


	virtual float getControllerMinHeight() const = 0;
	virtual float getControllerMaxHeight() const = 0;

	virtual float getPlayerMass() const = 0;
	virtual float getDuckingDownVelocity() const = 0;
	virtual float getDuckingUpVelocity() const = 0;
	virtual float getStrafeSidesCoefficient() const = 0;

	virtual void setControllerMinHeight( float value ) = 0;
	virtual void setControllerMaxHeight( float value ) = 0;

	virtual void setPlayerMass( float value ) = 0;
	virtual void setDuckingDownMultiplier( float value ) = 0;
	virtual void setDuckingUpMultiplier( float value ) = 0;
	virtual void setStrafeSidesCoefficient( float value ) = 0;

	virtual const CCMovementProperties& getWalkMovementProperties() const = 0;
	virtual void setWalkMovementProperties( const CCMovementProperties& properties ) = 0;

	virtual const CCMovementProperties& getRunMovementProperties() const = 0;
	virtual void setRunMovementProperties( const CCMovementProperties& properties ) = 0;

	virtual const CCJumpProperties& getJumpProperties() const = 0;
	virtual void setJumpProperties( CCJumpProperties& value ) = 0;

	virtual const CCAfterJumpDuckProperties& getAfterJumpDuckProperties() const = 0;
	virtual void setAfterJumpDuckProperties( const CCAfterJumpDuckProperties& properties ) = 0;

	virtual const CCSlopeMovementLimitationsProperties& getSlopeMovementLimitationsProperties() const = 0;
	virtual void setSlopeMovementLimitationsProperties( CCSlopeMovementLimitationsProperties& properties ) = 0;
		
	virtual ValueOptional< Angle > getFootingOrientationAngle() const = 0;

	virtual RefOptional< PxVec3 > getBestFootingFaceDir() const = 0;
	virtual RefOptional< PxVec3 > getContactNormal() const = 0;
	virtual RefOptional< ImpactVector > getFootingImpactData() const = 0;

	// has CC's shape volume overlapping
	virtual bool checkVolume() const = 0;

protected:

	virtual LockablePxController& getPhysxController() = 0;


}; // struct AbstractCC


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PhysX_BASE_ABSTRACT_CHARACTER_HPP__
