#ifndef __ZPEW_CCManager_HPP__
#define __ZPEW_CCManager_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"
#include "ZpewSources/Physx/CharacterController/CCProperties.hpp"
#include "ZpewSources/Physx/CharacterController/CCBroadcaster.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{
		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCManager
	:	public NonCopyable
{
public:

	typedef std::set< AbstractCC * > CCSet;

	typedef std::map< const PxShape*, AbstractCC* > CCByShapes;
	typedef std::map< const PxActor*, AbstractCC* > CCByActors;

	typedef std::pair< const PxShape*, AbstractCC* > CCAndShapePair;
	typedef std::pair< const PxActor*, AbstractCC* > CCAndActorPair;

	CCManager(
			PxPhysics& physics
		,	PxScene& scene
		,	PxControllerManager& physXControllerManager
		,	PxU32 charactercontrollersCollisionGroup
	);

	AbstractCC& buildCC( const CCDescription & description );

	void updateAllControllers( float timeStep );
	void removeController( AbstractCC* that );


	DefaultCCFrictionAdapter& getDefaultCharacterControllerFrictionAdapter()				{ return mDefaultCharacterFrictionAdapter; }
	const DefaultCCFrictionAdapter& getDefaultCharacterControllerFrictionAdapter() const	{ return mDefaultCharacterFrictionAdapter; }


	CCSet& getCharacters()										{ return mAllCharacters; }
	AbstractCCInput& getBroadcaster()							{ return mCCBroadcaster; }

	PxControllerManager& getPhysXControllerManager()			{ return mPxControllerManager; }
	const PxControllerManager& getPhysXControllerManager() const{ return mPxControllerManager; }

	PxScene& getPhysxScene()									{ return mScene;							}
	const PxScene& getPhysxScene()const							{ return mScene;							}	

	PxPhysics& getPhysics()										{ return mPhysics;							}
	const PxPhysics& getPhysics() const							{ return mPhysics;							}

	void setCharacterControllerPxMaterial( PxMaterial* mat )	{ mCharacterControllerPxMaterial = mat;		}
	const PxMaterial* getCharacterControllerPxMaterial() const	{ return mCharacterControllerPxMaterial;	}
	PxMaterial* getCharacterControllerPxMaterial()				{ return mCharacterControllerPxMaterial;	}

	PxU32 getCharacterControllerCollisionGroup() const			{ return mCCCollisionGroup; }

	void setTessellation( bool flag, float maxEdgeLength )
	{
		mPxControllerManager.setTessellation( flag, maxEdgeLength );
	}

	bool isCharactersShape( const PxShape* shape ) const;

	// would return 0 if wouldn't find
	AbstractCC* tryCastShape( const PxShape* shape ) const;
	AbstractCC* tryCastActor( const PxActor* actor ) const;

private:

	PxShape* getCCShape( AbstractCC& cc ) const;
	
private:
	
	CCBroadcaster mCCBroadcaster;

	DefaultCCFrictionAdapter mDefaultCharacterFrictionAdapter;
	PxControllerManager& mPxControllerManager;
	PxScene& mScene;
	PxPhysics& mPhysics;
	PxU32 mCCCollisionGroup;
	
	CCByActors mCharactersActors;
	CCByShapes mCharactersShapes;
	CCSet mAllCharacters;

	PxMaterial* mCharacterControllerPxMaterial;
	
}; // class CCManager


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_CCManager_HPP__
