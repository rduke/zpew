
#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/LockablePxController.hpp"
#include "ZpewSources/Physx/CharacterController/CCManager.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::lock()
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	if ( mIsLocked == 0 )
	{
		//m_controller->getActor()->getScene()->lockRead( __FILE__, __LINE__ );
		mController->getActor()->getScene()->lockWrite( __FILE__, __LINE__ );
	}
		
	++ mIsLocked;

	if ( mIsLocked > 10*100 )
		throw OverLockException( "LockablePxController::lock > 10000" );

	Assert( mIsLocked >= 0 && mIsLocked< 10*100 );

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::unlock()
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	if( mIsLocked == 1 )
	{
		mIsLocked = 0;
		//m_controller->getActor()->getScene()->unlockRead();
		mController->getActor()->getScene()->unlockWrite();
	}
	else
		--mIsLocked;

	Assert( mIsLocked >= 0 );

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::forceUnLock()
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	if ( mIsLocked > 0 )
	{
		mIsLocked = 0;
		unlock();
	}

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

CCAutoLock::CCAutoLock( LockablePxController * _locker )
	:	mLockIFace( _locker )
{
	mLockIFace->lock();
}

#else

CCAutoLock::CCAutoLock( LockablePxController * _locker )
{}

#endif



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock::CCAutoLock( CCAutoLock& other )
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	mLockIFace = other.mLockIFace;
	other.mLockIFace = 0;

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock&
CCAutoLock::operator = ( CCAutoLock & other )
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	if ( mLockIFace )
		mLockIFace->unlock();

	mLockIFace = other.mLockIFace;
	other.mLockIFace = 0;

#endif

	return *this;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock::~CCAutoLock()
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	die();

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCAutoLock::die()
{
#ifdef ZPEW_USE_CONCURRENT_CC_SIMULATION

	if ( mLockIFace )
		mLockIFace->unlock();

	mLockIFace = 0;

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
