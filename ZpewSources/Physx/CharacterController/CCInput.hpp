#ifndef __ZPEW_CharacterControllerInput_HPP__
#define __ZPEW_CharacterControllerInput_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCInput
	:	public AbstractCC
{

public:

	CCInput()							{ resetInput();					}

	virtual void strafeLeftOn()			{ mMoveLeftActive = true;		}
	virtual void strafeLeftOff()		{ mMoveLeftActive = false;		}

	virtual void strafeRightOn()		{ mMoveRightActive = true;		}
	virtual void strafeRightOff()		{ mMoveRightActive = false;		}

	virtual void moveForwardOn()		{ mMoveForwardActive = true;	}
	virtual void moveForwardOff()		{ mMoveForwardActive = false;	}

	virtual void moveBackwardOn()		{ mMoveBackwardActive = true;	}
	virtual void moveBackwardOff()		{ mMoveBackwardActive = false;	}

	virtual void jumpOn()				{ mUserContinueJumping = true; onActivateJump();	}
	virtual void jumpOff()				{ mUserContinueJumping = false;						}

	virtual void duckDown()				{ mIsDuckingDown = true;		}
	virtual void duckUp()				{ mIsDuckingDown = false;		}

	virtual void run()					{ mIsRunning = true;			}
	virtual void walk()					{ mIsRunning = false;			}

	virtual void resetInput()
	{
		mMoveForwardActive = false;
		mMoveBackwardActive = false;
		mMoveLeftActive = false;
		mMoveRightActive = false;
		mIsDuckingDown = false;
		mIsRunning = false;
		mUserContinueJumping = false;
	}


protected:
	
	virtual void onActivateJump() = 0;

	bool isUserDucking() const				{ return mIsDuckingDown;		}
	bool isUserRunning() const				{ return mIsRunning;			}
	bool isUserMovingForward() const		{ return mMoveForwardActive;	}
	bool isUserMovingBackward() const		{ return mMoveBackwardActive;	}
	bool isUserMovingLeft() const			{ return mMoveLeftActive;		}
	bool isUserMovingRight() const			{ return mMoveRightActive;		}
	bool isUserJumping() const				{ return mUserContinueJumping;	}

private:
	
	bool mIsRunning;
	bool mIsDuckingDown;
	bool mUserContinueJumping;

	bool mMoveForwardActive;
	bool mMoveBackwardActive;
	bool mMoveLeftActive;
	bool mMoveRightActive;
			
}; // class CharacterControllerInput


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_CharacterControllerInput_HPP__
