#ifndef __ZPEW_CharacterControllerCore_HPP__
#define __ZPEW_CharacterControllerCore_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Physx/CharacterController/CCGameParameters.hpp"
#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"
#include "ZpewSources/Physx/CharacterController/CCBody.hpp"
#include "ZpewSources/Common/Basement/TimeSimulators.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	const float CC_OFFSET = 0.05f;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct JumpFrameStage
{
	JumpFrameStage()
		:	mJumpInitialVerticalVelocity( 0 )
	{
		reset();
	}

	void reset()
	{
		mIsDucking = false;
		mDuckingElapsedTime = 0;
	}

	bool isDucking() const	{ return mIsDucking; }

	void activateClassicJumpWithDucking()
	{
		mJumpInitialVerticalVelocity = 0;
		mIsDucking = true;
	}

	bool canMakeNewJump() const { return !mIsDucking; }

public:

	float mJumpInitialVerticalVelocity;
	bool mIsDucking;
	float mDuckingElapsedTime;

}; // struct JumpFrameStage


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCCore
	:	public CCGameParameters
	,	public CCBodyListener
{

public:

	CCCore( const CCDescription & description, CCManager & CCManager );

	virtual void onCalculateNextAppliedForce( float realPrevElevationAngle );
		
	virtual void update( float timeStep );
	
	void setCharacterPos( PxVec3 pos );
	virtual RefOptional< PxVec3 > getBestFootingFaceDir() const;
	virtual ValueOptional< Angle > getFootingOrientationAngle() const;
	virtual RefOptional< PxVec3 > getContactNormal() const			{ return mCCBody.getFootingContactNormal();	}
	virtual RefOptional< ImpactVector > getFootingImpactData() const{ return mCCBody.getFootingImactData();		}

	
	PxActor* getCharacterActor()									{ return getPhysxController().getActor();	}
	virtual PxRigidDynamic& getCharacterRigidDynamic();

	virtual LockablePxController& getPhysxController()				{ return mCCBody.getPhysxController();		}
	virtual const LockablePxController& getPhysxController() const	{ return mCCBody.getPhysxController();		}
	virtual PxVec3 getCurrentVectorVelocity() const					{ return mCCBody.getCurrentVectorVelocity(); }

	PxU32 getCurrentCollisions() const								{ return mCCBody.getCurrentCollisionFlags(); }
	
	virtual PxVec3 getRealPosition() const							{ return mCCBody.getRealPosition();			}
	virtual PxVec3 getInterpolatedPosition() const					{ return mCCBody.getInterpolatedPosition();	}

	~CCCore()														{ /* do not delete used PxController*  ! */ }
	virtual float getCurrentHeight() const							{ return mCurrentHeight;					}
	virtual const CCManager& getManager() const						{ return mCCManager;		}
	virtual CCManager& getManager()									{ return mCCManager;		}
	
	virtual void setDirection( InVec3 dir );
	virtual PxVec3 getDirection() const;
	virtual Angle getDirectionAngle() const							{ return mCurrentFaceAngle; }

protected:

	virtual void onActivateJump();
	
private:

	inline void assertHeight(){ Assert( mCurrentHeight<= getControllerMaxHeight() && mCurrentHeight >= getControllerMinHeight() ); }

	bool checkVolume( float radius, float pxHeight, InVec3 pos ) const;
	virtual bool checkVolume() const;

	void resizeHeightOnGround( const float delta );
	void resizeHeightInFlight( const float delta );

	bool isCoreOrderingDucking();
	
	void applyForwardBackwardAcceleration(
			PxVec3& inOutValue
		,	bool isNegative
		,	InVec3 charCtrlDirection
	);
	void applyLeftRightAcceleration(
			PxVec3& inOutValue
		,	bool isNegative
		,	InVec3 charCtrlDirection
	);
	void prepareAfterJumpScenarios();

	void updateCharacterExtents();
	void updateJumping();
	void updateMovementProps( float angleDegrees );

protected:

	CCManager & mCCManager; // Singleton
	CCBody mCCBody;

private:

	Angle mCurrentFaceAngle;
	
	bool mJumpLaunched;
	JumpFrameStage mJumpStage;

	float mAccumulatedTimeSinceLastJumpFinished;
	float mDt;
	float mCurrentHeight;
	float mCurrentJumpAcceleration;
	
	std::auto_ptr< TimeSimulator > mTimeSimulator;
	CCMovementProperties* mSelectedMovementProps;

	float mIdealAcceleration;
	float mIdealMaxVelocity;

	PxControllerShapeType::Enum mBodyType;
		
}; // class CharacterControllerCore


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_CharacterControllerCore_HPP__
