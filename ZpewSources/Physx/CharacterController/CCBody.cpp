#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CCBody.hpp"
#include "ZpewSources/Physx/CharacterController/CCManager.hpp"
#include "ZpewSources/Physx/CharacterController/CCCore.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCBody::CCBody(
		const CCSlopeMovementLimitationsProperties& slopeLimitationProps
	,	float pxMoveStepOffset
	,	const AbstractCC* owner
	,	CCBodyListener* listener
)
	:	mGravity( G )
	,	mCurrentStaticFriction( 0 )
	,	mCurrentDynamicFriction( 0 )
	,	mCurrentCollisionFlags( 0 )
	,	mPxController( 0 )
	,	mDt( 0.0001f )
	,	mFrictionAdapter( 0 )
	,	mSlopeLimitationProps( slopeLimitationProps )
	,	mInterpolatedTimeStep( 0 )
	,	mPxMoveStepOffset( pxMoveStepOffset )
	,	mPositionsHistory( PxVec3(0) )
	,	mTimeSteps( 0 )
	,	mCollisionHistory( CollisionFrame( 0,0 ) )
	,	mInterpolatedPosition( 0 )
	,	mLastLandingVelocity( 0 )
	,	mLocalVelocityHistory( PxVec3(0) )
	,	mBodyType( PxControllerShapeType::eCAPSULE )
	,	mBeforePxMovePos( 0 )
	,	mPreviousTotalMove( 0 )
	,	mPreviousLocalMove( 0 )
	,	mPreviousGlobalMove( 0 )
	,	mLastFrameCCVolumeIsNotOverlapped( true )
	,	mTestMovementActivated( false )
	,	mFootingShape( 0 )
	,	mOwner( owner ) // RAW MEMORY!
	,	mListener( listener ) // RAW MEMORY!
{
	// DO NOT USE HERE "mOwner"  "mListener"
	mFootingData.reserve( 4 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

 
void
CCBody::setPxController( PxController & usedPxController )
{
	mPxController.set( &usedPxController );

	for( int i = 0; i < SWITCH_SIZE; i++ )
		mPositionsHistory.insertCurrent( mPxController.getPosition() );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::updateGlobalRidingMovementInfluence()
{
	float prevDt = mTimeSteps.getPreviousPrevious();
	
	PxU32 fakeCollisionFlags;
	PxVec3 beforeRidePos = mPxController.getPosition();

	PxVec3 deltaGlobalMove(0);
	mPreviousGlobalVelocity=PxVec3(0);
	if ( mFootingShape && mFootingShape->getActor().isRigidDynamic() )
	{
		Assert( mTouchedShapeGlobalPos && mTouchedShapeLocalPos );
				
		const PxTransform shapeTransform = PxShapeExt::getGlobalPose( *mFootingShape );
		const PxVec3& posPreviousFrame = *mTouchedShapeGlobalPos;
		const PxVec3 posCurrentFrame = shapeTransform.transform( *mTouchedShapeLocalPos );
		deltaGlobalMove = posCurrentFrame - posPreviousFrame;
		mPreviousGlobalVelocity = deltaGlobalMove / prevDt;
	}
	
	mPreviousGlobalMove = deltaGlobalMove;
	
	if ( deltaGlobalMove.y != 0 )
	{
		// do not add vertical velocity to mVelocity
		//mVelocity.addGlobalMandatoryVelocity( PxVec3(0, deltaGlobalMove.y/prevDt, 0 ) );
		pxMove( PxVec3( 0, deltaGlobalMove.y, 0 ), fakeCollisionFlags, false );
		deltaGlobalMove.y = 0;
	}

	if( ! mOwner->checkVolume() )
	{
		mTestMovementActivated = true;
		pxMove( PxVec3( 0, fabs( mPreviousGlobalMove.y ) + CC_OFFSET, 0 ), fakeCollisionFlags );
		pxMove( PxVec3( 0, beforeRidePos.y - mPxController.getPosition().y, 0 ), fakeCollisionFlags );
		mTestMovementActivated = false;
	}
	
	mLastFrameCCVolumeIsNotOverlapped = mOwner->checkVolume();	

	if ( ! isZeroXZ( deltaGlobalMove ) )
	{ 
		mPxController.setPosition( beforeRidePos );

		deltaGlobalMove.x /= prevDt;
		deltaGlobalMove.z /= prevDt;

		PxVec3 accelerationXZ = deltaGlobalMove;
		Assert( accelerationXZ.y == 0 );
		accelerationXZ.x /= mDt;
		accelerationXZ.z /= mDt;

		float maxVel = magnitudeXZ( deltaGlobalMove );

		mVelocity.addGlobalOptionalAcceleration( accelerationXZ, maxVel );
	}

} // CCBody::updateGlobalMovementInfluence


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::update( float dt, const CCMovementProperties* currentProps )
{
	mDt = dt;
	mCurrentMovementProps = currentProps;
	mInterpolatedTimeStep = 0;
	mTimeSteps.insertCurrent( dt );
		
	PxVec3 beforeRidePos = mPxController.getPosition();
	updateGlobalRidingMovementInfluence();
	PxVec3 originPos = mPxController.getPosition();

	// use previous frame local elevation for checking correcting movement acceleration and max velocity
	mListener->onCalculateNextAppliedForce( getXZandYVectorAngle( mPreviousLocalMove ) );

	PxVec3 cleanMove = mDt * mVelocity.getCurrVelocity();
	pxMove(	cleanMove, mCurrentCollisionFlags, true );

	if ( isCCVolumeNotOverlapped() )
	{
		correctTakeoffDescent();
		correctElevation( originPos );
	}

	PxVec3 newPos = mPxController.getPosition();

	mVelocity.consumeUnexpressedVelocity( cleanMove, newPos - originPos );
	
	if ( isCCVolumeNotOverlapped() )
	{
		updateSlitheringOnSlopeActivation();
	}	

	mCollisionHistory.insertCurrent( CollisionFrame ( mDt, mCurrentCollisionFlags ) );
	mPositionsHistory.insertCurrent( newPos );

	vecMin( mPreviousTotalMove, newPos, beforeRidePos );
	vecMin( mPreviousLocalMove, newPos, originPos );
		
	mLocalVelocityHistory.insertCurrent( mVelocity.getCurrLocalVelocity() );
	
	if ( checkLandingPattern() )
		mLastLandingVelocity = mLocalVelocityHistory.getPreviousPrevious();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::updateSlitheringOnSlopeActivation()
{
	if ( ! mFootingContactNormal )
		return;
	
	ValueOptional< float > angle = getCurrentFootingPatchSlope();
	if( angle && *angle >= mSlopeLimitationProps.mMaxElevationAngle )
	{				
		Assert( *angle >= 0 && *angle <= 90 );
			
		PxVec3 slopeSlitheringDir(0);
		
		int size = mFootingData.size();
		if (size>1)
		{
			int debug = 1;
		}
		for( int i = 0; i < size; i++ )
		{
			Assert( mFootingData[ i ].mContactNormal.magnitude() < 1.0001f && mFootingData[ i ].mContactNormal.y >= 0 );

			slopeSlitheringDir += mFootingData[ i ].mContactNormal;
		}

		slopeSlitheringDir.normalize();
		slopeSlitheringDir.y = -slopeSlitheringDir.y;
		if ( slopeSlitheringDir.y > - 0.5f )
		{
			slopeSlitheringDir.y = - 0.5f;
			slopeSlitheringDir.normalize();
		}
		
		slopeSlitheringDir *= mSlopeLimitationProps.mXZAccelerationSlitheringDown;

		PxVec3 resultAcceleration = slopeSlitheringDir;
		slopeSlitheringDir *= mDt; // resultMove is velocity
		// correct slithering acceleration simultaneously considering the gathered SlopSlithering velocity
		slopeSlitheringDir += mVelocity.getCurrentSlitheringVel();
		slopeSlitheringDir *= mDt; // resultMove is real move!

		PxVec3 originPos = mPxController.getPosition();
		PxVec3 newTestPos = makeTestMove( slopeSlitheringDir ); // m = dir * A * t^2;
		mPxController.setPosition( originPos );

		// this check has only one frame logical sense
		// but causes some slithering bugs - doesn't slithering on edges!
		if ( ( newTestPos.y - originPos.y ) / mDt > 0.001f )
  		{
			mSlopeSlitheringAcceleration.reset();
			return;
 		}

		float realMoveMagnitude = ( newTestPos - originPos ).magnitude();
		float theoreticalMoveMagnitude = slopeSlitheringDir.magnitude();

		if ( theoreticalMoveMagnitude / realMoveMagnitude > mSlopeLimitationProps.mMaximumRatioMoveSlithering )
		{
			mSlopeSlitheringAcceleration.reset();
			return;
		}
		else
		{
			mSlopeSlitheringAcceleration = resultAcceleration;
			return;
		}
	}
	else
		mSlopeSlitheringAcceleration.reset();

} // CCBody::updateSlitheringOnSlopeActivation


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::correctTakeoffDescent()
{
 	if ( charCtrlDownCollisions() || mFootingContactNormal || mVelocity.getCurrVelocity().y > 0 )
		return;

	PxVec3 originPos = mPxController.getPosition();
	
	PxVec3 newMove( 0, - mCurrentMovementProps->mMinYVelocityTakeoff * mDt, 0 );
	pxMove( newMove, mCurrentCollisionFlags, true );

	if ( ! mFootingContactNormal || ! charCtrlDownCollisions() )
		mPxController.setPosition( originPos );

} // CCBody::correctTakeoffDescent


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//DEPRECATED
bool
CCBody::checkPatternStep( InVec3 originPos )
{
	const PxVec3 currentPos = mPxController.getPosition();
	const float moveMagnitudeSquared = ( currentPos - originPos ).magnitudeSquared();

	if (
			currentPos.y - originPos.y <= 0 
		||	! charCtrlSidesAndDownCollisions( mCurrentCollisionFlags )
		||	moveMagnitudeSquared < mVelocity.getCurrVelocity().magnitudeSquared() * mDt * mDt
		//||	getCurrentFootingPatchSlope() > mOwner->getSlopeMovementLimitationsProperties().mMaxElevationAngle
		// last check would be correct, because if this is true, it could be also pattern step!
	)
		return false;
		
	CCAutoLock lock = mPxController.autoLock();

	mPxController.setStepOffset( 0 );
	mPxController.setPosition( originPos );
		
	mTestMovementActivated = true;
	PxU32 flags;

	PxVec3 move = currentPos - originPos;
	// if we are jumping, we must use it in testing!
	move.y = mVelocity.getCurrVelocity().y * mDt;

	pxMove( move, flags );
	float testYPos = mPxController.getPosition().y;
	mTestMovementActivated = false;

	mPxController.setStepOffset( mPxMoveStepOffset );
	mPxController.setPosition( currentPos );

	lock.die();

	if ( testYPos + 0.0001f < currentPos.y && testYPos <= originPos.y )
		return true;
	else
		return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//DEPRECATED
bool
CCBody::verifyStepJerkingPatternAndStepPattern( InVec3 originPos )
{
	bool stepJerkingDetectedOrSteppingDetected = checkPatternStep( originPos );

	// if stepJerkingDetectedOrSteppingDetected == false 
	// that means - pattern step did not find!
	// try verify stepping by jerking-detection
	// 
	if( ! stepJerkingDetectedOrSteppingDetected && mActivateStepMoveLengthXZ.isInit() )
	{
		// this block should test jerking when moving on the step-obstacle
	
		PxVec3 testMove = mVelocity.getCurrVelocity() * mDt;
		float testMoveMagnitudeXZ = returnCorrectDivider( magnitudeXZ( testMove ) );
		if( testMoveMagnitudeXZ < *mActivateStepMoveLengthXZ )
		{
			testMove *= *mActivateStepMoveLengthXZ / testMoveMagnitudeXZ;
			testMove.y = 0;

			InVec3 newPos = mPxController.getPosition();
			PxU32 collFlags;
			pxMove( testMove, collFlags, false );
			if( ! checkPatternStep( originPos ) )
				stepJerkingDetectedOrSteppingDetected = false;
			else
				stepJerkingDetectedOrSteppingDetected = true;

			mPxController.setPosition( newPos );

		}
		else
			stepJerkingDetectedOrSteppingDetected = false;
	}
	
	return stepJerkingDetectedOrSteppingDetected;

} // CCBody::verifyStepJerkingPatternAndStepPattern


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::correctElevation( InVec3 originPos )
{
	if( mVelocity.getCurrVelocity().magnitudeSquared() < 0.00001f )
		return;
	
	// this logic is realy needed!
	correctElevationOnBodies( originPos, 0, 0 );


//	old code presented, now CC doesn't need checkPatternStep() because
//	it was used to detect whether should check elevation or not
//	now the contact angle ( slope footing ) is calculated throught raycasting, so we have true
//	face angle of slope flat!

// 	bool elevationStepEnabled = checkPatternStep( originPos );
// 	if( ! elevationStepEnabled && mVelocity.getCurrVelocity().y < 0 )
// 		correctElevationOnBodies( originPos, 0,0 );
// 	  	
// 	if( elevationStepEnabled || mActivateStepMoveLengthXZ.isInit() )
// 	{
// 		// capsule controller in Physx 3.2.x stepping is nice =)
// 		if ( mBodyType == PxControllerShapeType::eBOX && verifyStepJerkingPatternAndStepPattern( originPos ) )
//  		{
// 			float currVelocity = mVelocity.getCurrVelocity().magnitude();
// 			InVec3 newPos = mPxController.getPosition();
// 		
// 			if( ! mActivateStepMoveLengthXZ.isInit() )
// 				mActivateStepMoveLengthXZ.reset( magnitudeXZ( newPos - originPos ) );
// 
// 			mPxController.setPosition( originPos );
// 			clamp( 2.5f, 10000.f, currVelocity );
// 			if( *mActivateStepMoveLengthXZ < currVelocity * mDt )
// 				mActivateStepMoveLengthXZ = currVelocity * mDt;
// 
// 			pxMove (
// 					PxVec3 ( 0, currVelocity * 2 * mDt, 0 )
// 				,	mCurrentCollisionFlags
// 			);
//  		}
// 		else
// 		{
// 			mActivateStepMoveLengthXZ.reset();
// 		}
// 	}

} // CCBody::correctElevation


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCBody::correctElevationOnBodies( InVec3 originPos, float movementRotationAngle, int calledTimes )
{
	if ( !mFootingContactNormal.isInit() )
		return true;

	PxVec3 newPos = mPxController.getPosition();
	
	ValueOptional< float > realFootingAngle = getCurrentFootingPatchSlope();

	if (
			realFootingAngle.isInit()
		&&	*realFootingAngle >= mSlopeLimitationProps.mMaxElevationAngle
		&&	newPos.y - originPos.y > 0
	)
	{
		float a = *realFootingAngle;
		InVec3 footingDirection = *mFootingContactNormal;

		float angleDir = getXZFlatAngle_0_360( mVelocity.getCurrVelocity() );
		float angleFoot = getXZFlatAngle_0_360( footingDirection );

		// ibah42 -  mSlopeLimitationProps.mPerFrameAdditionalRotationAngle * ( calledTimes + 1 );
		// this is need to correct edge sticking
		float angleFootPlus = angleFoot + 90.0f - mSlopeLimitationProps.mPerFrameAdditionalRotationAngle * ( calledTimes + 1 );
		correctAngle_0_360( angleFootPlus );
		float angleFootMinus = angleFoot - 90.0f + mSlopeLimitationProps.mPerFrameAdditionalRotationAngle * ( calledTimes + 1 );
		correctAngle_0_360( angleFootMinus );

		float firstRotationAngle = getRotationMinimalAngleFirstToSecond( angleDir, angleFootPlus );
		float secondRotationAngle = getRotationMinimalAngleFirstToSecond( angleDir, angleFootMinus );

		float rotationAngle;
		if( fabs ( firstRotationAngle ) > fabs ( secondRotationAngle ) )
			rotationAngle = secondRotationAngle;
		else
			rotationAngle = firstRotationAngle;

		if( fabs( movementRotationAngle ) + fabs( rotationAngle ) < mSlopeLimitationProps.mMaxSlopeCollidingRotationAngle )
		{
 			int k = 2;
			float ratio = ( k + degrees( rotationAngle ).cos() ) / ( k + 1 );
 			Assert( ratio <= 1.0f );
 
  			mVelocity.getCurrVelocity().x *= ratio;
  			mVelocity.getCurrVelocity().z *= ratio;
			rotateXZ( mVelocity.getCurrVelocity(), rotationAngle );

  			float dot = fabs( cosAngleXZ( mVelocity.getCurrVelocity(), footingDirection ) );
  			Assert( dot < 0.001 * calledTimes + 0.1  );

			mPxController.setPosition( originPos );
			
			//  recursive call left to improve quality of rotation
			{
				if ( ++calledTimes > mSlopeLimitationProps.mMaxSlopeRotationsAppliedAtOneFrame )
					return false;

				mPxController.setStepOffset( 0 );
				pxMove( mVelocity.getCurrVelocity() * mDt, mCurrentCollisionFlags, true );
				mPxController.setStepOffset( mPxMoveStepOffset );

				correctElevationOnBodies( originPos, movementRotationAngle + rotationAngle, calledTimes );
			}

			return false;
		}
		else
		{
			mVelocity.getCurrVelocity().x = 0;
			mVelocity.getCurrVelocity().z = 0;
			return false;
		}
	}
	return true;

} // CCBody::correctElevationOnBodies


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::updateFootingMaterialsAndFriction()
{
	if ( ! mCurrentMaterial.isInit() )
	{
		if( mActivateStepMoveLengthXZ.isInit() )
			mCurrentStaticFriction = 0;
		else
			mCurrentStaticFriction = mFrictionAdapter->getInflightStaticFriction();

		mCurrentDynamicFriction = mFrictionAdapter->getInflightDynamicFriction();
	}
	else
	{
		mCurrentStaticFriction = mFrictionAdapter->getStaticFriction( *mCurrentMaterial );
		mCurrentDynamicFriction = mFrictionAdapter->getDynamicFriction( *mCurrentMaterial );
	}
	
	// some materials about friction
	// http://www.science-education.ru/39-1468
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
CCBody::makeTestMove( InVec3 move )
{
	PxVec3 originPos = mPxController.getPosition();
	PxU32 currentCollisionFlags;

	mTestMovementActivated = true;

	mPxController.lock();
	pxMove( move, currentCollisionFlags, false );
	PxVec3 result = mPxController.getPosition();

	mPxController.setPosition( originPos );
	mPxController.unlock();

	mTestMovementActivated = false;
	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
 
 
void
CCBody::updateFooting( const FootingData& data )
{
	if ( data.mContactPoint.y >= mPxController.getPosition().y || data.mContactNormal.y < 0 )
		return;

	if (	! mFootingContactNormal
		||	! mCurrentMaterial
		||	mFootingContactNormal->y < data.mContactNormal.y // this means that the best normal is vertical, which means x=0 y =1 z=0
	)
	{
		getCurrentFootingPatchSlope( &data.mContactNormal );
		mFootingContactNormal = data.mContactNormal;
		mCurrentMaterial = data.mMaterial;

		mFootingImpactData.reset( ImpactVector( data.mContactNormal, data.mContactPoint ) );
		
		// TEMP is used!
		mFootingShape = data.mTouchedShape;
		const PxTransform shapeTransform = PxShapeExt::getGlobalPose( *mFootingShape );
		mTouchedShapeGlobalPos = data.mContactPoint;
		mTouchedShapeLocalPos = shapeTransform.transformInv( *mTouchedShapeGlobalPos );
	}

	mFootingData.push_back( data );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::findEdgeBestSlope( const PxControllerShapeHit& hit )
{	
	// the idea of this CALL is to find better slope angle if possible
	// on edges/slopes if current slope angle is bigger than permitted elevation angle
	// in other cases - skipping ( even if there are better slopes )
	// this CALL need to make correct detection of StepPatterns, and ElevationOnBodies policy following

	float currentSlopeAngle = mOwner->getSlopeMovementLimitationsProperties().mMaxElevationAngle;
	if (
			! getCurrentFootingPatchSlope().isInit()
		||	getCurrentFootingPatchSlope().get() < currentSlopeAngle
		||	hit.worldNormal.y < 0 
	)
		return;

// 	if (
// 		getCurrentFootingPatchSlope( &hit.worldNormal ).get() < currentSlopeAngle 
// 		ALREADY CHECKED!
// 
// 	||	mBeforePxMovePos.y - mPxController.getPosition().y > 0 // strictly bigger ">" - because of Physx base point detection!
//		slithering must be also checked while isn't moving - for SLITHERING	
// 	)
// 		return;

	float currHeight = mOwner->getCurrentHeight();
	float yOffset = currHeight / 100;
	register const int size = 3;
	PxVec3 direction = - hit.worldNormal;
	Assert( direction.y <= 0 );
 
	// prevent parallel ground raycasting!
 	if ( direction.y > -0.3f )
 	{
 		const_cast< int& >( size ) = 5;

 		direction.y = -0.3f;
 		direction.normalize();
 	}
	
	PxRaycastHit hitData[size];

	for( int i=0; i<size; i++ )
	{
		PxVec3 origin = toVector3< PxVec3 >( hit.worldPos );
		origin.y += i * yOffset;
		
		mOwner->getManager().getPhysxScene().raycastSingle(
				origin
			,	direction
			,	currHeight * 0.05f + mPxController.getSkinOffset()
			,	PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eDIRECT_SWEEP | PxSceneQueryFlag::eIMPACT | PxSceneQueryFlag::eNORMAL
			,	hitData[i]
		);
	}

	int best = 0;
	for( int i=1; i<size; ++i )
	{
		if (	hitData[i].shape
			&&	hitData[i].normal.y > hitData[best].normal.y
			/*&& hitData[i].shape == hit.shape*/ // could be raycasted another shape!
		)
			best = i;
	}

	//if ( hitData[best].shape == hit.shape )
	if ( hitData[best].shape )
		getCurrentFootingPatchSlope( &hitData[best].normal );

} // CCBody::findEdgeBestSlope


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::onShapeHit( const PxControllerShapeHit& hit )
{
	if( mTestMovementActivated || hit.worldNormal.y < 0 )
		return;

	if( ! hit.shape )
		throw std::exception( "onShapeHit with PxControllerShapeHit.shape is NULL " );

	updateFooting(
		FootingData (
				hit.worldNormal
			,	hit.shape->getMaterialFromInternalFaceIndex( hit.triangleIndex ) // safe call!
			,	toVector3< PxVec3 >( hit.worldPos )
			,	hit.shape
		)
	);

	findEdgeBestSlope( hit );
	
	PxActor& actor = hit.shape->getActor();

	if(		
			actor.isRigidDynamic()
		&&	actor.isRigidDynamic()->getRigidDynamicFlags() != PxRigidDynamicFlag::eKINEMATIC
	)
	{
		float coeff =  actor.isRigidDynamic()->getMass() / mOwner->getPlayerMass();
		clamp( 0.1f, 1.f, coeff );
		coeff *= hit.length;

		PxVec3 dir = hit.dir;
		dir.x = -dir.x;
		dir.z = -dir.z;
		dir *= mOwner->getPlayerMass() * coeff;
// 
// 		PxRigidBodyExt::addLocalForceAtPos(
// 				*actor.isRigidDynamic()
// 			,	dir
// 			,	toVector3<PxVec3>( hit.worldPos )
// 			,	PxForceMode::eFORCE
// 		);		
	}

} // CCBody::onShapeHit


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::onControllerHit( const PxControllersHit& hit )
{
	if( mTestMovementActivated || hit.worldNormal.y < 0 )
		return;
	
	//BUG
	//	CharacterControllerCollisionExporter exporter( mOwner->getManager() );
	//boost::optional< const FootingData& > result = exporter.getAndPushFootingData( mFootingData, hit, mUsedPxController );

	PxShape* shape;
	hit.controller->getActor()->getShapes( &shape, 1 );
	updateFooting(
		FootingData(
				hit.worldNormal
			,	mOwner->getManager().getCharacterControllerPxMaterial()
			,	toVector3<PxVec3>( hit.worldPos )
			,	shape
		)
	);

} // CCBody::onControllerHit


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::pxMove( InVec3 vec, PxU32 & flags, bool eraseFooting /*= false*/ )
{
	if( eraseFooting )
	{
		mFootingData.clear();
	
		mCurrentStaticFriction = 0;
		mCurrentDynamicFriction = 0;

		mFootingShape = 0;

		mFootingContactNormal.reset();
		mCurrentMaterial.reset();
		
		mCurrentFootingPathSlope.reset();
		mFootingImpactData.reset();
		mTouchedShapeGlobalPos.reset();
		mTouchedShapeLocalPos.reset();
		mTracedSlopeDir.reset();
	}

	bool checked = isCCVolumeNotOverlapped();
	PxVec3 originPos = mPxController.getPosition();

	mPxController.move(
			vec
		,	mOwner->getManager().getCharacterControllerCollisionGroup()
		,	mDt
		,	flags
	);

	// PhysX has penetration bug, prevent it as possible:
	if ( checked && ! mOwner->checkVolume() )
		mPxController.setPosition( originPos );
		
	if ( eraseFooting )
	{		
		if ( !charCtrlDownCollisions() )
			mFootingShape = 0;

		updateFootingMaterialsAndFriction();
	}

} // CCBody::pxMove


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
CCBody::getCurrentVectorVelocity() const
{
	return mPositionsHistory.getVec3Velocity() / mDt;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
CCBody::getCurrentVelocity() const
{
	return mPositionsHistory.getVec3Velocity().magnitude() / mDt;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCBody::checkLandingPattern()
{
	if ( ! charCtrlDownCollisions() )
		return false;

	const PxVec3& currVel = mLocalVelocityHistory.getPrevious();
	const PxVec3& prevVel = mLocalVelocityHistory.getPreviousPrevious();

	const CCJumpProperties& props = mOwner->getJumpProperties();
// 	return
// 			prevVel.y < currVel.y
// 		&&	currVel.y > - 2 * mDt * mGravity 
// 		&&	prevVel.y < - 2 * mTimeSteps.getPreviousPrevious() * mGravity
// 	;
		
	return
			prevVel.y < currVel.y
		&&	currVel.y > props.mLandingPattern_CurrFrameMaxYVeloicty
		&&	prevVel.y < props.mLandingPattern_PrevFrameMinYVeloicty
	;
} // CCBody::checkLandingPattern


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCBody::checkStayGroundPattern()
{
	if ( ! charCtrlDownCollisions() )
		return false;

	Assert( mFootingShape );

	if ( mFootingShape->getActor().isRigidStatic() )
		return true;

	float currLocalYVel = mVelocity.getCurrLocalVelocity().y; 
	return currLocalYVel <= 0 && currLocalYVel > - 0.1 * mGravity;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::pxMove( InVec3 move )
{
	PxU32 f;
	pxMove( move, f );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::accelerate(
		PxVec3 acceleration
	,	float maxVelocity
	,	float minStopVelocity
	,	float maxStopVelocity
	,	float stopVelocityOnGroundMultiplier
	,	float stopVelocityInFlightMultiplier
)
{
  	if( mSlopeSlitheringAcceleration )
	{
		if (
				fabs( getXZAngleBetweenVectors( acceleration, *mSlopeSlitheringAcceleration ) )
			>	mSlopeLimitationProps.mInitiatedAcceleratonApplyLimitDeviationAngle
		)
		{
			// allow jumping on hight slopes!
			// this restriction controlled in Core
			acceleration.x = 0;
			acceleration.z = 0;
		}

		mVelocity.setSlitheringAcceleration( *mSlopeSlitheringAcceleration, mSlopeLimitationProps.mSlitheringDownMaxVelocity );
	}

	float stopVelocityMultiplier;
	if( charCtrlSidesAndDownCollisions ( mCurrentCollisionFlags ) )
		stopVelocityMultiplier = stopVelocityOnGroundMultiplier;
	else
		stopVelocityMultiplier = stopVelocityInFlightMultiplier;

	mVelocity.update(
			mDt
		,	acceleration
		,	maxVelocity
		,	minStopVelocity
		,	maxStopVelocity
		,	stopVelocityMultiplier
		,	mCurrentDynamicFriction
		,	mCurrentStaticFriction
		,	mGravity
		,	mPreviousGlobalVelocity
	);

} // CCBody::accelerate


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCBody::updateInterpolatedPosition( float realTimeStep )
{
	mInterpolatedTimeStep += realTimeStep;
	float deltaTime = mTimeSteps.getPrevious();
	if ( deltaTime <= mInterpolatedTimeStep )
	{
		mInterpolatedTimeStep = 0;
		mInterpolatedPosition = mPxController.getPosition();
		return;
	}

	PxVec3 prevPos = mPositionsHistory.getPrevious();
	PxVec3 prevPrevPos = mPositionsHistory.getPreviousPrevious();

	float mu = mInterpolatedTimeStep / deltaTime;
	Assert( mu <= 1 );

	mInterpolatedPosition = cosineVector3Interpolation( prevPrevPos, prevPos, mu );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


ValueOptional< Angle >
CCBody::getFootingOrientationAngle() const
{
 	if ( mFootingContactNormal.isInit() )
		return ValueOptional< Angle >( degrees( getXZFlatAngle_0_360( *mFootingContactNormal ) ) );
 	else
 		return ValueOptional< Angle>();

} // CCBody::getFootingOrientationAngle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


ValueOptional< float >
CCBody::getCurrentFootingPatchSlope( const PxVec3 * source )
{
	if( ! source )
		return mCurrentFootingPathSlope;
	else
	{
		mTracedSlopeDir = *source;
		mCurrentFootingPathSlope = 90.f - fabs( getXZandYVectorAngle( *source ) );
		return mCurrentFootingPathSlope;
	}

} // CCBody::getCurrentFootingPatchSlope


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxU32
CCBody::getBehaviorFlags(const PxShape& shape)
{
	return PxControllerBehaviorFlag::eCCT_USER_DEFINED_RIDE;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxU32
CCBody::getBehaviorFlags(const PxController& controller)
{
	return PxControllerBehaviorFlag::eCCT_USER_DEFINED_RIDE;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxU32
CCBody::getBehaviorFlags(const PxObstacle& obstacle)
{
	return PxControllerBehaviorFlag::eCCT_USER_DEFINED_RIDE;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



PxVec3
CCBody::getInterpolatedPosition() const
{
	if ( mInterpolatedTimeStep <= 0 )
		return getRealPosition();

	return mInterpolatedPosition;					
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCBody::canJump()
{
	return !mSlopeSlitheringAcceleration && checkStayGroundPattern();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/