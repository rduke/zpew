#ifndef __CCBody_HPP__
#define __CCBody_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Common/AutoConfiguration.hpp"
#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"
#include "ZpewSources/Physx/CharacterController/LockablePxController.hpp"
#include "ZpewSources/Physx/CharacterController/CCVelocity.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
  
using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class CCManager;
struct FootingData;
struct CollisionFrame;
struct CCBodyListener;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#ifdef ZPEW_DEBUG
	#define SWITCH_SIZE 2000
#else
	#define SWITCH_SIZE 4
#endif // ZPEW_DEBUG

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct FootingData
{

	FootingData ( InVec3 normal, const PxMaterial* mat, InVec3 contactPoint, PxShape* shape )
		:	mContactNormal( normal )
		,	mMaterial( mat )
		,	mContactPoint( contactPoint )
		,	mTouchedShape( shape )
	{}

public:

	PxShape* mTouchedShape;
	PxVec3 mContactPoint;
	PxVec3 mContactNormal;
	const PxMaterial* mMaterial;

}; // class FootingData


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CollisionFrame
{
	CollisionFrame ( float time, PxU32 flags )
		:	mTime( time )
		,	mCollisionFlags( flags )
	{}

	CollisionFrame()
		:       mTime( 0 )
		,       mCollisionFlags( 0 )
	{}

public:

	float mTime;
	PxU32 mCollisionFlags;

}; // class CollisionFrame


inline void makeZero( ACC::CollisionFrame& value )
{
	value.mTime = 0;
	value.mCollisionFlags = 0;
}

inline bool makeDifference( const ACC::CollisionFrame& , const ACC::CollisionFrame&  )
{
	throw std::exception("tried to make velocity from CollisionFrame - nonsence!" );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCBodyListener
{
	virtual void onCalculateNextAppliedForce( float realPrevElevationAngle ) = 0;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCBody
	:	public PxUserControllerHitReport
	,	public PxControllerBehaviorCallback
{

public:

	virtual PxU32 getBehaviorFlags(const PxShape& shape);
	virtual PxU32 getBehaviorFlags(const PxController& controller);
	virtual PxU32 getBehaviorFlags(const PxObstacle& obstacle);

public:

	typedef Switch< CollisionFrame, 20 + SWITCH_SIZE > CollisionHistory;

public:

	inline static bool charCtrlSidesAndDownCollisions( PxU32 collisions )
	{
		return 0 != ( collisions & ( PxControllerFlag::eCOLLISION_DOWN | PxControllerFlag::eCOLLISION_SIDES ) );
	}

	inline static bool charCtrlDownCollisions( PxU32 collisions )
	{
		return 0 != ( collisions & PxControllerFlag::eCOLLISION_DOWN );
	}

public:

	CCBody(
			const CCSlopeMovementLimitationsProperties& slopeLimitationProps
		,	float nxMoveStepOffset
		,	const AbstractCC* owner
		,	CCBodyListener* listener
	);      

	void setAbstractCharacter( const AbstractCC* character );
	void setPxController( PxController & usedPxController );

	virtual void onShapeHit( const PxControllerShapeHit& hit );
	virtual void onControllerHit( const PxControllersHit& hit );
	// do not use obstacles at all
	virtual void onObstacleHit(const PxControllerObstacleHit& hit )
	{
		int CatchDebugBreakPoint = 0;
	}
  
	PxVec3 getCurrentVectorVelocity() const;
	float getCurrentVelocity() const;
 
	void update( float timeStep, const CCMovementProperties* currentProps );
	bool checkLandingPattern();
	bool checkStayGroundPattern();
 
	ValueOptional< Angle > getFootingOrientationAngle() const;

	void accelerate(
			PxVec3 acceleration
		,	float maxVelocity
		,	float minStopVelocity
		,	float maxStopVelocity
		,	float stopVelocityOnGroundMultiplier
		,	float stopVelocityInFlightMultiplier
	);
 
	void updateInterpolatedPosition( float realTimeStep );
	ValueOptional< float > getCurrentFootingPatchSlope( const PxVec3 * source = 0 );
 
 
	inline ~CCBody()       { /* do not delete used PxController*  ! */ }
	inline void setCharacterControllerFrictionAdapter( const CCFrictionAdapter* adapter ) { mFrictionAdapter = adapter; }
  
	inline PxU32 getCurrentCollisionFlags() const							{ return mCurrentCollisionFlags;				}
	inline void setGravity( float yValue )									{ mGravity = fabs( yValue );					}
	inline float getGravity() const											{ return mGravity;								}
	inline RefOptional< ImpactVector > getFootingImactData() const			{ return mFootingImpactData;					}
	inline PxVec3 getRealPosition() const									{ return mPxController.getPosition();			}
	inline RefOptional< PxVec3 > getTracedSlopeDir() const					{ return mTracedSlopeDir;						}
	inline PxVec3 getLocalVelocity() const									{ return mVelocity.getCurrLocalVelocity();		}
	inline RefOptional< PxVec3 > getFootingContactNormal() const			{ return mFootingContactNormal;					}
	inline PxVec3 getCurrentVecVelocity() const								{ return mVelocity.getCurrVelocity();			}
	inline PxVec3 getLastLandingVelocity() const							{ return mLastLandingVelocity;					}
	CollisionHistory& getCollisionHistory()									{ return mCollisionHistory;						}
	LockablePxController& getPhysxController()								{ return mPxController;							}
	const LockablePxController& getPhysxController() const					{ return mPxController;							}
	inline bool isCCVolumeNotOverlapped() const								{ return mLastFrameCCVolumeIsNotOverlapped;							}
	inline bool charCtrlDownCollisions()									{ return charCtrlDownCollisions( mCurrentCollisionFlags );			}
	inline bool charCtrlSidesAndDownCollisions()							{ return charCtrlSidesAndDownCollisions( mCurrentCollisionFlags );	}
	
	bool canJump();
	PxVec3 getInterpolatedPosition() const;
	void pxMove( InVec3 move );

private:

	void updateFootingMaterialsAndFriction();

	void correctTakeoffDescent();
 
	PxVec3 makeTestMove( InVec3 move );

	void pxMove( InVec3 vec, PxU32 & flags, bool eraseFooting = false );
	void consumeVelocity( PxVec3& source, float consumeVelocityMagnitude );
	void correctElevation( InVec3 originPos );
	bool correctElevationOnBodies( const PxVec3 & originPos, float movementRotationAngle, int calledTimes);

	bool checkPatternStep( const PxVec3& originPos );
	bool verifyStepJerkingPatternAndStepPattern( const PxVec3& originPos );
	void updateSlitheringOnSlopeActivation();
	void updateFooting( const FootingData& data );
	void updateGlobalRidingMovementInfluence();
	void findEdgeBestSlope( const PxControllerShapeHit& hit );

public:
	
	CCVelocity mVelocity;

private:

	float mDt;

	PxShape * mFootingShape;
	RefOptional< PxVec3 > mTouchedShapeLocalPos;
	RefOptional< PxVec3 > mTouchedShapeGlobalPos;
	
	bool mLastFrameCCVolumeIsNotOverlapped;	
	
	PxControllerShapeType::Enum mBodyType;

	PxVec3 mBeforePxMovePos;
	
	ValueOptional< float > mCurrentFootingPathSlope;
	RefOptional< PxVec3 > mFootingContactNormal;

	RefOptional< ImpactVector > mFootingImpactData;

	float mPxMoveStepOffset;

	RefOptional< PxVec3 > mSlopeSlitheringAcceleration;

	// jerking on stairs additional data
	ValueOptional< float > mActivateStepMoveLengthXZ;
	std::vector< const FootingData > mFootingData;
		
	bool mTestMovementActivated;
	PxVec3 mLastLandingVelocity;
 
	ValueOptional< CCBodyListener* > mListener;
	ValueOptional< const PxMaterial* > mCurrentMaterial;

	float mCurrentStaticFriction;
	float mCurrentDynamicFriction;
	PxU32 mCurrentCollisionFlags;

	float mGravity;

	float mInterpolatedTimeStep;
	PxVec3 mInterpolatedPosition;
	RefOptional< PxVec3 > mTracedSlopeDir;

	PxVec3 mPreviousGlobalVelocity;
	PxVec3 mPreviousLocalMove;
	PxVec3 mPreviousGlobalMove;
	PxVec3 mPreviousTotalMove;
	
	Switch< PxVec3, SWITCH_SIZE > mPositionsHistory;
	Switch< PxVec3, SWITCH_SIZE > mLocalVelocityHistory;
	Switch< float, SWITCH_SIZE > mTimeSteps;
	CollisionHistory mCollisionHistory;
 
	LockablePxController mPxController; // here must be deleted PxController
	const AbstractCC* mOwner;
	const CCFrictionAdapter* mFrictionAdapter;
	const CCSlopeMovementLimitationsProperties& mSlopeLimitationProps;
	const CCMovementProperties* mCurrentMovementProps;
	
}; // class CCBody


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __CCBody_HPP__
