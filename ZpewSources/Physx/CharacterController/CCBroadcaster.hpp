#ifndef __CCBroadcaster_HPP__
#define __CCBroadcaster_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Common/AutoConfiguration.hpp"
#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCBroadcaster
	:	public AbstractCCInput
{

public:

	CCBroadcaster( std::set< AbstractCC * > & inputSet )
		:	mInputSet( inputSet )
	{}

	virtual void strafeLeftOn();
	virtual void strafeLeftOff();

	virtual void strafeRightOn();
	virtual void strafeRightOff();

	virtual void moveForwardOn();
	virtual void moveForwardOff();

	virtual void moveBackwardOn();
	virtual void moveBackwardOff();

	virtual void jumpOn();
	virtual void jumpOff();

	virtual void duckDown();
	virtual void duckUp();
	virtual void run();
	virtual void walk();

	virtual void resetInput();

private:

	std::set< AbstractCC * >& mInputSet;
};

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __CCBroadcaster_HPP__