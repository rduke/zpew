#ifndef __ZPEW_CharacterControllerGameParameters_HPP__
#define __ZPEW_CharacterControllerGameParameters_HPP__

#include "ZpewSources/Physx/CharacterController/AbstractCC.hpp"
#include "ZpewSources/Physx/CharacterController/CCInput.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  CCGameParameters
	:	public CCInput
{

public:

	CCGameParameters();

	virtual float getControllerMinHeight() const														{ return mControllerMinHeight;		}
	virtual float getControllerMaxHeight() const														{ return mControllerMaxHeight;		}
	virtual float getPlayerMass() const																	{ return mPlayerMass;				}

	virtual float getDuckingDownVelocity() const														{ return mDuckingDownVelocity;		}
	virtual float getDuckingUpVelocity() const															{ return mDuckingUpVelocity;		}
	virtual float getStrafeSidesCoefficient() const														{ return mStrafeSidesCoefficient;	}

	virtual void setControllerMinHeight( float value );
	virtual void setControllerMaxHeight( float value );

	virtual void setPlayerMass( float value );
	virtual void setDuckingDownMultiplier( float value );
	virtual void setDuckingUpMultiplier( float value );
	virtual void setStrafeSidesCoefficient( float value );

	virtual const CCMovementProperties& getWalkMovementProperties() const									{ return mWalkMovementProperties;}
	virtual void setWalkMovementProperties( const CCMovementProperties& valueproperties );

	virtual const CCMovementProperties& getRunMovementProperties() const									{ return mRunMovementProperties; }
	virtual void setRunMovementProperties( const CCMovementProperties& valueproperties );

	virtual const CCAfterJumpDuckProperties& getAfterJumpDuckProperties() const								{ return mAfterJumpDuckScenario; }
	virtual void setAfterJumpDuckProperties( const CCAfterJumpDuckProperties& valueproperties );

	virtual const CCSlopeMovementLimitationsProperties& getSlopeMovementLimitationsProperties() const		{ return mSlopeProperties; }
	virtual void setSlopeMovementLimitationsProperties( CCSlopeMovementLimitationsProperties& valueproperties );

	virtual const CCJumpProperties& getJumpProperties() const												{ return mJumpProperties; }
	virtual void setJumpProperties( CCJumpProperties& value );

private:

	void makeDefaultAfterJumpDuckProperties();
	void makeDefaultMovementProperties();
	void makeDefaultJumpProperties();
	void makeDefaultSlopeMovementProps();

	void setMovementProperties( const CCMovementProperties& valueproperties, CCMovementProperties& valuedestination );

protected:


	struct AfterJumpDuckScenario
		:	public CCAfterJumpDuckProperties
	{
		bool mStayDuckedPhysicLogic;
		float mCurrentDuckTimeCounter;

	}; // struct AfterJumpDuckProperties


	AfterJumpDuckScenario mAfterJumpDuckScenario;	
	CCMovementProperties mRunMovementProperties;
	CCMovementProperties mWalkMovementProperties;
	CCJumpProperties mJumpProperties;
	CCSlopeMovementLimitationsProperties mSlopeProperties;

private:

	float mControllerMinHeight;
	float mControllerMaxHeight;
	float mPlayerMass;
	float mDuckingDownVelocity;
	float mDuckingUpVelocity;
	float mStrafeSidesCoefficient;

}; // class CharacterControllerGameParameters


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_CharacterControllerGameParameters_HPP__
