#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CCCore.hpp"
#include "ZpewSources/Physx/CharacterController/CCManager.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCCore::CCCore( const CCDescription & description, CCManager & CCManager )
	:	mCurrentFaceAngle( description.mOrientationAngleXZ )
	,	mTimeSimulator( 0 )
	,	mCurrentJumpAcceleration( 0 )
	,	mCCBody(
				CCGameParameters::mSlopeProperties
			,	description.mPxMoveStepOffset
			,	this
			,	this
		)
	,	mIdealAcceleration( 0 )
	,	mIdealMaxVelocity( 0 )
	,	mCCManager( CCManager )
	,	mBodyType( PxControllerShapeType::eCAPSULE )
	,	mCurrentHeight( description.mShapeDescrition.mCurrentHeight )
	,	mAccumulatedTimeSinceLastJumpFinished( 0 )
{
	mSelectedMovementProps = &mWalkMovementProperties;
	Assert( description.mFixedTimeStepSize < 1.0f );

	if ( description.mFixedTimeStepSize > 0 )
		mTimeSimulator.reset( new FixedTimeSimulator( description.mFixedTimeStepSize ) );
	else
		mTimeSimulator.reset( new VariableTimeSimulator() );
	
	setPlayerMass( description.mMass );
	
	//Assert ( description.m_shapeDescrition.m_isBox != description.m_shapeDescrition.m_isCapsule );
	
	PxControllerManager & PxControllerManager = mCCManager.getPhysXControllerManager();
	PxScene & pxScene = mCCManager.getPhysxScene();
	
	PxCapsuleControllerDesc desc;
	desc.setToDefault();
	desc.position.x = description.mShapeDescrition.mPosition.x;
	desc.position.y = description.mShapeDescrition.mPosition.y;
	desc.position.z = description.mShapeDescrition.mPosition.z;

	assertHeight();
	desc.height = description.mShapeDescrition.mCurrentHeight - 2.f * description.mShapeDescrition.mRadius;
	Assert( desc.height > 0 );

	desc.radius = description.mShapeDescrition.mRadius;
	desc.contactOffset = CC_OFFSET;
	replaceWithBigger( desc.contactOffset, getControllerMinHeight() / 50.f );
	replaceWithBigger( desc.contactOffset, description.mShapeDescrition.mRadius / 40.f );

	//ACHTUNG!
	desc.slopeLimit = 0;// degrees( mSlopeProperties.mMaxElevationAngle ).cos();
	desc.stepOffset = description.mPxMoveStepOffset;
	desc.upDirection = PxVec3( 0, 1, 0 );

	desc.climbingMode = PxCapsuleClimbingMode::eCONSTRAINED;
	desc.nonWalkableMode = PxCCTNonWalkableMode::ePREVENT_CLIMBING;
	desc.invisibleWallHeight = mCurrentHeight;
	desc.maxJumpHeight = mJumpProperties.getOriginJumpVelocity( mCCBody.getGravity(), mJumpProperties.mIdealAccelPath ) * 2.0f;

	desc.material = getManager().getCharacterControllerPxMaterial();
	desc.groupsBitmask = -1;
	desc.interactionMode = PxCCTInteractionMode::eINCLUDE;

	desc.callback = &mCCBody;
	desc.behaviorCallback = &mCCBody;
	desc.material = mCCManager.getCharacterControllerPxMaterial();

	Assert( desc.isValid() );

	PxController * builtPxController = PxControllerManager.createController( mCCManager.getPhysics(), & pxScene, desc );	

	//TODO: use vector gravity, no gust Y!
	mCCBody.setGravity( pxScene.getGravity().y );
	mCCBody.setPxController( *builtPxController );

	if( description.mFrictionAdapter )
		mCCBody.setCharacterControllerFrictionAdapter( description.mFrictionAdapter );
	else
		mCCBody.setCharacterControllerFrictionAdapter( &mCCManager.getDefaultCharacterControllerFrictionAdapter() );

	builtPxController->setGroupsBitmask( mCCManager.getCharacterControllerCollisionGroup() );

	PxShape* builtCCShape;
	builtPxController->getActor()->getShapes( &builtCCShape, 1 );
	builtCCShape->setFlag( PxShapeFlag::eUSE_SWEPT_BOUNDS, true );
	Assert( builtPxController->getActor()->getNbShapes() == 1 );
		
} // CCCore::CharacterController


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::update( float dt )
{
	mCCBody.updateInterpolatedPosition( dt );
	if( ! mTimeSimulator->update( dt ) )
		return;

	mDt = mTimeSimulator->getCurrentTimeStep();

	mCCBody.setGravity( mCCBody.getPhysxController().getActor()->getScene()->getGravity().y );
	updateJumping();
	mCCBody.update( mDt, mSelectedMovementProps );

	updateCharacterExtents();
	prepareAfterJumpScenarios();
	
	if ( mCCBody.mVelocity.getCurrLocalVelocity().y > 0 )
		mJumpLaunched = true;

	if ( mJumpLaunched || mCCBody.checkLandingPattern() )
	{
		mJumpLaunched = false;
		mAccumulatedTimeSinceLastJumpFinished = 0;
	}
	
	mAccumulatedTimeSinceLastJumpFinished += mDt;

} // CCCore::update


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::prepareAfterJumpScenarios()
{
	if ( ! mAfterJumpDuckScenario.mIsFeatureEnabled )
	{
		mAfterJumpDuckScenario.mStayDuckedPhysicLogic = false;
		return;
	}


	if ( ! mAfterJumpDuckScenario.mStayDuckedPhysicLogic &&	mCCBody.checkLandingPattern() )
	{
		PxVec3 previousVelocityMPS = mCCBody.getLastLandingVelocity();
		if ( previousVelocityMPS.y > mAfterJumpDuckScenario.mMinActivationYVelocityMPS )
			return;
	
		mAfterJumpDuckScenario.mCurrentDuckTimeCounter -= previousVelocityMPS.y * mAfterJumpDuckScenario.mTimeStayDuckedVelocityCoefficient;

		clamp( 0.1f, 0.2f, mAfterJumpDuckScenario.mCurrentDuckTimeCounter );
	}


	if (
			mAfterJumpDuckScenario.mCurrentDuckTimeCounter > 0
		&&	fabs( mCurrentHeight - getControllerMinHeight() ) > 0.01f
	)
	{
		mAfterJumpDuckScenario.mStayDuckedPhysicLogic = true;
		mAfterJumpDuckScenario.mCurrentDuckTimeCounter -= mDt;
		
		if ( mAfterJumpDuckScenario.mCurrentDuckTimeCounter < 0 )
			 mAfterJumpDuckScenario.mCurrentDuckTimeCounter = 0;
	}
 	else
 	{
 		mAfterJumpDuckScenario.mCurrentDuckTimeCounter = 0;
 		mAfterJumpDuckScenario.mStayDuckedPhysicLogic = false;
 	}
	
} // CCCore::prepareAfterJumpScenarios


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



bool
CCCore::isCoreOrderingDucking()
{
	if( mAfterJumpDuckScenario.mIsFeatureEnabled )
	{
		if ( mAfterJumpDuckScenario.mStayDuckedPhysicLogic )
			return true;
	}
	if ( mJumpStage.isDucking() )
		return true;

	return false;

} // CCCore::isCoreOrderingDucking


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::updateCharacterExtents()
{
	if ( ! checkVolume() )
		return;

	bool shouldDuckNow = isCoreOrderingDucking();
	if ( ! shouldDuckNow )
		shouldDuckNow = isUserDucking();

	float delta = 0;
	if ( shouldDuckNow )
	{
		if ( fabs( getControllerMinHeight() - mCurrentHeight ) < 0.00001f )
			return;

		delta = - getDuckingDownVelocity() * mDt;
	}
	else
	{
		if ( fabs( getControllerMaxHeight() - mCurrentHeight ) < 0.00001f )
			return;

		delta = getDuckingUpVelocity() * mDt;
	}

	if ( mCCBody.checkStayGroundPattern() )
		resizeHeightOnGround( delta );
	else
		resizeHeightInFlight( delta );

	Assert( checkVolume() );
	assertHeight();

} // CCCore::updateCharacterExtents


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::resizeHeightOnGround( const float delta )
{
	LockablePxController& controller = getPhysxController();

	Assert( mBodyType==PxControllerShapeType::eCAPSULE );

	PxCapsuleController& capsuleCtrl = static_cast< PxCapsuleController& >( * controller.get() );
		
	const PxVec3 position = controller.getPosition();
	float newHeight = mCurrentHeight + delta;
	clamp( getControllerMinHeight(), getControllerMaxHeight(), newHeight );
	PxVec3 newPosition( position.x, position.y + ( newHeight-mCurrentHeight ) * 0.5f, position.z );

	const float r = capsuleCtrl.getRadius();

	if ( !checkVolume( r, newHeight - 2 * r, newPosition ) )
		return;

	mCurrentHeight = newHeight;
		
	Assert( mCurrentHeight - 2*r >= 0 );
	capsuleCtrl.setHeight( mCurrentHeight - 2*r );
	//mCCBody.pxMove( PxVec3( 0, newHeight-mCurrentHeight, 0 ) );
	controller.setPosition( newPosition );

} // CCCore::resizeHeightOnGround


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::resizeHeightInFlight( const float delta )
{
	LockablePxController& controller = getPhysxController();

	Assert( mBodyType==PxControllerShapeType::eCAPSULE );
	
	PxCapsuleController& capsuleCtrl = static_cast< PxCapsuleController& >( * controller.get() );
		
	float newHeight = mCurrentHeight + delta;
	clamp( getControllerMinHeight(), getControllerMaxHeight(), newHeight );

	const float r = capsuleCtrl.getRadius();

	if ( delta > 0 )
	{
		if ( !checkVolume( r, newHeight - 2 * r, controller.getPosition() ) )
		{
			resizeHeightOnGround( delta );
			return;
		}
	}

	mCurrentHeight = newHeight;
	
	Assert( mCurrentHeight - 2*r >= 0 );
	capsuleCtrl.setHeight( mCurrentHeight - 2*r );

} // CCCore::resizeHeightInFlight


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCCore::checkVolume() const
{
	Assert( mBodyType == PxControllerShapeType::eCAPSULE );
	const PxCapsuleController& capsuleCtrl = static_cast< const PxCapsuleController& >( * getPhysxController().get() );
	return checkVolume( capsuleCtrl.getRadius(), capsuleCtrl.getHeight(), getPhysxController().getPosition() );

} // CCCore::checkVolume


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CCCore::checkVolume( float radius, float pxHeight, InVec3 pos ) const
{
	Assert( pxHeight >= 0 );
	const PxCapsuleGeometry geom( radius, pxHeight * 0.5f );
	const PxQuat orientation( PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f) );

	PxSweepHit sweep;
	
	PxShape* hit;
	int overlap = mCCManager.getPhysxScene().overlapMultiple(
			geom
		,	PxTransform( pos, orientation )
		,	&hit
		,	1
	);

	if( overlap == -1 || overlap > 1 )
		return false;
	
	return true;

} // CCCore::checkVolume



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::onActivateJump()
{		
	Assert( mAccumulatedTimeSinceLastJumpFinished >= 0 && mJumpProperties.mLastLandingDelayNextJump >= 0 );

	//decomposed for debug
	if ( mAfterJumpDuckScenario.mStayDuckedPhysicLogic || ! mJumpStage.canMakeNewJump() )
		return;

	if( ! mCCBody.canJump() )
		return;

	if ( ! checkVolume() )
		return;

	if ( mJumpProperties.mLastLandingDelayNextJump > mAccumulatedTimeSinceLastJumpFinished )// if both are ZERO, so *>*
		return;
		
	mJumpStage.activateClassicJumpWithDucking();
	
} // CCCore::onActivateJump


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::updateJumping()
{
	if ( mJumpStage.isDucking() )
	{
		mJumpStage.mDuckingElapsedTime += mDt;

		if (
				! isUserJumping()
			||	getCurrentHeight() <= getControllerMinHeight()
			||	mJumpProperties.mIdealAccelPath <= fabs ( getControllerMaxHeight() - getCurrentHeight()	)	
 		)
		{
			if( mJumpProperties.mIdealAccelPath > 0 )
			{
				float pastPath = getControllerMaxHeight() - getCurrentHeight();
				Assert( pastPath >= 0 );
				clamp(
						0.3f * mJumpProperties.mIdealAccelPath
					,	1.0f * mJumpProperties.mIdealAccelPath
					,	pastPath
				);
				mJumpStage.mJumpInitialVerticalVelocity = mJumpProperties.getOriginJumpVelocity( mCCBody.getGravity(), pastPath );
			}
			else
				mJumpStage.mJumpInitialVerticalVelocity = mJumpProperties.getOriginJumpVelocity( mCCBody.getGravity(), 0/* could be any float-value*/ );
		
			mJumpStage.reset();	
		}
	}

} // haracterController::updateJumping


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::setCharacterPos( PxVec3 pos )
{
	getPhysxController().setPosition( PxVec3( pos.x, pos.y, pos.z )	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::applyForwardBackwardAcceleration(
		PxVec3& inOutValue
	,	bool isNegative
	,	InVec3 charCtrlDirection
)
{
	PxVec3 move(
			charCtrlDirection.x * mIdealAcceleration
		,	0
		,	charCtrlDirection.z * mIdealAcceleration
	);	

	if ( isNegative )
		inOutValue -= move;
	else
		inOutValue += move;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::applyLeftRightAcceleration(
		PxVec3& inOutValue
	,	bool isNegative
	,	InVec3 charCtrlDirection
)
{
	float rotYAngle;
	if ( isNegative )
		rotYAngle = - 90;
	else
		rotYAngle = + 90;

	correctAngle_0_360( rotYAngle );
	
	PxVec3 move(
			charCtrlDirection.x * mIdealAcceleration
		,	0
		,	charCtrlDirection.z * mIdealAcceleration
	);	
	
	rotateXZ( move, rotYAngle );

	inOutValue += move;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::onCalculateNextAppliedForce( float realPrevElevationAngle )
{
	updateMovementProps( realPrevElevationAngle );

	const PxVec3 direction = getDirection();
  	PxVec3 frameAcceleration( 0 );
  	
	if ( isUserMovingForward() )
  		applyForwardBackwardAcceleration( frameAcceleration, false, direction );

  	if ( isUserMovingBackward() )
		applyForwardBackwardAcceleration( frameAcceleration, true, direction );
  
  	if ( isUserMovingLeft() )
		applyLeftRightAcceleration( frameAcceleration, true, direction );
  
  	if ( isUserMovingRight() )
  		applyLeftRightAcceleration( frameAcceleration, false, direction );

	float accelerationMagnitude = returnCorrectDivider( frameAcceleration.magnitude() );
	if ( accelerationMagnitude > mIdealAcceleration )
		frameAcceleration *= mIdealAcceleration / accelerationMagnitude;

 	frameAcceleration.y += mJumpStage.mJumpInitialVerticalVelocity / mDt;
	mJumpStage.mJumpInitialVerticalVelocity = 0;

	mCCBody.accelerate(
			frameAcceleration
		,	mIdealMaxVelocity
		,	mSelectedMovementProps->mMinStopVelocity
		,	mSelectedMovementProps->mMaxStopVelocity
		,	mSelectedMovementProps->mStopVelFromCurrVelMultiplier_onGround
		,	mSelectedMovementProps->mStopVelFromCurrVelMultiplier_inFlight
	);

} // CCCore::onCalculateNextAppliedForce


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::updateMovementProps( float angleDegrees )
{
	if( isUserRunning() )
		mSelectedMovementProps = &mRunMovementProperties;
	else
		mSelectedMovementProps = &mWalkMovementProperties;

	float pseudoCurrentHeight;
	if ( isCoreOrderingDucking() )
		pseudoCurrentHeight = getControllerMaxHeight();
	else
		pseudoCurrentHeight = getCurrentHeight();

	mSelectedMovementProps->calculateCurrentVelocityProperties(
			mIdealAcceleration
		,	mIdealMaxVelocity
		,	getControllerMinHeight()
		,	getControllerMaxHeight()
		,	pseudoCurrentHeight
		,	angleDegrees
	);

	assertNonNullDivider( getStrafeSidesCoefficient() );

	if ( ! isUserMovingForward() && ! isUserMovingBackward() )
		mIdealMaxVelocity *= getStrafeSidesCoefficient();

} // CCCore::updateMovementProps


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


ValueOptional< Angle >
CCCore::getFootingOrientationAngle() const
{
	return mCCBody.getFootingOrientationAngle();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


RefOptional< PxVec3 >
CCCore::getBestFootingFaceDir() const
{
	return RefOptional< PxVec3 >( mCCBody.getTracedSlopeDir() );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxRigidDynamic&
CCCore::getCharacterRigidDynamic()
{
	return * mCCBody.getPhysxController().getActor()->isRigidDynamic();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
CCCore::getDirection() const
{
	PxVec3 result;
	result.x = mCurrentFaceAngle.sin();
	result.y = 0;
	result.z = mCurrentFaceAngle.cos();

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCCore::setDirection( InVec3 dir )
{
	float a = getXZAngleBetweenVectors( PxVec3(0, 0, 1), dir );
	correctAngle_0_360( a );
	mCurrentFaceAngle.setDegrees( a );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/