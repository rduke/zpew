#ifndef __CHARACTER_PROPERTIES_HPP__
#define __CHARACTER_PROPERTIES_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCFrictionAdapter;
struct CCMovementProperties;
struct CCShapeDescription;
struct CCDescription;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct EnabledFeature
{
	bool mIsFeatureEnabled;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCMovementProperties
{
	struct SlopeProperties
	{
		void set( float angle, float influence )
		{
			mMaxSlowdownCoefficient = influence;
			mMaxAngleDegrees = angle;
		}

		float getCurrentMovementInfluence( float angleDegree ) const;

		// slope angle, used as interpolation variable
		float mMaxAngleDegrees;

		// this coefficient is maximum slowdown on slope with angle equal to mMaxAngleDegrees
		// used as linear interpolation from ( 1, 0-deg slope ) to ( mMaxSlowdownCoefficient, mMaxAngleDegrees )
		// 0 maximum slowdown, 1 - no slowdown
		float mMaxSlowdownCoefficient; // multiplier
	
	}; // struct SlopeProperties
	

	void calculateCurrentVelocityProperties(
			float& currentAccelerationMPS
		,	float& currentMaxVelocity
		,	float minHeight
		,	float maxHeight
		,	float currentHeight
		,	float currentSlopeAngleDegree
	);

	// max velocity and max acceleration depends on CC height
	// if there's no need of such feature next values should be equal
	// mMinHeightAccelerationMPS == mMaxHeightAccelerationMPS
	// mMinHeightFlatMaxVelocity == mMaxHeightFlatMaxVelocity
	//{
		float mMinHeightAccelerationMPS;
		float mMaxHeightAccelerationMPS;

		float mMinHeightFlatMaxVelocity;
		float mMaxHeightFlatMaxVelocity;
	//}

	// maxVelocity on ascent/descent = currentHeightFlatMaxVelocity * calculatedSlopeInfluence( angle );
	SlopeProperties mAscentInfluence;
	SlopeProperties mDescentInfluence;

	// stop velocity is applied as deceleration to CC
	// stopVel = currentMoveVel * mStopVelFromCurrVelMultiplier_X
	// and then corrected - clamp( mMinStopVelocity,mMaxStopVelocity,stopVel );

	// 0.. +inf
	float mStopVelFromCurrVelMultiplier_onGround;
	// 0.. +inf
	float mStopVelFromCurrVelMultiplier_inFlight;

	// 0.. +inf
	float mMinStopVelocity;
	// 0.. +inf, mMaxStopVelocity >= mMinStopVelocity
	float mMaxStopVelocity;

	// corrects ground following logic
	// used to eliminate takeoff while moving down the slope
	// might be 0..+inf 
	// 0 - no correction
	// +inf - stick to ground always
	float mMinYVelocityTakeoff;

private:

	static float getCurrentResultByProportion(
			float minSample
		,	float maxSample
		,	float currentSample
		,	float resultMin
		,	float resultMax
	);

}; // struct MovementProperties



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCShapeDescription
{
	CCShapeDescription();
	bool isValid() const;

public:

	PxVec3 mPosition;

	// used capsule radius - co equal to PxCapsuleGeometry.radius
	float mRadius;	

	// total height - co equal to PxCapsuleGeometry.halfHeight * 2 + 2 * mRadius
	float mCurrentHeight;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCDescription
{
	CCDescription ();

	CCFrictionAdapter* mFrictionAdapter;
	CCShapeDescription mShapeDescrition;

public:

	Angle mOrientationAngleXZ;
	float mMass;

	// if zero or less then 0 - non fixedTimeStep
	float mFixedTimeStepSize;

	// step offset o climb over high slopes ( close to 90degs ), stairs etc
	// should be lower then height height, and in ideal case - not higher then radius of capsule shape
	float mPxMoveStepOffset;

}; // struct CharacterDescription



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCJumpProperties
{
public:

	float getOriginJumpVelocity( float gravity, float pastAccelPath ) const;

public:

	float mJumpMaxHeight; 

	// used to duck before jump, currentHEight + mIdealAccelPath must be lower
	// then maxHeight, to make most efficient jump
	// 0..+inf
	float mIdealAccelPath;

	// delay next jump begin, after landing is done
	float mLastLandingDelayNextJump;

	// velocities to use check landing pattern
	// both must be:
	// mLandingPattern_PrevFrameMinYVeloicty < 0 
	// mLandingPattern_CurrFrameMaxYVeloicty <= 0
	// by correct logic mLandingPattern_CurrFrameMaxYVeloicty - must be equal to == -epsyone + 0
	// and mLandingPattern_PrevFrameMinYVeloicty must be lower then mLandingPattern_CurrFrameMaxYVeloicty
	// as lower mLandingPattern_PrevFrameMinYVeloicty is, as landing pattern activates at higher -Yvelocity 
	
	//must be almost equal to -1/10 *G
	float mLandingPattern_PrevFrameMinYVeloicty;
	// must be lower then 0, but too close to 0 ~= -0.01f
	float mLandingPattern_CurrFrameMaxYVeloicty;

}; // class CCJumpProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct ImpactVector
{
	ImpactVector( InVec3 n, InVec3 p )
		:	normal( n )
		,	point( p )
	{}

	ImpactVector()	{}

public:

	PxVec3 normal;
	PxVec3 point;

}; // struct ImpactVector


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCAfterJumpDuckProperties
	:	public EnabledFeature
{
	//if vertical velocity is highter then mMinActivationYVelocityMPS - no ducking enabled
	float mMinActivationYVelocityMPS;
	
	// time = mTimeStayDuckedVelocityCoefficient * velocity.y
	float mTimeStayDuckedVelocityCoefficient;

}; // class AfterJumpDuckProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// calls every frame to get friction from material
// or while free-flight, in such case returned friction could be zero -
// to make no stopping or accelerating CC while it has no footing collisions

struct CCFrictionAdapter
{
	// dynamic friction used when CC is accelerating
	// static friction used to stop CC

	virtual float getInflightDynamicFriction() const = 0;
	virtual float getInflightStaticFriction() const = 0;

	virtual float getDynamicFriction( const PxMaterial* mat ) const = 0;
	virtual float getStaticFriction( const PxMaterial* mat ) const = 0;

	virtual ~CCFrictionAdapter() { }

}; // struct CharacterControllerFrictionAdapter


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct CCSlopeMovementLimitationsProperties
{
	// if this angle exceeded - slope limitations and slithering activates...
	// this angle has no need to be equal to or in some other relations with CCMovementProperties::SlopeProperties::mMaxAnglesDegrees
	float mMaxElevationAngle;

	float mSlitheringDownMaxVelocity;

	//applied acceleration when CC is slithering down the slope
	float mXZAccelerationSlitheringDown;

	// A := angle( currently applied SlopeSlithering accel , CC's local acceleration(user accel) )
	// if a > mInitiatedAcceleratonApplyLimitDeviationAngle - then local acceleration won't be applied
	float mInitiatedAcceleratonApplyLimitDeviationAngle;

	// rotation of velocity when hitting high ground - slope > maxElevation angle
	float mMaxSlopeCollidingRotationAngle;

	// CC rotates - and then tests again the footing slope
	// until reached maximum count of calls equal to mMaxSlopeRotationsAppliedAtOneFrame
	// or mMaxSlopeCollidingRotationAngle rotation angle per frame achieved
	// or found slope's angle which make move unconstrained by mMaxElevationAngle;
	// 2 .. 20 = is correct range, the upper value have been chosen due to performance
	int mMaxSlopeRotationsAppliedAtOneFrame;

	// that's value about 0.0 .. 5.0 degrees - additional reflection angle
	// it makes rotation 90 + mPerFrameAdditionalRotationAngle
	// that makes descent move always applied to current footing normal
	// 0 .. 5.0 -is correct available range, the upper value have been chosen due to smooth rotation logic
	float mPerFrameAdditionalRotationAngle;

	// maximum ratio appliedMove/idealMove(or calculated move) - when is checking slithering down pattern
	// if ratio smaller then this coefficient - slithering is not applied
	float mMaximumRatioMoveSlithering;

}; // class SlopeMovementLimitationsProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// default Friction adapter makes most common adaptation for *slippy* and *non-slippy*
// surfaces

class  DefaultCCFrictionAdapter
	:	public CCFrictionAdapter
{
public:

	DefaultCCFrictionAdapter( PxScene* scene )
		:	mScene( scene )
	{}

	virtual float getDynamicFriction( const PxMaterial* mat ) const;
	virtual float getStaticFriction( const PxMaterial* mat ) const;

	virtual float getInflightDynamicFriction() const	{ return 0.12f; }
	virtual float getInflightStaticFriction() const		{ return 0.04f; }

	PxScene** getScenePtr()								{ return &mScene; }

private:

	float getTableAdaptation( float friciton ) const;

private:

	PxScene* mScene;

}; // class  DefaultCCFrictionAdapter



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __CHARACTER_PROPERTIES_HPP__
