#ifndef __ZPEW_LOCABLE_CONTROLLER_HPP__
#define __ZPEW_LOCABLE_CONTROLLER_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class LockablePxController;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct OverLockException
	:	public std::exception
{
	OverLockException( const char* _str ) :	std::exception( _str ) {}
};



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCAutoLock
{
public:
	
	CCAutoLock( LockablePxController * _locker );
	
	CCAutoLock( CCAutoLock& other );
	
	CCAutoLock& operator = ( CCAutoLock & other );
	
	~CCAutoLock();
	
	void die();
	
	
private:

	LockablePxController* mLockIFace;

}; // class CCAutoLock


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  LockablePxController
{

public:

	inline LockablePxController( PxController* controller )
		:	mController( controller )
		,	mIsLocked( 0 )
	{}

	void set( PxController* controller )
	{
		if( mController )
			unlock();

		mController = controller;
	}

	inline PxController* get()
	{
		return mController;
	}

	inline const PxController* get() const
	{
		return mController;
	}


	inline PxControllerShapeType::Enum getType() const
	{
		return mController->getType();
	}

	inline void move( const PxVec3& disp, PxU32 activeGroups, float timeStep, PxU32& collisionFlags, PxObstacleContext* context = 0 )
	{
		PxControllerFilters filter;
		filter.mActiveGroups = activeGroups;

		lock();
		collisionFlags = mController->move( disp, 0, timeStep, filter, context );
		unlock();

		checkNumericalOverflowV3( mController->getPosition() );
	}


	inline void setPosition( const PxVec3& position )
	{
		lock();
		mController->setPosition( toVector3<PxExtendedVec3>( position ) );
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}


	inline void setPositionExt( const PxExtendedVec3& position )
	{
		lock();
		mController->setPosition( position );
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}

	inline float getSkinOffset() const
	{
		return mController->getContactOffset();
	}

	inline PxVec3 getPosition() const
	{
		checkNumericalOverflowV3( mController->getPosition() );
		return toVector3<PxVec3>( mController->getPosition() );
	}

	inline PxExtendedVec3 getPositionExt() const
	{
		checkNumericalOverflowV3( mController->getPosition() );
		return mController->getPosition();
	}


	inline PxActor* getActor() const
	{
		return mController->getActor();
	}

	inline void setStepOffset( const float offset )
	{
		lock();
		mController->setStepOffset( offset );
		unlock();
	}

	inline PxCCTInteractionMode::Enum getInteraction() const
	{
		return mController->getInteraction();
	}


	inline void setInteraction( PxCCTInteractionMode::Enum mode )
	{
		lock();
		mController->setInteraction( mode );
		unlock();
	}


	inline void invalidateCache()
	{
		lock();
		mController->invalidateCache();
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}

	inline void* getUserData() const
	{
		return mController->getUserData();
	}


	inline void resize( float height )
	{
		lock();
		mController->resize( height );
		unlock();
	}

	inline void releaseController()
	{
		if( !mController )
			return;

		lock();
		mController->release();
		mController = 0;
		unlock();        
	}

	void lock();
	void unlock();

	inline CCAutoLock autoLock()
	{
		return CCAutoLock( this );
	}

	inline ~LockablePxController()
	{
		releaseController();
	}

	//dangerous!
	void forceUnLock();

private:

	int mIsLocked;
	PxController* mController;

};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_LOCABLE_CONTROLLER_HPP__