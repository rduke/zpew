
#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CCGameParameters.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCGameParameters::CCGameParameters()
	:	mControllerMinHeight( 0.8f )
	,	mControllerMaxHeight( 1.8f )
	,	mPlayerMass( 80 )
	,	mDuckingDownVelocity( 4.5f )
	,	mDuckingUpVelocity( 6.0f )
	,	mStrafeSidesCoefficient( 1.0f )
{
	makeDefaultJumpProperties();
	makeDefaultAfterJumpDuckProperties();
	makeDefaultMovementProperties();
	makeDefaultSlopeMovementProps();
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setControllerMinHeight( float value )
{
	mControllerMinHeight = value;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
CCGameParameters::setControllerMaxHeight( float value )
{
	mControllerMaxHeight = value;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
CCGameParameters::setPlayerMass( float value )
{
	mPlayerMass = value;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setDuckingDownMultiplier( float value )
{
	mDuckingDownVelocity = value;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
CCGameParameters::setDuckingUpMultiplier( float value )
{
	mDuckingUpVelocity = value;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
CCGameParameters::setStrafeSidesCoefficient( float value )
{
	Assert( 0 > value && value < 1.0f );
	mStrafeSidesCoefficient = value;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::makeDefaultAfterJumpDuckProperties()
{
	mAfterJumpDuckScenario.mIsFeatureEnabled = true;
	mAfterJumpDuckScenario.mTimeStayDuckedVelocityCoefficient = 0.015f;
	mAfterJumpDuckScenario.mCurrentDuckTimeCounter = 0;
	mAfterJumpDuckScenario.mStayDuckedPhysicLogic = false;
	mAfterJumpDuckScenario.mMinActivationYVelocityMPS = -3.f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setMovementProperties( const CCMovementProperties& properties, CCMovementProperties& destination )
{
	Assert( properties.mAscentInfluence.mMaxAngleDegrees >= 0 );
	Assert( properties.mDescentInfluence.mMaxAngleDegrees >= 0 );

	Assert( properties.mAscentInfluence.mMaxSlowdownCoefficient >= 0 );
	Assert( properties.mDescentInfluence.mMaxSlowdownCoefficient >= 0 );

	Assert( properties.mMaxHeightAccelerationMPS > 0 );
	Assert( properties.mMinHeightAccelerationMPS > 0 );

	Assert( properties.mMaxHeightFlatMaxVelocity > 0 );
	Assert( properties.mMinHeightFlatMaxVelocity > 0 );

	Assert( properties.mMaxHeightAccelerationMPS >= properties.mMinHeightAccelerationMPS );
	Assert( properties.mMaxHeightFlatMaxVelocity >= properties.mMinHeightFlatMaxVelocity );

	Assert( properties.mStopVelFromCurrVelMultiplier_inFlight >= 0 );
	Assert( properties.mStopVelFromCurrVelMultiplier_onGround >= 0 );
	Assert( properties.mMinStopVelocity >= 0 );
	Assert( properties.mMaxStopVelocity >= 0 );
	Assert( properties.mMinStopVelocity < properties.mMaxStopVelocity );
	Assert( properties.mMinYVelocityTakeoff >= 0 );
	
	destination = properties;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setAfterJumpDuckProperties( const CCAfterJumpDuckProperties& properties )
{
	mAfterJumpDuckScenario.mIsFeatureEnabled = properties.mIsFeatureEnabled;
	if( !properties.mIsFeatureEnabled )
		return;

	Assert( properties.mTimeStayDuckedVelocityCoefficient >= 0 );

	mAfterJumpDuckScenario.mMinActivationYVelocityMPS = properties.mMinActivationYVelocityMPS;
	mAfterJumpDuckScenario.mTimeStayDuckedVelocityCoefficient = properties.mTimeStayDuckedVelocityCoefficient;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setRunMovementProperties( const CCMovementProperties& properties )
{
	setMovementProperties( properties, mRunMovementProperties );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setWalkMovementProperties( const CCMovementProperties& properties )
{
	setMovementProperties( properties, mWalkMovementProperties );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::makeDefaultJumpProperties()
{
	mJumpProperties.mJumpMaxHeight = 0.8f;
	mJumpProperties.mIdealAccelPath = 0.3f;
	mJumpProperties.mLastLandingDelayNextJump = 0.07f;
	mJumpProperties.mLandingPattern_PrevFrameMinYVeloicty = -0.5f;
	mJumpProperties.mLandingPattern_CurrFrameMaxYVeloicty = -0.01f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setJumpProperties( CCJumpProperties& value )
{
	mJumpProperties = value;
	Assert( value.mJumpMaxHeight > 0 );
	Assert( value.mIdealAccelPath >= 0 );
	Assert( value.mLastLandingDelayNextJump >= 0 );
	Assert( value.mLandingPattern_PrevFrameMinYVeloicty < 0 );
	Assert( value.mLandingPattern_CurrFrameMaxYVeloicty <= 0 );
	
	mJumpProperties = value;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::makeDefaultMovementProperties()
{
	mRunMovementProperties.mAscentInfluence.set( 60, 0.3f );
	mRunMovementProperties.mDescentInfluence.set( 60, 0.8f );

	mRunMovementProperties.mMaxHeightAccelerationMPS = 25.f;
	mRunMovementProperties.mMinHeightAccelerationMPS = 18.f;

	mRunMovementProperties.mMaxHeightFlatMaxVelocity = 4.5f;
	mRunMovementProperties.mMinHeightFlatMaxVelocity = 2.0f;

	mRunMovementProperties.mStopVelFromCurrVelMultiplier_inFlight = 0.4f;
	mRunMovementProperties.mStopVelFromCurrVelMultiplier_onGround = 7.0f;
	mRunMovementProperties.mMinStopVelocity = 14.0f;
	mRunMovementProperties.mMaxStopVelocity = 26.0f;
	mRunMovementProperties.mMinYVelocityTakeoff = 5.0f;


	mWalkMovementProperties.mAscentInfluence.set( 60, 0.3f );
	mWalkMovementProperties.mDescentInfluence.set( 60, 0.7f );

	mWalkMovementProperties.mMaxHeightAccelerationMPS = 20.0f;
	mWalkMovementProperties.mMinHeightAccelerationMPS = 15.0f;

	mWalkMovementProperties.mMaxHeightFlatMaxVelocity = 2.5f;
	mWalkMovementProperties.mMinHeightFlatMaxVelocity = 1.5f;

	mWalkMovementProperties.mStopVelFromCurrVelMultiplier_inFlight = 0.3f;
	mWalkMovementProperties.mStopVelFromCurrVelMultiplier_onGround = 5.5f;
	mWalkMovementProperties.mMinStopVelocity = 10.0f;
	mWalkMovementProperties.mMaxStopVelocity = 26.0f;
	mWalkMovementProperties.mMinYVelocityTakeoff = 4.0f;

} // CharacterControllerGameParameters::makeDefaultMovementProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::makeDefaultSlopeMovementProps()
{
	CCSlopeMovementLimitationsProperties props;
	props.mInitiatedAcceleratonApplyLimitDeviationAngle = 45.0f;
	props.mMaxElevationAngle = 45.0f;
	props.mSlitheringDownMaxVelocity = 20;
	props.mXZAccelerationSlitheringDown = 5.0f;
	props.mMaxSlopeCollidingRotationAngle = 135.0f;
	props.mPerFrameAdditionalRotationAngle = 1.0f;
	props.mMaxSlopeRotationsAppliedAtOneFrame = 3;
	props.mMaximumRatioMoveSlithering = 5.0f;

	setSlopeMovementLimitationsProperties( props );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCGameParameters::setSlopeMovementLimitationsProperties( CCSlopeMovementLimitationsProperties& properties )
{
	Assert(
			properties.mInitiatedAcceleratonApplyLimitDeviationAngle >= 0
		&&	properties.mInitiatedAcceleratonApplyLimitDeviationAngle < 90
	);

	Assert( properties.mMaxElevationAngle > 0 && properties.mMaxElevationAngle < 90 );
	Assert( properties.mSlitheringDownMaxVelocity > 0 );
	Assert( properties.mXZAccelerationSlitheringDown > 0 );
	Assert( properties.mMaxSlopeCollidingRotationAngle >= 90 && properties.mMaxSlopeCollidingRotationAngle <= 180 );
	Assert( properties.mMaxSlopeRotationsAppliedAtOneFrame > 2 && properties.mMaxSlopeRotationsAppliedAtOneFrame < 20 );
	Assert( properties.mPerFrameAdditionalRotationAngle >= 0 && properties.mPerFrameAdditionalRotationAngle < 5.0f );
	Assert( properties.mInitiatedAcceleratonApplyLimitDeviationAngle >= 0 && properties.mInitiatedAcceleratonApplyLimitDeviationAngle <= 90 );
	Assert( properties.mMaximumRatioMoveSlithering > 1.0f );
	mSlopeProperties = properties;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
