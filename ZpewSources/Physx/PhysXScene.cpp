#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/PhysXScene.hpp"
#include "ZpewSources/Physx/PhysXCollisionGroup.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define CC_COUNT 1
#define SHAPES_COUNT 100

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Scene::Scene( World& world, PxSceneDesc* inDesc )
	:	mScene( 0 )
	,	mWorld( world )
	,	mManager( 0 )
	,	mTimeStep(0)
{
	mDefaultMaterial =  mWorld.getPhysics().createMaterial(0.5f, 0.5f, 0.9f);
	Assert( mDefaultMaterial );

	PxSceneDesc sceneDesc( mWorld.getDefaultToleranceScale() );
	if ( ! inDesc )
	{		
		inDesc = &sceneDesc;

		CollisionGroup group;
		inDesc->filterShaderData = group.getCollisionGroups();
		inDesc->filterShaderDataSize = group.getCollisionGroupsSizeBytes();

		inDesc->gravity					= PxVec3( 0, -G, 0 );
		inDesc->filterShader			= & World::getSimulationFilterCallback;
		inDesc->simulationEventCallback	= 0;
 		inDesc->flags |=
 				PxSceneFlag::eENABLE_SWEPT_INTEGRATION
 			|	PxSceneFlag::eENABLE_ACTIVETRANSFORMS
			//|	PxSceneFlag::eENABLE_KINEMATIC_PAIRS
			|	PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS
 		;
		
		inDesc->cpuDispatcher = mWorld.getCpuDispatcher();
		inDesc->gpuDispatcher = mWorld.getGpuDispatcher();
	}

	if( inDesc->isValid() )
		mScene = mWorld.getPhysics().createScene( *inDesc );

	Assert( mScene );
	
	

	mScene->setVisualizationParameter( PxVisualizationParameter::eSCALE,			1.0f );
	mScene->setVisualizationParameter( PxVisualizationParameter::eCOLLISION_SHAPES,	1.0f );
// 	mScene->setVisualizationParameter( PxVisualizationParameter::eWORLD_AXES,		1.0f );
// 	mScene->setVisualizationParameter( PxVisualizationParameter::eBODY_ANG_VELOCITY,1.0f );
// 	mScene->setVisualizationParameter( PxVisualizationParameter::eCOLLISION_AXES,	1.0f );
// 	mScene->setVisualizationParameter( PxVisualizationParameter::eCOLLISION_EDGES,	1.0f );
// 	mScene->setVisualizationParameter( PxVisualizationParameter::eCOLLISION_FNORMALS,1.0f );
	mScene->setVisualizationParameter( PxVisualizationParameter::eCONTACT_FORCE,	1.0f );
	mScene->setVisualizationParameter( PxVisualizationParameter::eCONTACT_NORMAL,	1.0f );
	mScene->setVisualizationParameter( PxVisualizationParameter::eCONTACT_POINT,	1.0f );

	mManager = new ACC::CCManager( mWorld.getPhysics(), *mScene, mWorld.getControllerManager(), 0 );
	
	Assert( mManager );
	

	ACC::CCDescription charDesc;
	charDesc.mFixedTimeStepSize = -1;
	charDesc.mMass = 80;
	charDesc.mPxMoveStepOffset = 0.2f;
	charDesc.mOrientationAngleXZ = degrees(0);

	charDesc.mShapeDescrition.mCurrentHeight = 1.78f;
	charDesc.mShapeDescrition.mPosition = PxVec3( 0, 300, 0 );
	charDesc.mShapeDescrition.mRadius = 0.20f;

	charDesc.mFixedTimeStepSize = -1.0f / 50.0f;
	for( int i=0; i<CC_COUNT; i++ )
	{
		charDesc.mShapeDescrition.mPosition.x = float( ::rand() % 50 - 25 );
		charDesc.mShapeDescrition.mPosition.y = float( ::rand() % 10 + 5  );
		charDesc.mShapeDescrition.mPosition.z = float( ::rand() % 50 - 25 );

		if( i==0 )
			//charDesc.mShapeDescrition.mPosition = PxVec3( -6, 20, 15 );
			charDesc.mShapeDescrition.mPosition = PxVec3( -0, 10, -5 );

		if( i==1 )
			charDesc.mShapeDescrition.mPosition = PxVec3( 2, 7, 10.5 );

		if( i==2 )
			charDesc.mShapeDescrition.mPosition = PxVec3( 2, 13, 10.5 );

		mManager->buildCC( charDesc );

	}
	

	PxRigidStatic* plane = PxCreatePlane(
			mWorld.getPhysics()
		,	PxPlane( PxVec3(0,1,0), 0 )
		,	*mWorld.getPhysics().createMaterial( 0.3f, 0.4f, 0.5f )
	);
	assert( plane );
	mScene->addActor(*plane);

	createRandomBoxes( SHAPES_COUNT, PxVec3( 0,0,0 ), 18 );

	
	createStairs( 0.05f, 0.20f, PxVec3( 0,0,0), 50 );	
	createStairs( 0.10f, 0.25f, PxVec3(-2,0,0), 50 );
	createStairs( 0.15f, 0.30f, PxVec3(-4,0,0), 50 );
	createStairs( 0.20f, 0.35f, PxVec3(-6,0,0), 50 );
	createStairs( 0.25f, 0.37f, PxVec3(-8,0,0), 40 );
	createStairs( 0.30f, 0.40f, PxVec3(-10,0,0), 20 );
	createStairs( 0.35f, 0.42f, PxVec3(-12,0,0), 10 );
	createStairs( 0.40f, 0.45f, PxVec3(-14,0,0), 10 );
	createStairs( 0.45f, 0.49f, PxVec3(-16,0,0), 10 );
	createStairs( 0.50f, 0.53f, PxVec3(-18,0,0), 10 );

	createStairs( 0.05f, 0.10f, PxVec3(-20,0,0), 20 );
	createStairs( 0.10f, 0.10f, PxVec3(-22,0,0), 20 );
	createStairs( 0.15f, 0.10f, PxVec3(-24,0,0), 20 );
	createStairs( 0.25f, 0.10f, PxVec3(-26,0,0), 20 );

	createStairs( 0.30f, 0.10f, PxVec3(-28,0,0), 20 );
	createStairs( 0.33f, 0.10f, PxVec3(-30,0,0), 20 );
	createStairs( 0.35f, 0.10f, PxVec3(-32,0,0), 20 );
	createStairs( 0.38f, 0.10f, PxVec3(-34,0,0), 20 );

	float length = 20;
	int count = 20;
	for( int i=0; i<count; i++ )
	{
		createRamp(
				20 + i * ( 45.f / count )
			,	10
			,	length / count
			,	PxVec3( 10 + length / count * i * 2, 0, 0 )
		);
	}

	createKinematicMovement( PxVec3( 2, 0.3f, 0 ), PxVec3( 1, 0.1f, 1 ), PxVec3( 0, 0, 1 ), 10.f );
	createKinematicMovement( PxVec3( 2, 0.3f, -4 ), PxVec3( 1, 0.1f, 1 ), PxVec3( 0, 2.0, 0 ), 5.f );
	createKinematicMovementSphere( PxVec3( 4, 0.3f, -4 ), 2 , PxVec3( 0, 2.0, 0 ), 5.f );
	createKinematicMovementSphere( PxVec3( 4, -0.6f, -4 ), 1.2f , PxVec3( 0, 0, 1 ), 10.f );
	createMeshCarrot(
			PxVec3( 0, 0.6f, -10 )
		,	buildQuat< PxQuat, PxVec3 >( degrees(45), PxVec3(1,0,0) )
	);

// 	createMeshX2(
// 			PxVec3( 2, 0.6f,- 8 )
// 		,	buildQuat< PxQuat, PxVec3 >( degrees(0), PxVec3(1,0,0) )
// 	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createKinematicMovement( PxVec3 pos, PxVec3 extents, PxVec3 move, float maxDist )
{
	PxRigidDynamic* actorKin = mWorld.getPhysics().createRigidDynamic( PxTransform ( pos ) );
	actorKin->setRigidDynamicFlag( PxRigidDynamicFlag::eKINEMATIC, true );
	
	KinematicVolume kv;
	kv.actor = actorKin;
	kv.extent = extents;
	kv.maxDist = maxDist;
	kv.speed = move;
	kv.originPos = pos;

	mKinematics.push_back(kv);
	
	PxShape * shapeKin = actorKin->createShape( PxBoxGeometry( extents ), *mDefaultMaterial );
	shapeKin->setFlag( PxShapeFlag::eUSE_SWEPT_BOUNDS, true );
	mScene->addActor( * actorKin );
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createKinematicMovementSphere( PxVec3 pos, float radius, PxVec3 move, float maxDist )
{
	PxRigidDynamic* actorKin = mWorld.getPhysics().createRigidDynamic( PxTransform ( pos ) );
	actorKin->setRigidDynamicFlag( PxRigidDynamicFlag::eKINEMATIC, true );

	KinematicVolume kv;
	kv.actor = actorKin;
	kv.extent.x = radius;
	kv.extent.y = radius;
	kv.extent.z = radius;
	kv.maxDist = maxDist;
	kv.speed = move;
	kv.originPos = pos;

	mKinematics.push_back(kv);

	PxShape * shapeKin = actorKin->createShape( PxCapsuleGeometry( radius, 0.01f ), *mDefaultMaterial );
	shapeKin->setFlag( PxShapeFlag::eUSE_SWEPT_BOUNDS, true );
	mScene->addActor( * actorKin );
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::updateKinematics( float dt )
{
	std::vector< KinematicVolume >::iterator begin = mKinematics.begin();
	std::vector< KinematicVolume >::iterator end = mKinematics.end();

	while( begin != end )
	{
		KinematicVolume& kv = *begin;
		PxVec3 dist = kv.actor->getGlobalPose().p - kv.originPos;
		float currDistMag = dist.magnitude();
		if ( currDistMag > kv.maxDist )
		{
			if ( ( dist + kv.speed ).magnitude() > currDistMag )
			{
				kv.speed = -kv.speed;
				goto La;
			}

		}

		if ( kv.actor->getGlobalPose().p.y < - kv.extent.y )
		{
			if ( ( dist + PxVec3( 0, kv.speed.y, 0 ) ).magnitude() > currDistMag )
				kv.speed.y = -kv.speed.y;
		}
		
		La:
		kv.actor->setKinematicTarget( PxTransform( kv.actor->getGlobalPose().p + kv.speed * dt ) );
		++begin;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Scene::~Scene()
{
	delete mManager;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createRandomBoxes( int count,  PxVec3 center, float deviation )
{
	for ( int i=0; i<count; i++ )
	{
		PxTransform t(
			PxVec3(
					center.x + random( -deviation, deviation )
				,	center.y + random( -deviation, deviation )
				,	center.z + random( -deviation, deviation )
			)
		);

		PxRigidDynamic* actor = mManager->getPhysics().createRigidDynamic( t );
		assert( actor );

		float x,y,z;
		x = random( 0.1f, 1 );
		y = random( 0.1f, 1 );
		z = random( 0.1f, 1 );

		PxShape * shape = actor->createShape( PxBoxGeometry( x,y,z ), *mDefaultMaterial );
		
		assert( shape );
		actor->setMass( x *y * z  * 1 );
		shape->setFlag( PxShapeFlag::eUSE_SWEPT_BOUNDS, true );
		shape->setFlag( PxShapeFlag::eSIMULATION_SHAPE, true );

		mScene->addActor( *actor );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::update( float dt )
{
	Assert( dt > 0 );
	float m = 1;
	mTimeStep = dt/m;
	
	clamp( 0.f, 0.2f, mTimeStep );

	mManager->updateAllControllers( mTimeStep );
	updateKinematics( mTimeStep );
	mScene->simulate( mTimeStep );
	mScene->fetchResults( true );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createStairs( const float height, const float depth, const PxVec3 pos, const int count )
{
	float x,y,z;
	x=y=z=0;
	for( int i=0; i<count; ++i )
	{
		PxTransform t( PxVec3( x, y, z ) + pos );
		PxRigidStatic* actor = mManager->getPhysics().createRigidStatic( t );
		assert( actor );

		PxShape * shape = actor->createShape(
				PxBoxGeometry( 1, height * 0.5f, 1 )
			,	*mDefaultMaterial
		);

		assert( shape );
		mScene->addActor( *actor );
		
		y += height;		
		x = 0;
		z += depth;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createRamp( float angle, float length, float width, const PxVec3 pos )
{
	PxQuat q = buildQuat< PxQuat, PxVec3 >(
			degrees( -angle )
		,	PxVec3(1,0,0)
	);

	PxTransform t( pos, q );
	PxRigidStatic* actor = mWorld.getPhysics().createRigidStatic( t );
	assert( actor );

	PxShape * shape = actor->createShape(
			PxBoxGeometry( width, 0.3f, length )
		,	*mDefaultMaterial
	);
	assert( shape );
	mScene->addActor( *actor );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createMesh( PxVec3 pos, PxQuat q, int size, PxVec3* buff )
{
	PxConvexMeshDesc convexDesc;
	convexDesc.points.count		= size;
	convexDesc.points.stride	= sizeof(PxVec3);
	convexDesc.points.data		= buff;
	convexDesc.flags			= PxConvexFlag::eCOMPUTE_CONVEX;

	MemoryOutputStream outStr;
	Assert( mWorld.getCooking().cookConvexMesh(convexDesc, outStr) );

	MemoryInputData inStr( outStr.getData(), outStr.getSize() );
	PxConvexMesh * mesh= mWorld.getPhysics().createConvexMesh(inStr);

	Assert(mesh);
	PxTransform t( pos, q );

	PxRigidStatic* actor = mWorld.getPhysics().createRigidStatic( t );
	Assert(actor);

	PxShape* shape = actor->createShape(PxConvexMeshGeometry(mesh), *mDefaultMaterial );
	Assert(shape);
	mScene->addActor(*actor);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createMeshX2( PxVec3 pos, PxQuat q )
{
	const int COUNT = 9;
	PxVec3 carrotVerts[] = {
			PxVec3(-4, -2, 4 )
		,	PxVec3( 0, 1, 3  )
		,	PxVec3( 4, -2, 4 )
		,	PxVec3( 2, 3, 2  )
		,	PxVec3( -2, 3, 2 )
		,	PxVec3( -3, 1, 0 )
		,	PxVec3( 0, 2, 0 )
		,	PxVec3( 3, 0.6f, 0 )
		,	PxVec3( 0, -1, -2 )
	};


	createMesh( pos, q, 5, carrotVerts );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::createMeshCarrot( PxVec3 pos, PxQuat q )
{
	const int COUNT = 5;
	PxVec3 carrotVerts[] = {
			PxVec3(0,0,3)
		,	PxVec3(0.5f,0,0)
		,	PxVec3(-0.5f,0,0)
		,	PxVec3(0,0.5f,0)
		,	PxVec3(0,-0.5f,0)
	};

	
	createMesh( pos, q, 5, carrotVerts );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxRigidStatic*
Scene::createHeightField(PxReal* heightmap, PxReal hfScale, PxU32 hfSize)
{
	const PxReal heightScale = 0.001f;

	PxU32 hfNumVerts = hfSize*hfSize;

	PxHeightFieldSample* samples = (PxHeightFieldSample*)malloc( sizeof(PxHeightFieldSample)*hfNumVerts );
	memset( samples,0,hfNumVerts*sizeof(PxHeightFieldSample));

	for( PxU32 x = 0; x < hfSize; x++ )
		for( PxU32 y = 0; y < hfSize; y++ )
		{
			samples[x+y*hfSize].height = (PxI16)(heightmap[y+x*hfSize]/heightScale);
			samples[x+y*hfSize].setTessFlag();
			samples[x+y*hfSize].materialIndex0=1;
			samples[x+y*hfSize].materialIndex1=1;
		}

		PxHeightFieldDesc hfDesc;
		hfDesc.format = PxHeightFieldFormat::eS16_TM;
		hfDesc.nbColumns = hfSize;
		hfDesc.nbRows = hfSize;
		hfDesc.samples.data = samples;
		hfDesc.samples.stride = sizeof(PxHeightFieldSample);

		PxHeightField* heightField = mWorld.getPhysics().createHeightField(hfDesc);
		Assert( heightField );
			

		PxTransform pose = PxTransform::createIdentity();
		pose.p = PxVec3(-(hfSize/2*hfScale),0,-(hfSize/2*hfScale));

		PxRigidStatic* hfActor = mWorld.getPhysics().createRigidStatic(pose);
		Assert( hfActor );
			

		PxHeightFieldGeometry hfGeom(heightField, PxMeshGeometryFlags(), heightScale, hfScale, hfScale);
		PxShape* hfShape = hfActor->createShape( hfGeom, *mDefaultMaterial );
		hfShape->setFlag(PxShapeFlag::eUSE_SWEPT_BOUNDS, true );

		Assert( hfShape);
		mScene->addActor(*hfActor);

		free( samples );

		return hfActor;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


MemoryInputData::MemoryInputData(PxU8* data, PxU32 length) :
	mSize	(length),
	mData	(data),
	mPos	(0)
{
}

PxU32 MemoryInputData::read(void* dest, PxU32 count)
{
	PxU32 length = PxMin<PxU32>(count, mSize-mPos);
	memcpy(dest, mData+mPos, length);
	mPos += length;
	return length;
}

PxU32 MemoryInputData::getLength() const
{
	return mSize;
}

void MemoryInputData::seek(PxU32 offset)
{
	mPos = PxMin<PxU32>(mSize, offset);
}

PxU32 MemoryInputData::tell() const
{
	return mPos;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
