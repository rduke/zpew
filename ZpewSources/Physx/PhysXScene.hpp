#ifndef __ZPEW_PhysXSCENE_HPP__
#define __ZPEW_PhysXSCENE_HPP__

#include "ZpewSources/Physx/CharacterController/CCManager.hpp"
#include "ZpewSources/Physx/PhysXWorld.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Scene
{
public:

	Scene( World& world, PxSceneDesc* desc = 0 );
	~Scene();
	const PxRenderBuffer& getPxRenderBuffer() { return mScene->getRenderBuffer(); }

	void update( float dt );
	void createStairs( const float hight, const float depth, const PxVec3 pos, const int count );

	void createRamp( float angle, float length, float width,const PxVec3 pos );
	void createRandomBoxes( int count,  PxVec3 center, float deviation );

	void createKinematicMovementSphere( PxVec3 pos, float radius, PxVec3 move, float maxDist );
	void createKinematicMovement( PxVec3 pos, PxVec3 extents, PxVec3 move, float maxDist );
	void updateKinematics( float dt );
	void createMeshCarrot( PxVec3 pos, PxQuat q );
	void createMeshX2( PxVec3 pos, PxQuat q );
	void createMesh( PxVec3 pos, PxQuat q, int size, PxVec3* buff );
	PxRigidStatic* createHeightField(PxReal* heightmap, PxReal hfScale, PxU32 hfSize);

public:

	struct KinematicVolume
	{
		PxVec3 extent;
		PxVec3 originPos;
		PxRigidDynamic* actor;
		float maxDist;
		PxVec3 speed;
		bool canSwitch;
	};

	std::vector< KinematicVolume > mKinematics;
	PxMaterial * mDefaultMaterial;
	World& mWorld;
	PxScene* mScene;
	float mTimeStep;

	ACC::CCManager* mManager;

}; // class Scene

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class MemoryOutputStream
	: public PxOutputStream
{
public:
	
	MemoryOutputStream()
		:	mData(NULL)
		,	mSize(0)
		,	mCapacity(0)
	{}
	
	virtual	~MemoryOutputStream()
	{
		if(mData)
			delete[] mData;
	}

	PxU32 write(const void* src, PxU32 size)
	{
		PxU32 expectedSize = mSize + size;
		if(expectedSize > mCapacity)
		{
			mCapacity = expectedSize + 4096;

			PxU8* newData = new PxU8[mCapacity];
			PX_ASSERT(newData!=NULL);

			if(newData)
			{
				memcpy(newData, mData, mSize);
				delete[] mData;
			}
			mData = newData;
		}
		memcpy(mData+mSize, src, size);
		mSize += size;
		return size;
	}


	PxU32		getSize()	const	{	return mSize; }
	PxU8*		getData()	const	{	return mData; }
private:
	PxU8*		mData;
	PxU32		mSize;

	PxU32		mCapacity;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class MemoryInputData: public PxInputData
{
public:
	MemoryInputData(PxU8* data, PxU32 length);

	PxU32		read(void* dest, PxU32 count);
	PxU32		getLength() const;
	void		seek(PxU32 pos);
	PxU32		tell() const;

private:
	PxU32		mSize;
	const PxU8*	mData;
	PxU32		mPos;
};



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace Physx
} // namespace Zpew

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PhysXSCENE_HPP__
