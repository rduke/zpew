#ifndef __VEHICLE_DEMO_HPP__
#define __VEHICLE_DEMO_HPP__


#include "BulletDemoSources/OpenGL/GlutDemoApplication.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "ZpewSources/Bullet/BulletWorld.hpp"

class btCollisionObject;

class VehicleDemo
	:	public GlutDemoApplication
{
public:
	virtual ~VehicleDemo();
	virtual void initPhysics();
	void initPhysics(const char* bspfilename);
	virtual void clientMoveAndDisplay();
	virtual void displayCallback();
	virtual void updateCamera();
	void renderme();
	virtual void specialKeyboard(int key, int x, int y);
	virtual void specialKeyboardUp(int key, int x, int y);

private:
	btCollisionObject *mGroundObject;

	Zpew::bullet::VehicleController		*mVehicleController;
	Zpew::bullet::CharacterController	*mCharacterController;
	Zpew::bullet::Scene					*mScene;

	float								 mEngineForce;
	float								 mBreakingForce;
	float								 mSteerLeft;
	float								 mSteerRight;
	float								 mVehicleSteering;

	float								 mCameraHeight;
	float								 mMinCameraDistance;
	float								 mMaxCameraDistance;
}; // class VehicleDemo


#endif // __VEHICLE_DEMO_HPP__