
#include "VehicleDemo.hpp"
#include "CharacterDemo.h"
#include "BulletDemoSources/OpenGL/GlutStuff.h"
#include "BulletDemoSources/OpenGL/GLDebugDrawer.h"
#include "btBulletDynamicsCommon.h"


GLDebugDrawer	gDebugDrawer;

int main(int argc,char** argv)
{
	GlutDemoApplication* demo = new CharacterDemo;

    demo->initPhysics(); 
	demo->getDynamicsWorld()->setDebugDrawer(&gDebugDrawer);

    return glutmain(
			argc
		,	argv
		,	640
		,	480
		,	"Bullet Vehicle Demo. http://www.continuousphysics.com/Bullet/phpBB2/"
		,	demo
	);
}

