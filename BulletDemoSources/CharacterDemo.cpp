/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "btBulletDynamicsCommon.h"

#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include "BulletDynamics/Character/btKinematicCharacterController.h"

#include "LinearMath/btQuickprof.h"
#include "LinearMath/btIDebugDraw.h"

#include "BulletDemoSources/OpenGL/GLDebugDrawer.h"



#include <stdio.h> //printf debugging


#include "CharacterDemo.h"
#include "BulletDemoSources/OpenGL/GL_ShapeDrawer.h"
#include "BulletDemoSources/OpenGL/GlutStuff.h"

#include "ZpewSources/Bullet/BulletCharacterControllerDesc.hpp"
#include "ZpewSources/Bullet/BulletCharacterController.hpp"
#include "ZpewSources/Bullet/BulletVehicleControllerDesc.hpp"
#include "ZpewSources/Bullet/BulletVehicleController.hpp"
#include "ZpewSources/Bullet/BulletScene.hpp"

const int maxProxies = 32766;
const int maxOverlap = 65535;





#define CUBE_HALF_EXTENTS 1


static int gForward = 0;
static int gBackward = 0;
static int gLeft = 0;
static int gRight = 0;
static int gJump = 0;

#define CUBE_HALF_EXTENTS 1
#define EXTRA_HEIGHT -20.f


CharacterDemo::~CharacterDemo()
{
	//cleanup in the reverse order of creation/initialization

	//remove the rigidbodies from the dynamics world and delete them
	int i;
	for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		m_dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	//delete collision shapes
	for (int j=0;j<m_collisionShapes.size();j++)
	{
		btCollisionShape* shape = m_collisionShapes[j];
		delete shape;
	}
}

void	CharacterDemo::initPhysics()
{
	const char* bspfilename = "Demo.bsp";
	
	initPhysics(bspfilename);
}



void	CharacterDemo::initPhysics(const char* bspfilename)
{
	setTexturing(true);
	setShadows(false);
	
	m_cameraUp = btVector3(0,1,0);
	m_forwardAxis = 1;

	setCameraDistance(22.f);
	m_cameraPosition = btVector3(30.0f,30.0f,30.0f);

	///Setup a Physics Simulation Environment
	mScene = Zpew::bullet::World::getSingletonRef().createScene();
	m_dynamicsWorld = mScene->getDiscreteDynamicsWorld();

	btCollisionShape* groundShape = new btBoxShape(btVector3(50,3,50));
	m_collisionShapes.push_back(groundShape);
	btTransform groundTransform;
	groundTransform.setIdentity ();
	//startTransform.setOrigin (btVector3(0.0, 4.0, 0.0));
	groundTransform.setOrigin (btVector3(0.0f, 0.0f, 0.0f));
	
	m_groundObject = new btCollisionObject();
	m_groundObject->setWorldTransform(groundTransform);
	m_groundObject->setCollisionShape( groundShape );
	m_groundObject->setCollisionFlags(btCollisionObject::CF_STATIC_OBJECT);
	m_dynamicsWorld->addCollisionObject(m_groundObject);
	
	btTransform startTransform;
	startTransform.setIdentity ();
	//startTransform.setOrigin (btVector3(0.0, 4.0, 0.0));
	startTransform.setOrigin (btVector3(10.210098,10.6433364,16.453260));

	Zpew::bullet::CharacterControllerDesc characterControllerDesc;
	characterControllerDesc.defaultConfig();
	characterControllerDesc.mScene = mScene;
	mCharacterController = Zpew::bullet::World::getSingletonRef().
		createCharacterController(&characterControllerDesc);
	
	clientResetScene();
	setCameraDistance(56.f);

}


void CharacterDemo::clientMoveAndDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	float dt = getDeltaTimeMicroseconds() * 0.000001f;
	
	m_dynamicsWorld->stepSimulation(dt);
	mCharacterController->update( dt );

	//rotate view
	if (gLeft)
	{
		mCharacterController->rotateLeft();
	}

	if (gRight)
	{
		mCharacterController->rotateRight();
	}

	if (gForward)
		mCharacterController->walkForward();

	if (gBackward)
		mCharacterController->walkBackward();

	if (gJump)
		mCharacterController->jump();

	//optional but useful: debug drawing
	m_dynamicsWorld->debugDrawWorld();

	renderme();

	glFlush();
	glutSwapBuffers();

}



void CharacterDemo::displayCallback(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 


	renderme();

	//optional but useful: debug drawing
	if (m_dynamicsWorld)
		m_dynamicsWorld->debugDrawWorld();

	glFlush();
	glutSwapBuffers();
}



void CharacterDemo::updateCamera()
{

//#define DISABLE_CAMERA 1
#ifdef DISABLE_CAMERA
	DemoApplication::updateCamera();
	return;
#endif //DISABLE_CAMERA

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	btTransform characterWorldTrans;

	//look at the vehicle
	characterWorldTrans = mCharacterController->getPairCachingGhostObject()->getWorldTransform();
	btVector3 up = characterWorldTrans.getBasis()[1];
	btVector3 backward = -characterWorldTrans.getBasis()[2];
	up.normalize ();
	backward.normalize ();

	m_cameraTargetPosition = characterWorldTrans.getOrigin();
	m_cameraPosition = m_cameraTargetPosition + up * 10.0 + backward * 12.0;
	
	//use the convex sweep test to find a safe position for the camera (not blocked by static geometry)
	btSphereShape cameraSphere(0.2f);
	btTransform cameraFrom,cameraTo;
	cameraFrom.setIdentity();
	cameraFrom.setOrigin(characterWorldTrans.getOrigin());
	cameraTo.setIdentity();
	cameraTo.setOrigin(m_cameraPosition);
	
	btCollisionWorld::ClosestConvexResultCallback cb( characterWorldTrans.getOrigin(), cameraTo.getOrigin() );
	cb.m_collisionFilterMask = btBroadphaseProxy::StaticFilter;
		
	m_dynamicsWorld->convexSweepTest(&cameraSphere,cameraFrom,cameraTo,cb);
	if (cb.hasHit())
	{

		btScalar minFraction  = cb.m_closestHitFraction;//btMax(btScalar(0.3),cb.m_closestHitFraction);
		m_cameraPosition.setInterpolate3(cameraFrom.getOrigin(),cameraTo.getOrigin(),minFraction);
	}

	//update OpenGL camera settings
    glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 10000.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    gluLookAt(m_cameraPosition[0],m_cameraPosition[1],m_cameraPosition[2],
		      m_cameraTargetPosition[0],m_cameraTargetPosition[1], m_cameraTargetPosition[2],
			  m_cameraUp.getX(),m_cameraUp.getY(),m_cameraUp.getZ());
}

//to be implemented by the demo
void CharacterDemo::renderme()
{
	updateCamera();

	btVector3 wheelColor(1,0,0);

	btVector3	worldBoundsMin,worldBoundsMax;
	mScene->getDiscreteDynamicsWorld()->
		getBroadphase()->
		getBroadphaseAabb(worldBoundsMin,worldBoundsMax);

	DemoApplication::renderme();
}


void CharacterDemo::specialKeyboardUp(int key, int x, int y)
{
   switch (key)
    {
    case GLUT_KEY_UP:
	{
		gForward = 0;
	}
	break;
	case GLUT_KEY_DOWN:
	{
		gBackward = 0;
	}
	break;
	case GLUT_KEY_LEFT:
	{
		gLeft = 0;
	}
	break;
	case GLUT_KEY_RIGHT:
	{
		gRight = 0;
	}
	break;
	case GLUT_KEY_F1:
	{
		gJump = 0;
	}
	break;
	default:
		DemoApplication::specialKeyboardUp(key,x,y);
        break;
    }
}


void CharacterDemo::specialKeyboard(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
	{
		gForward = 1;
	}
	break;
	case GLUT_KEY_DOWN:
	{
		gBackward = 1;
	}
	break;
	case GLUT_KEY_LEFT:
	{
		gLeft = 1;
	}
	break;
	case GLUT_KEY_RIGHT:
	{
		gRight = 1;
	}
	break;
	case GLUT_KEY_F1:
	{
		if (mCharacterController && mCharacterController->canJump())
			gJump = 1;
	}
	break;
	default:
		DemoApplication::specialKeyboard(key,x,y);
        break;
    }

//	glutPostRedisplay();


}
