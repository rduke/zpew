/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
#ifndef BSP_DEMO_H
#define BSP_DEMO_H

#include "BulletDemoSources/OpenGL/GlutDemoApplication.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "ZpewSources/Bullet/BulletWorld.hpp"

class btBroadphaseInterface;
class btCollisionShape;
class btCollisionObject;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;

class btCharacterControllerInterface;
class btDynamicCharacterController;
class btKinematicCharacterController;


///Demo shows the convex collision detection, by converting a Quake BSP file into convex objects and allowing interaction with boxes.
class CharacterDemo : public GlutDemoApplication
{
public:

	//keep the collision shapes, for deletion/cleanup
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;

	btBroadphaseInterface*	m_broadphase;

	btCollisionDispatcher*	m_dispatcher;

	btConstraintSolver*	m_solver;

	btDefaultCollisionConfiguration* m_collisionConfiguration;

	btCollisionObject *m_groundObject;

	Zpew::bullet::CharacterController	*mCharacterController;
	Zpew::bullet::Scene					*mScene;
	

	virtual ~CharacterDemo();

	virtual void	initPhysics();

	void	initPhysics(const char* bspfilename);

	virtual void clientMoveAndDisplay();

	virtual void displayCallback();

	virtual void updateCamera();

	void renderme();

	virtual void specialKeyboard(int key, int x, int y);

	virtual void specialKeyboardUp(int key, int x, int y);
	
	static DemoApplication* Create()
	{
		CharacterDemo* demo = new CharacterDemo;
		demo->myinit();
		demo->initPhysics("Demo.bsp");
		return demo;
	}

};

#endif //BSP_DEMO_H


