#include "VehicleDemo.hpp"

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

#include "ZpewSources/Bullet/BulletVehicleControllerDesc.hpp"
#include "ZpewSources/Bullet/BulletVehicleController.hpp"
#include "ZpewSources/Bullet/BulletScene.hpp"

static const float	maxEngineForce			= 1000.f;//this should be engine/velocity dependent
static const float	maxBreakingForce		= 100.f;
static const float	steeringIncrement		= 0.04f;
static const float	steeringClamp			= 0.3f;
static const float	wheelRadius				= 0.5f;
static const float	wheelWidth				= 0.4f;
static const float	wheelFriction			= 1000;//BT_LARGE_FLOAT;
static const float	suspensionStiffness		= 20.f;
static const float	suspensionDamping		= 2.3f;
static const float	suspensionCompression	= 4.4f;
static const float	rollInfluence			= 0.1f;//1.0f;

VehicleDemo::~VehicleDemo()
{
	
}

void VehicleDemo::initPhysics()
{
	mEngineForce	= 0.0f;
	mBreakingForce	= 0.0f;
	mSteerLeft		= 0.0f;
	mSteerRight		= 0.0f;
	mVehicleSteering= 0.0f;

	setTexturing(true);
	setShadows(false);
	
	mCameraHeight = 4.0f;
	mMinCameraDistance = 3.0f;
	mMaxCameraDistance = 10.0f;

	m_cameraUp = btVector3(0,1,0);
	m_forwardAxis = 1;

	setCameraDistance(22.f);
	m_cameraPosition = btVector3(30.0f,30.0f,30.0f);

	///Setup a Physics Simulation Environment
	mScene = Zpew::bullet::World::getSingletonRef().createScene();
	m_dynamicsWorld = mScene->getDiscreteDynamicsWorld();
	m_dynamicsWorld->setGravity(btVector3(0.0f, -10.0f, 0.0f));

	btCollisionShape* groundShape = new btBoxShape(btVector3(50,3,50));
	btTransform groundTransform;
	groundTransform.setIdentity ();
	//startTransform.setOrigin (btVector3(0.0, 4.0, 0.0));
	groundTransform.setOrigin (btVector3(0.0f, -20.0f, 0.0f));

	mScene->localCreateRigidBody(0.f, groundTransform, groundShape);

	Zpew::bullet::VehicleControllerDesc vehicleControllerDesc;
	vehicleControllerDesc.defaultConfig();
	vehicleControllerDesc.mWorldTransform.setOrigin(btVector3(10.0f, 2.0f, 0.0f));
	vehicleControllerDesc.mScene = mScene;
	mVehicleController = Zpew::bullet::World::getSingletonRef().
		createVehicleController( &vehicleControllerDesc );

	clientResetScene();
	setCameraDistance(56.f);
}


void VehicleDemo::clientMoveAndDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	float dt = getDeltaTimeMicroseconds() * 0.000001f;
	
	m_dynamicsWorld->stepSimulation(dt);

	int wheelIndex = 2;
	mVehicleController->
		getRaycastVehicle()->
			applyEngineForce(mEngineForce,wheelIndex);

	mVehicleController->
		getRaycastVehicle()->
			setBrake(mBreakingForce,wheelIndex);
	
	wheelIndex = 3;

	mVehicleController->
		getRaycastVehicle()->
			applyEngineForce(mEngineForce,wheelIndex);

	mVehicleController->
		getRaycastVehicle()->
			setBrake(mBreakingForce,wheelIndex);

	wheelIndex = 0;

	mVehicleController->
		getRaycastVehicle()->
			setSteeringValue(mVehicleSteering,wheelIndex);

	wheelIndex = 1;

	mVehicleController->
		getRaycastVehicle()->
		setSteeringValue(mVehicleSteering,wheelIndex);

	//optional but useful: debug drawing
	m_dynamicsWorld->debugDrawWorld();

	renderme();

	glFlush();
	glutSwapBuffers();
}



void VehicleDemo::displayCallback(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 


	renderme();

	//optional but useful: debug drawing
	if (m_dynamicsWorld)
		m_dynamicsWorld->debugDrawWorld();

	glFlush();
	glutSwapBuffers();
}



void VehicleDemo::updateCamera()
{
	
//#define DISABLE_CAMERA 1
#ifdef DISABLE_CAMERA
	DemoApplication::updateCamera();
	return;
#endif //DISABLE_CAMERA

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	btTransform chassisWorldTrans;

	//look at the vehicle
	mVehicleController->
		getChassis()->
			getMotionState()->getWorldTransform(chassisWorldTrans);
	m_cameraTargetPosition = chassisWorldTrans.getOrigin();

	//interpolate the camera height
#ifdef FORCE_ZAXIS_UP
	m_cameraPosition[2] = (15.0*m_cameraPosition[2] + m_cameraTargetPosition[2] + m_cameraHeight)/16.0;
#else
	m_cameraPosition[1] = (15.0*m_cameraPosition[1] + m_cameraTargetPosition[1] + mCameraHeight)/16.0;
#endif 

	btVector3 camToObject = m_cameraTargetPosition - m_cameraPosition;

	//keep distance between min and max distance
	float cameraDistance = camToObject.length();
	float correctionFactor = 0.f;
	if (cameraDistance < mMinCameraDistance)
	{
		correctionFactor = 0.15*(mMinCameraDistance-cameraDistance)/cameraDistance;
	}
	if (cameraDistance > mMaxCameraDistance)
	{
		correctionFactor = 0.15*(mMaxCameraDistance-cameraDistance)/cameraDistance;
	}
	m_cameraPosition -= correctionFactor*camToObject;
	
	  btScalar aspect = m_glutScreenWidth / (btScalar)m_glutScreenHeight;
        glFrustum (-aspect, aspect, -1.0, 1.0, 1.0, 10000.0);

         glMatrixMode(GL_MODELVIEW);
         glLoadIdentity();

    gluLookAt(m_cameraPosition[0],m_cameraPosition[1],m_cameraPosition[2],
                      m_cameraTargetPosition[0],m_cameraTargetPosition[1], m_cameraTargetPosition[2],
                          m_cameraUp.getX(),m_cameraUp.getY(),m_cameraUp.getZ());
}

//to be implemented by the demo
void VehicleDemo::renderme()
{
	updateCamera();

	btScalar m[16];
	int i;

	btVector3 wheelColor(1,0,0);

	btVector3	worldBoundsMin,worldBoundsMax;
	mScene->getDiscreteDynamicsWorld()->
		getBroadphase()->
		getBroadphaseAabb(worldBoundsMin,worldBoundsMax);

	for (i=0;i<mVehicleController->getRaycastVehicle()->getNumWheels();i++)
	{
		//synchronize the wheels with the (interpolated) chassis worldtransform
		mVehicleController->getRaycastVehicle()->updateWheelTransform(i,true);
		//draw wheels (cylinders)
		mVehicleController->getRaycastVehicle()->getWheelInfo(i).m_worldTransform.getOpenGLMatrix(m);
		m_shapeDrawer->drawOpenGL(m,
			mVehicleController->getWheelShape(),
			wheelColor,
			getDebugMode(),
			worldBoundsMin,
			worldBoundsMax);
	}

	DemoApplication::renderme();
}


void VehicleDemo::specialKeyboardUp(int key, int x, int y)
{
   switch (key)
    {
    case GLUT_KEY_UP:
	{
		mEngineForce = 0.0f;
	}
	break;
	case GLUT_KEY_DOWN:
		mEngineForce = 0.0f;
		break;
	case GLUT_KEY_LEFT:
		mSteerLeft = 0;
		break;
	case GLUT_KEY_RIGHT:
		mSteerRight = 0;
		break;
	default:
		DemoApplication::specialKeyboardUp(key,x,y);
        break;
    }
}


void VehicleDemo::specialKeyboard(int key, int x, int y)
{

//	printf("key = %i x=%i y=%i\n",key,x,y);

    switch (key)
    {
    case GLUT_KEY_UP:
	{
		//gForward = 1;
		mEngineForce = maxEngineForce;
		mBreakingForce = 0.f;
	}
	break;
	case GLUT_KEY_DOWN:
	{
		mBreakingForce = maxBreakingForce; 
		mEngineForce = 0.f;
	}
	break;
	case GLUT_KEY_LEFT:
	{
		//gLeft = 1;
				mVehicleSteering += steeringIncrement;
			if (mVehicleSteering > steeringClamp)
				mVehicleSteering = steeringClamp;
	}
	break;
	case GLUT_KEY_RIGHT:
	{
		//gRight = 1;
				mVehicleSteering -= steeringIncrement;
			if (mVehicleSteering < -steeringClamp)
				mVehicleSteering = -steeringClamp;
	}
	break;
	default:
		DemoApplication::specialKeyboard(key,x,y);
        break;
    }

//	glutPostRedisplay();


}