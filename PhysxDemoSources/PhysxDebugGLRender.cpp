#include "PhysxDemoSources/PCH.hpp"

#include "PhysxDemoSources/PhysxDebugGLRender.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
PhysxDebugGLRender::begin()
{
	glDisable(GL_LIGHTING);
	glLineWidth(1.f);
	glPointSize(1.f);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::end()
{
	glEnable(GL_LIGHTING);
	glutPostRedisplay();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::render(const PxRenderBuffer& _data )
{
	renderPoints(_data);
	renderLines(_data);
	renderTriangles(_data);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::renderPoints( const PxRenderBuffer& _data )
{
	glBegin(GL_POINTS);
	
	const PxDebugPoint * lower = _data.getPoints();
	const PxDebugPoint * upper = lower + _data.getNbPoints();;
	while( lower < upper )
	{
		PxU32 color = lower->color;
		const PxVec3& point = Zpew::vectorXFlip( lower->pos );
		
		setColor( color );
		glVertex3f( point.x, point.y, point.z );

		++lower;
	}
	
	glEnd();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void PhysxDebugGLRender::renderLines( const PxRenderBuffer& _data )
{
	glBegin(GL_LINES);
	const PxDebugLine * lower = _data.getLines();
	const PxDebugLine * upper = lower + _data.getNbLines();
	while( lower < upper )
	{
		{
			PxU32 color = lower->color0;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos0 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}
		
		{
			PxU32 color = lower->color1;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos1 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}

		++lower;
	}

	glEnd();	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::renderLine( const PxVec3& p1, const PxVec3& p2, PxU32 color )
{
	glLineWidth( 2.f );
	glPointSize( 2.f );
	glBegin(GL_LINES);
	
	setColor( color );
	glVertex3f( -p1.x, p1.y, p1.z );
	glVertex3f( -p2.x, p2.y, p2.z );

	glEnd();	
	glLineWidth( 1.f );
	glPointSize( 1.f );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::renderTriangles( const PxRenderBuffer& _data )
{
	glBegin(GL_TRIANGLES);

	const PxDebugTriangle * lower = _data.getTriangles();
	const PxDebugTriangle * upper = lower + _data.getNbTriangles();
	while( lower < upper )
	{
		{
			PxU32 color = lower->color0;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos0 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}
		
		{
			PxU32 color = lower->color1;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos1 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}

		{
			PxU32 color = lower->color2;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos2 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}


		{
			PxU32 color = lower->color0;
			const PxVec3& point = Zpew::vectorXFlip( lower->pos0 );

			setColor( color );
			glVertex3f( point.x, point.y, point.z );
		}


		++lower;
	}

	glEnd();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
PhysxDebugGLRender::setColor( PxU32 color )
{
	float red = r( color );
	float green = g( color );
	float blue = b( color );

	if ( 4294902015 == color )
	{
		red = 0.1f;
		blue = 0.01f;
	}
		
	glColor3f( red, green, blue );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/