#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/Camera.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxMat33
get3x3RotMatrix(const float degrees, const PxVec3& up )
{
	assert( up.isNormalized() );
	const float rad = degrees * Zpew::Pi / 180.f;
	const float c = cos( rad );
	const float s = sin( rad );
	const float mc = 1.f - c;

	return PxMat33(
			PxVec3(
					up.x * up.x * mc	+	c	
				,	up.x * up.y * mc	-	up.z * s
				,	up.x * up.z * mc	+	up.y * s
			)
		,	PxVec3(	
					up.y * up.x * mc	+	up.z * s
				,	up.y * up.y * mc	+	c
				,   up.z * up.y * mc	-	up.x * s
			)
		,	PxVec3(
					up.z * up.x * mc	-	up.y * s
				,	up.z * up.y * mc	+	up.x * s
				,	up.z * up.z * mc	+	c
			)
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


const PxVec3&
Camera::rotate( float _xDeg, float _yDeg )
{
	_yDeg = -_yDeg;
	
	Zpew::clamp( mLowDegCone, mHighDegCone, _yDeg );
	Zpew::clamp( -180.f, 180.f, _xDeg );
	
	float targetDegree = currentPitchDegree() + _yDeg;
	mDir.y = 0;
	Zpew::clamp( mLowDegCone, mHighDegCone, targetDegree );
	Zpew::rotateXZ( mDir, _xDeg );
	mDir.normalize();
	
	Zpew::Angle a = Zpew::Angle::buildDegrees( targetDegree );
	mDir.y = a.sin();
	mDir.x *= a.cos();
	mDir.z *= a.cos();
	mDir.normalize();

	float pitchDegree = currentPitchDegree();
	assert ( Zpew::cmpEqual( pitchDegree, targetDegree, 0.01f ) );	
	assert ( Zpew::cmpSmaller( pitchDegree, mHighDegCone, 0.0001f ) );
	assert ( Zpew::cmpBigger( pitchDegree, mLowDegCone, 0.0001f ) );
	
	return mDir;

} // Camera::rotate


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Camera::Camera( PxVec3& pos, PxVec3& dir, PxVec3 up, float lowDegCone, float highDegCone )
	:	mPos( pos )
	,	mDir( dir )
	,	mUp( up )
	,	mLowDegCone( lowDegCone )
	,	mHighDegCone( highDegCone )
{
	mDir.normalize();
	mUp.normalize();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Camera::defaultPrefsLookAt()
{
	gluLookAt ( 
			pos().x
		,	pos().y
		,	pos().z
	
		,	-dir().x * 100.f + pos().x
		,	dir().y * 100.f + pos().y
		,	dir().z * 100.f + pos().z
		
		,	up().x
		,	up().y
		,	up().z
	);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float
Camera::currentPitchDegree() const
{
	return Zpew::Angle::atan(
		mDir.y / ::sqrt( mDir.x * mDir.x  +  mDir.z * mDir.z )
	).getDegrees();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/