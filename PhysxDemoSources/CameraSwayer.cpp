#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CameraSwayer.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CameraSwayer::CameraSwayer( std::auto_ptr< CCCamera > _camera, Real _usedFixedTimeStep  )
	:	m_camera( _camera.release() )
	,	m_frequency( _usedFixedTimeStep )
	,	m_angleDeviation( _usedFixedTimeStep )
	,	m_radius( _usedFixedTimeStep )
{
	reset();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CameraSwayer::reset()
{
	m_continueWork = false;

	m_frequency.reset();
	m_radius.reset();
	m_angleDeviation.reset();
	m_simulationElapsedFrequencyTime = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CameraSwayer::increaseFrequencyToAndDeviationAngleTo(
		Real _goalFreq
	,	Real _deltaFreqPerSec
	,	Real _goalAngleDeviation
	,	Real _deltaAngleDeviationPerSec
	,	Real _radius
	,	Real _radiusPerSec
)
{
	ZPEW_ASSERT( _goalAngleDeviation >= 0.0f && _goalAngleDeviation <= 90.0f );
	ZPEW_ASSERT( _goalFreq >= 0.0f );
	ZPEW_ASSERT(_radius >= 0.0f );

	m_continueWork = true;

	m_frequency.set( _goalFreq, _deltaFreqPerSec );
	m_angleDeviation.set( _goalAngleDeviation, _deltaAngleDeviationPerSec );
	m_radius.set( _radius, _radiusPerSec );
	
// 	m_frequency.setCurrentValue( _goalFreq );
// 	m_angleDeviation.setCurrentValue( _goalAngleDeviation );
// 	m_radius.setCurrentValue( _radiusPerSec );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
CameraSwayer::update( Real _timeStep, PxVec3 & _basePoint, Real _angleXZ  )
{
	PxVec3 newPosition( _basePoint.x, _basePoint.y, _basePoint.z );
	if ( ! m_continueWork )
		return newPosition;

	// correct all inertia
	m_frequency.update( _timeStep );
	m_radius.update( _timeStep );
	m_angleDeviation.update( _timeStep );

	// P0 - base Deviation Max angle
	// P - current angle from -P0 to P0
	Real P0 = * m_angleDeviation;
	Real W = * m_frequency;

	m_simulationElapsedFrequencyTime += W * _timeStep;
	// here can be RADIAN!
	Real P = P0 * Basement::Angle::buildDegrees( m_simulationElapsedFrequencyTime ).sin();

	Basement::Angle xzDeg = Basement::Angle::buildDegrees( _angleXZ + 90.0f );
	Basement::Angle yDeg = Basement::Angle::buildDegrees( P + 180.0f );
		
	Real deltaX = yDeg.sin() * ( *m_radius ) * xzDeg.sin(); // forward direction
	Real deltaY = yDeg.cos() * ( *m_radius );
	Real deltaZ = yDeg.sin() * ( *m_radius ) * xzDeg.cos(); // right direction

	newPosition.x += deltaX;
	newPosition.y += deltaY;
	newPosition.z += deltaZ;	
	
	return newPosition;

} // CameraSwayer::update


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCCamera&
CameraSwayer::getCamera()
{
	return *m_camera;
}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Real
CameraSwayer::getCurrentRadius() const
{
	return *m_radius;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
