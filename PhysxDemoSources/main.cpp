#include "PhysxDemoSources/PCH.hpp"

#include "PhysxDemoSources/Demo.hpp"


#pragma comment ( lib, "freeglut.lib" )
#pragma comment ( lib, "glew32.lib" )

int main(int argc,char** argv)
{
	try 
	{
		Demo demo( argc, argv );
	}
	catch( const std::exception& e )
	{
		std::cout<<"std::exception based exception occured :: " << e.what();
		return 1;
	}
	catch(...)
	{
		std::cout<<"Undefined exception occured\n";
		return 42;
	}

	return 0;
}