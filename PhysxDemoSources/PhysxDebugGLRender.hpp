
#pragma once

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class PhysxDebugGLRender
{
public:
	
	void begin();
	void end();

	void render(const PxRenderBuffer& _data);
	void renderLine( const PxVec3& p1, const PxVec3& p2, PxU32 color = 4242424242 );

private:
		
	void renderLines( const PxRenderBuffer& _data  );
	void renderTriangles( const PxRenderBuffer& _data );
	void renderPoints( const PxRenderBuffer& _data );

	void setColor( PxU32 color );

	union transport
	{
		PxU32 source;
		unsigned char color[4];
	};

	inline float r( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[0];
		return t.color[0] / 255.f;
	}

	inline float g( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[1];
		return t.color[1] / 255.f;
	}

	inline float b( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[2];
		return t.color[2] / 255.f;
	}

}; // class PhysxDebugGLRender


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
