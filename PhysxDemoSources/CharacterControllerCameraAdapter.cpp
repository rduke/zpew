
#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/CharacterControllerCameraAdapter.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CharCtrlCameraAdapter::CharCtrlCameraAdapter(
		CharacterDescription & _description
	,	std::auto_ptr< CCCamera > _camera
	,	CharacterControllerManager & _characterControllerManager
)
	:	CharacterControllerCore ( _description, _characterControllerManager )
	,	m_cameraSwayer( _camera, _description.m_usedTimeStep )
	,	m_isUserInputConnected( false )
	,	m_isCameraConnected( false )
	,	m_cameraAngleXZandYBeforeUserInputDisconnect( _description.m_originalAngleXZandY )
{
	setDefaultCameraProperties();

	int size = sizeof( CharCtrlCameraAdapter );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::correctCameraDirection(
		Real _originalAngleXZ
	,	Real _originalAngleY
)
{
	if ( ! m_isUserInputConnected )
		return;

	ZPEW_ASSERT( -90.0f <= _originalAngleY  &&  90.0f >= _originalAngleY  );
	PxVec3 newDirection( 
 			Basement::Angle::buildDegrees( _originalAngleXZ ).cos()
 		,	Basement::Angle::buildDegrees( _originalAngleY ).sin()
 		,	Basement::Angle::buildDegrees( _originalAngleXZ ).sin()
	);
	newDirection.normalize();

 	m_cameraSwayer.getCamera().setDirection( newDirection );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::update( Real _timeStep )
{
	CharacterControllerCore::update( _timeStep );

	if ( m_isUserInputConnected )
	{
		CCCamera& camera = m_cameraSwayer.getCamera();

		if ( m_isCameraConnected )
			camera.setPosition( cameraSwayerUpdate( _timeStep ) );

		Real angle = Basement::getXZFlatAngle_0_360( camera.getDirection() );
		rotateToAngle( angle );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::disconnectUserInput()
{
	if ( !m_isUserInputConnected )
		return;

	m_isUserInputConnected = false;
	m_isCameraConnected = false;

	m_cameraAngleXZandYBeforeUserInputDisconnect = Basement::getXZandYVectorAngle( getFaceNormal() );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::connectUserInput( bool _andCameraInput )
{
	m_isUserInputConnected = true;
	m_isCameraConnected = _andCameraInput;
	
	if ( m_isCameraConnected )
 		correctCameraDirection(
 				getFaceAngle()
 			,	m_cameraAngleXZandYBeforeUserInputDisconnect
 		);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
CharCtrlCameraAdapter::isUserControlled() const
{
	return true;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CameraControllerProperties
CharCtrlCameraAdapter::getCameraControllerProperties() const
{
	return m_cameraControllerProperties;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::setCameraControllerProperties( CameraControllerProperties& _properties )
{
	if( ! _properties.m_isFeatureEnabled )
	{
		m_cameraControllerProperties.m_isFeatureEnabled = false;
		return;
	}

	ZPEW_ASSERT(
				_properties.m_angleDeviationLimit.first
			<	_properties.m_angleDeviationLimit.second
		&&
			_properties.m_angleDeviationLimit.first >= 0.0f
	);

	ZPEW_ASSERT(
				_properties.m_radiusLimit.first
			<	_properties.m_radiusLimit.second
		&&
			_properties.m_radiusLimit.first >= 0.0f
	);

	ZPEW_ASSERT(
				_properties.m_frequencyLimit.first
			<	_properties.m_frequencyLimit.second

		&&
			_properties.m_frequencyLimit.first >= 0.0f
	);

	ZPEW_ASSERT( _properties.m_frequencyFromSpeedMultiplier > 0.0f );
	ZPEW_ASSERT( _properties.m_angleDeviationFromSpeedMultiplier > 0.0f );
	ZPEW_ASSERT( _properties.m_radiusFromSpeedMultiplier > 0.0f );

	const Real MaximumAdduction = 100.0f;
	ZPEW_ASSERT(
				_properties.m_frequencyAdductionMultiplier > 0.0f
			&&	_properties.m_frequencyAdductionMultiplier < MaximumAdduction
	);
	ZPEW_ASSERT(
				_properties.m_angleDeviationAdductionMultiplier > 0.0f
			&&	_properties.m_angleDeviationAdductionMultiplier < MaximumAdduction
	);

	ZPEW_ASSERT(
				_properties.m_radiusAdductionMultiplier > 0.0f
			&&	_properties.m_radiusAdductionMultiplier < MaximumAdduction
	);

	m_cameraControllerProperties = _properties;

} //CharCtrlCameraAdapter::setCameraControllerProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
CharCtrlCameraAdapter::cameraSwayerUpdate( Real _timeStep )
{
	PxVec3 controllerPosition = m_characterBody.updateInterpolatedPosition( _timeStep );
	// ibah42 - what a strange camera correction!
	controllerPosition.y += getCurrentHeight() * 0.37f; // from the middle of body to the top

	PxVec3 newCameraPosition = m_cameraSwayer.update( _timeStep, controllerPosition, getFaceAngle() );
	
	Real speed = getCurrentVectorVelocity().magnitude();
	if ( ! getCurrentCollisions() )
		speed /= 2;
	
	Real currentGoalFrequency = speed * m_cameraControllerProperties.m_frequencyFromSpeedMultiplier;
	Real currentGoalAngleDeviation = speed * m_cameraControllerProperties.m_angleDeviationFromSpeedMultiplier;
	Real currentGoalRadius = speed * m_cameraControllerProperties.m_radiusFromSpeedMultiplier;


	Basement::clamp(
			m_cameraControllerProperties.m_frequencyLimit.first
		,	m_cameraControllerProperties.m_frequencyLimit.second
		,	currentGoalFrequency
	);

	Basement::clamp(
			m_cameraControllerProperties.m_angleDeviationLimit.first
		,	m_cameraControllerProperties.m_angleDeviationLimit.second
		,	currentGoalAngleDeviation
	);

	Basement::clamp(
			m_cameraControllerProperties.m_radiusLimit.first
		,	m_cameraControllerProperties.m_radiusLimit.second
		,	currentGoalRadius
	);

	m_cameraSwayer.increaseFrequencyToAndDeviationAngleTo(
			currentGoalFrequency
		,	m_cameraControllerProperties.m_frequencyAdductionMultiplier
		,	currentGoalAngleDeviation
		,	m_cameraControllerProperties.m_angleDeviationAdductionMultiplier
		,	currentGoalRadius
		,	m_cameraControllerProperties.m_radiusAdductionMultiplier
	);

	newCameraPosition.y += m_cameraSwayer.getCurrentRadius();
	
	if ( m_cameraControllerProperties.m_isFeatureEnabled )
		return newCameraPosition;
	else
		return PxVec3(
				controllerPosition.x
			,	controllerPosition.y
			,	controllerPosition.z
		);			

} // CharCtrlCameraAdapter::cameraSwayerUpdate


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CharCtrlCameraAdapter::setDefaultCameraProperties()
{
	CameraControllerProperties properties;
	properties.m_isFeatureEnabled = true;

	properties.m_angleDeviationFromSpeedMultiplier = 3.0f;
	properties.m_angleDeviationLimit.first = 1.0f;
	properties.m_angleDeviationLimit.second = 50.0f;

	properties.m_frequencyFromSpeedMultiplier = 2.0f;
	properties.m_frequencyLimit.first = 0.0f;
	properties.m_frequencyLimit.second = 60.0f;

	properties.m_radiusFromSpeedMultiplier = 1.0f / 7.0f;
	properties.m_radiusLimit.first = 0.1f;
	properties.m_radiusLimit.second = 0.5f;

	properties.m_frequencyAdductionMultiplier = 5.0f;
	properties.m_angleDeviationAdductionMultiplier = 4.0f;
	properties.m_radiusAdductionMultiplier = 1.2f;

	setCameraControllerProperties( properties );

} // CharCtrlCameraAdapter::setDefaultCameraProperties


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
