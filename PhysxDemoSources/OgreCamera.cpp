#include "ZpewSources/PCH/ZpewPCH.hpp"
#include "ZpewSources/Physx/CharacterController/OgreCamera.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


OgreCamera::OgreCamera( Ogre::Camera& _camera )
	:	mCamera( _camera )
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxVec3
OgreCamera::getDirection() const
{
	return Basement::toVector3<PxVec3>( mCamera.getDirection() );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
OgreCamera::setDirection( const PxVec3& _direction )
{
	mCamera.setDirection( Basement::toVector3<Ogre::Vector3>( _direction ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
OgreCamera::setPosition( const PxVec3& _pos )
{
	mCamera.setPosition( Basement::toVector3<Ogre::Vector3>( _pos ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
