#ifndef __ZPEW_PCH_HPP__
#define __ZPEW_PCH_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <ctime>
#include <iostream>
#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <hash_set>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <PxPhysicsAPI.h>
#include <foundation/PxVec3.h>
typedef const physx::PxVec3& InVec3;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <windows.h>
#include <GL/gl.h>
#include <CommonSources/Glut/GL/glut.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Common/AutoConfiguration.hpp"

#include "ZpewSources/Common/Basement/Optional.hpp"
#include "ZpewSources/Common/Basement/RangeAcceleratingValue.hpp"
#include "ZpewSources/Common/Basement/InertValue.hpp"
#include "ZpewSources/Common/Basement/Switch.hpp"
#include "ZpewSources/Common/Basement/Utility.hpp"
#include "ZpewSources/Common/Basement/CosineInterpolationCurve.hpp"
#include "ZpewSources/Common/Basement/Angle.hpp"
#include "ZpewSources/Common/Basement/Rotation.hpp"

#include "ZpewSources/Physx/PhysXWorld.hpp"
#include "ZpewSources/Physx/PhysXScene.hpp"
#include "ZpewSources/Physx/PhysXCollisionGroup.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_PCH_HPP__
