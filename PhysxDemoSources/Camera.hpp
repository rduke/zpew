#pragma once

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Camera
{

public:

	Camera( PxVec3& pos, PxVec3& dir, PxVec3 up, float lowDegCone, float highDegCone );
	const PxVec3& rotate( float _xDeg, float _yDeg );

	inline			PxVec3& up()				{ return mUp;  }
	inline	const	PxVec3& dir() const			{ return mDir; }
	inline			PxVec3& pos()				{ return mPos; }
	
	void defaultPrefsLookAt();

	float currentPitchDegree() const;
private:

	float mLowDegCone;
	float mHighDegCone;
	PxVec3 mUp;
	PxVec3 mDir;
	PxVec3 mPos;

}; // class Camera


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
