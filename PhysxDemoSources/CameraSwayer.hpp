#ifndef __ZPEW_CameraSwayer_HPP__
#define __ZPEW_CameraSwayer_HPP__

#include "ZpewSources/Common/AutoConfiguration.hpp"
#include "ZpewSources/Physx/CharacterController/CharacterControllerPropertiesAndScenarios.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  CameraSwayer
	:	private boost::noncopyable
{

public:

	CameraSwayer( std::auto_ptr< CCCamera > _camera, Real _usedFixedTimeStep );

	CCCamera& getCamera();
	void reset();
	PxVec3 update( Real _timeStep, PxVec3 & _basePoint, Real _angleXZ );
	void increaseFrequencyToAndDeviationAngleTo(
			Real _goalFreq
		,	Real _deltaFreqPerSec
		,	Real _goalAngleDeviation
		,	Real _goalAngleDeviationInreasePerSec
		,	Real _radius
		,	Real _radiusPerSec
	);
	Real getCurrentRadius() const;

private:

	bool m_continueWork;

	// this variable is not real elapsed time, its time elapsed multiplied by frequency,
	// which can differs, from on simulation to another,
	// in real this is the real frequency of the Swayer based on Sin-function.
	Real m_simulationElapsedFrequencyTime;
	
	Basement::RealInertValue m_radius;
	Basement::RealInertValue m_frequency;
	Basement::RealInertValue m_angleDeviation;
	
	boost::scoped_ptr< CCCamera > m_camera;

}; // class CameraSwayer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ZPEW_CameraSwayer_HPP__
