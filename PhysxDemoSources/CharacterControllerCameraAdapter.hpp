#ifndef __ZPEW_CharacterControllerCameraAdapter_HPP__
#define __ZPEW_CharacterControllerCameraAdapter_HPP__

#include "ZpewSources/Common/AutoConfiguration.hpp"
#include "ZpewSources/Physx/CharacterController/CharacterControllerCore.hpp"
#include "ZpewSources/Physx/CharacterController/CameraSwayer.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  CharCtrlCameraAdapter
	:	public CharacterControllerCore
{

public:

	CharCtrlCameraAdapter(
			CharacterDescription & _description
		,	std::auto_ptr< CCCamera > _camera
		,	CharacterControllerManager & _characterControllerManager
	);

	virtual void update( Real _timeStep );
	virtual bool isUserControlled() const;

	void disconnectUserInput();
	void connectUserInput( bool _andCameraInput );
	virtual CameraControllerProperties getCameraControllerProperties() const;
	virtual void setCameraControllerProperties( CameraControllerProperties& _properties );

private:

	void correctCameraDirection(
			Real _originalAngleXY
		,	Real _originalAngleZ
	);

	PxVec3 cameraSwayerUpdate( Real _timeStep );
	void setDefaultCameraProperties();

private:

	CameraSwayer m_cameraSwayer;
	Real m_cameraAngleXZandYBeforeUserInputDisconnect;

	bool m_isCameraConnected;
	bool m_isUserInputConnected;
	
	CameraControllerProperties m_cameraControllerProperties;

}; // class CharCtrlCameraAdapter


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ZPEW_CharacterControllerCameraAdapter_HPP__
