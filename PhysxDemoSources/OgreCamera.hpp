#ifndef __ZPEW_OGRE_CAMERA_HPP__
#define __ZPEW_OGRE_CAMERA_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ZpewSources/Physx/CharacterController/CharacterControllerPropertiesAndScenarios.hpp"
#include <OgreVector3.h>
#include <OgreCamera.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace Zpew{
namespace Physx{
namespace CharacterController{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  OgreCamera
	:	public CCCamera
{
public:

	OgreCamera( Ogre::Camera& _camera );

	virtual PxVec3 getDirection() const;
	virtual void setDirection( const PxVec3& _direction );
	virtual void setPosition( const PxVec3& _pos );

private:

	Ogre::Camera & mCamera;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace CharacterController{
} // namespace Physx{
} // namespace Zpew{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif //__ZPEW_OGRE_CAMERA_HPP__
